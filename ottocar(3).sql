-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2016 at 08:22 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ottocar`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`id`, `name`) VALUES
(1, 'Printer'),
(2, 'Servers'),
(3, 'Desktop'),
(4, 'VMs'),
(5, 'Web-site'),
(6, 'Accounts');

-- --------------------------------------------------------

--
-- Table structure for table `audiobooks`
--

CREATE TABLE IF NOT EXISTS `audiobooks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `author` varchar(250) NOT NULL,
  `duration` int(11) NOT NULL,
  `readingOrder` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `audiobooks`
--

INSERT INTO `audiobooks` (`id`, `title`, `author`, `duration`, `readingOrder`) VALUES
(1, 'The Final Empire: Mistborn', 'Brandon Sanderson', 1479, 1),
(2, 'The Name of the Wind', 'Patrick Rothfuss', 983, 2),
(3, 'The Blade Itself: The First Law', 'Joe Abercrombie', 1340, 3),
(4, 'The Heroes', 'Joe Abercrombie', 1390, 4),
(5, 'Assassin''s Apprentice: The Farseer Trilogy', 'Robin Hobb', 1043, 5),
(6, 'The Eye of the World: Wheel of Time', 'Robert Jordan', 1802, 6),
(7, 'The Wise Man''s Fear', 'Patrick Rothfuss', 1211, 7),
(8, 'The Way of Kings: The Stormlight Archive', 'Brandon Sanderson', 2734, 8),
(9, 'The Lean Startup', 'Eric Ries', 523, 9),
(10, 'House of Suns', 'Alastair Reynolds', 1096, 10),
(11, 'The Lies of Locke Lamora', 'Scott Lynch', 1323, 11),
(12, 'Best Served Cold', 'Joe Abercrombie', 1592, 12),
(13, 'Thinking, Fast and Slow', 'Daniel Kahneman', 1206, 13),
(14, 'The Dark Tower I: The Gunslinger', 'Stephen King', 439, 14),
(15, 'Theft of Swords: Riyria Revelations', 'Michael J. Sullivan', 1357, 15),
(16, 'The Emperor''s Blades: Chronicle of the Unhewn Throne', 'Brian Staveley', 1126, 16),
(17, 'The Magic of Recluce: Saga of Recluce', 'L. E. Modesitt Jr.', 1153, 17),
(18, 'Red Country', 'Joe Abercrombie', 1196, 18),
(19, 'Warbreaker', 'Brandon Sanderson', 1496, 19),
(20, 'Magician', 'Raymond E. Feist', 2173, 20),
(21, 'Blood Song', 'Anthony Ryan', 1385, 21),
(22, 'Half a King', 'Joe Abercrombie', 565, 22),
(23, 'Prince of Thorns: Broken Empire', 'Mark Lawrence', 537, 23),
(24, 'The Immortal Prince: Tide Lords', 'Jennifer Fallon', 1164, 24),
(25, 'Medalon: Demon Child', 'Jennifer Fallon', 1039, 25),
(26, 'The Black Company: Chronicles of The Black Company', 'Glen Cook', 654, 26);

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE IF NOT EXISTS `candidates` (
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `car` text NOT NULL,
  `status` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`first_name`, `last_name`, `email`, `phone`, `car`, `status`) VALUES
('haroob', 'Lendil', 'Harrob.lendil@gmail.com', '07894343434', 'SKODA', '0');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `car` varchar(225) NOT NULL DEFAULT '0',
  `reg` varchar(225) DEFAULT NULL,
  `type` varchar(225) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `availability` varchar(225) DEFAULT '0',
  `condition` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=52 ;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `car`, `reg`, `type`, `customer_id`, `availability`, `condition`) VALUES
(51, '0', 'TESTCAR3', '1', 168, '0', 'new'),
(50, '0', 'TESTCAR2', '2', 168, '0', 'new'),
(49, '0', 'TESTCAR1', '4', 168, '1', 'new');

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE IF NOT EXISTS `car_type` (
  `model` text NOT NULL,
  `rental_price` text NOT NULL,
  `frequency` text NOT NULL,
  `rental_number` text NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`model`, `rental_price`, `frequency`, `rental_number`, `id`) VALUES
('Used Toyota Prius Hybrid', '199', 'weekly', '156', 1),
('Seat Alhambra SE Diesel Auto', '210', 'weekly', '156', 2),
('Skoda Octavia Hatch SE Diesel', '249', 'weekly', '156', 3),
('Toyota Prius Hybrid Active', '249', 'weekly', '156', 4);

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE IF NOT EXISTS `contracts` (
  `name` text NOT NULL,
  `length` int(2) NOT NULL,
  `price` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datatables_demo`
--

CREATE TABLE IF NOT EXISTS `datatables_demo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) NOT NULL DEFAULT '',
  `last_name` varchar(250) NOT NULL DEFAULT '',
  `position` varchar(250) NOT NULL DEFAULT '',
  `email` varchar(250) NOT NULL DEFAULT '',
  `office` varchar(250) NOT NULL DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `age` int(8) DEFAULT NULL,
  `salary` int(8) DEFAULT NULL,
  `seq` int(8) DEFAULT NULL,
  `extn` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `seq` (`seq`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `datatables_demo`
--

INSERT INTO `datatables_demo` (`id`, `first_name`, `last_name`, `position`, `email`, `office`, `start_date`, `age`, `salary`, `seq`, `extn`) VALUES
(1, 'Tiger', 'Nixon', 'System Architect', 't.nixon@datatables.net', 'Edinburgh', '2011-04-25 00:00:00', 61, 320800, 2, '5421'),
(2, 'Garrett', 'Winters', 'Accountant', 'g.winters@datatables.net', 'Tokyo', '2011-07-25 00:00:00', 63, 170750, 22, '8422'),
(3, 'Ashton', 'Cox', 'Junior Technical Author', 'a.cox@datatables.net', 'San Francisco', '2009-01-12 00:00:00', 66, 86000, 6, '1562'),
(4, 'Cedric', 'Kelly', 'Senior Javascript Developer', 'c.kelly@datatables.net', 'Edinburgh', '2012-03-29 00:00:00', 22, 433060, 41, '6224'),
(5, 'Airi', 'Satou', 'Accountant', 'a.satou@datatables.net', 'Tokyo', '2008-11-28 00:00:00', 33, 162700, 55, '5407'),
(6, 'Brielle', 'Williamson', 'Integration Specialist', 'b.williamson@datatables.net', 'New York', '2012-12-02 00:00:00', 61, 372000, 21, '4804'),
(7, 'Herrod', 'Chandler', 'Sales Assistant', 'h.chandler@datatables.net', 'San Francisco', '2012-08-06 00:00:00', 59, 137500, 46, '9608'),
(8, 'Rhona', 'Davidson', 'Integration Specialist', 'r.davidson@datatables.net', 'Tokyo', '2010-10-14 00:00:00', 55, 327900, 50, '6200'),
(9, 'Colleen', 'Hurst', 'Javascript Developer', 'c.hurst@datatables.net', 'San Francisco', '2009-09-15 00:00:00', 39, 205500, 26, '2360'),
(10, 'Sonya', 'Frost', 'Software Engineer', 's.frost@datatables.net', 'Edinburgh', '2008-12-13 00:00:00', 23, 103600, 18, '1667'),
(11, 'Jena', 'Gaines', 'Office Manager', 'j.gaines@datatables.net', 'London', '2008-12-19 00:00:00', 30, 90560, 13, '3814'),
(12, 'Quinn', 'Flynn', 'Support Lead', 'q.flynn@datatables.net', 'Edinburgh', '2013-03-03 00:00:00', 22, 342000, 23, '9497'),
(13, 'Charde', 'Marshall', 'Regional Director', 'c.marshall@datatables.net', 'San Francisco', '2008-10-16 00:00:00', 36, 470600, 14, '6741'),
(14, 'Haley', 'Kennedy', 'Senior Marketing Designer', 'h.kennedy@datatables.net', 'London', '2012-12-18 00:00:00', 43, 313500, 12, '3597'),
(15, 'Tatyana', 'Fitzpatrick', 'Regional Director', 't.fitzpatrick@datatables.net', 'London', '2010-03-17 00:00:00', 19, 385750, 54, '1965'),
(16, 'Michael', 'Silva', 'Marketing Designer', 'm.silva@datatables.net', 'London', '2012-11-27 00:00:00', 66, 198500, 37, '1581'),
(17, 'Paul', 'Byrd', 'Chief Financial Officer (CFO)', 'p.byrd@datatables.net', 'New York', '2010-06-09 00:00:00', 64, 725000, 32, '3059'),
(18, 'Gloria', 'Little', 'Systems Administrator', 'g.little@datatables.net', 'New York', '2009-04-10 00:00:00', 59, 237500, 35, '1721'),
(20, 'Dai', 'Rios', 'Personnel Lead', 'd.rios@datatables.net', 'Edinburgh', '2012-09-26 00:00:00', 35, 217500, 45, '2290'),
(21, 'Jenette', 'Caldwell', 'Development Lead', 'j.caldwell@datatables.net', 'New York', '2011-09-03 00:00:00', 30, 345000, 17, '1937'),
(22, 'Yuri', 'Berry', 'Chief Marketing Officer (CMO)', 'y.berry@datatables.net', 'New York', '2009-06-25 00:00:00', 40, 675000, 57, '6154'),
(23, 'Caesar', 'Vance', 'Pre-Sales Support', 'c.vance@datatables.net', 'New York', '2011-12-12 00:00:00', 21, 106450, 29, '8330'),
(24, 'Doris', 'Wilder', 'Sales Assistant', 'd.wilder@datatables.net', 'Sidney', '2010-09-20 00:00:00', 23, 85600, 56, '3023'),
(25, 'Angelica', 'Ramos', 'Chief Executive Officer (CEO)', 'a.ramos@datatables.net', 'London', '2009-10-09 00:00:00', 47, 1200000, 36, '5797'),
(26, 'Gavin', 'Joyce', 'Developer', 'g.joyce@datatables.net', 'Edinburgh', '2010-12-22 00:00:00', 42, 92575, 5, '8822'),
(27, 'Jennifer', 'Chang', 'Regional Director', 'j.chang@datatables.net', 'Singapore', '2010-11-14 00:00:00', 28, 357650, 51, '9239'),
(28, 'Brenden', 'Wagner', 'Software Engineer', 'b.wagner@datatables.net', 'San Francisco', '2011-06-07 00:00:00', 28, 206850, 20, '1314'),
(29, 'Fiona', 'Green', 'Chief Operating Officer (COO)', 'f.green@datatables.net', 'San Francisco', '2010-03-11 00:00:00', 48, 850000, 7, '2947'),
(30, 'Shou', 'Itou', 'Regional Marketing', 's.itou@datatables.net', 'Tokyo', '2011-08-14 00:00:00', 20, 163000, 1, '8899'),
(31, 'Michelle', 'House', 'Integration Specialist', 'm.house@datatables.net', 'Sidney', '2011-06-02 00:00:00', 37, 95400, 39, '2769'),
(32, 'Suki', 'Burks', 'Developer', 's.burks@datatables.net', 'London', '2009-10-22 00:00:00', 53, 114500, 40, '6832'),
(33, 'Prescott', 'Bartlett', 'Technical Author', 'p.bartlett@datatables.net', 'London', '2011-05-07 00:00:00', 27, 145000, 47, '3606'),
(34, 'Gavin', 'Cortez', 'Team Leader', 'g.cortez@datatables.net', 'San Francisco', '2008-10-26 00:00:00', 22, 235500, 52, '2860'),
(35, 'Martena', 'Mccray', 'Post-Sales support', 'm.mccray@datatables.net', 'Edinburgh', '2011-03-09 00:00:00', 46, 324050, 8, '8240'),
(36, 'Unity', 'Butler', 'Marketing Designer', 'u.butler@datatables.net', 'San Francisco', '2009-12-09 00:00:00', 47, 85675, 24, '5384'),
(37, 'Howard', 'Hatfield', 'Office Manager', 'h.hatfield@datatables.net', 'San Francisco', '2008-12-16 00:00:00', 51, 164500, 38, '7031'),
(38, 'Hope', 'Fuentes', 'Secretary', 'h.fuentes@datatables.net', 'San Francisco', '2010-02-12 00:00:00', 41, 109850, 53, '6318'),
(39, 'Vivian', 'Harrell', 'Financial Controller', 'v.harrell@datatables.net', 'San Francisco', '2009-02-14 00:00:00', 62, 452500, 30, '9422'),
(40, 'Timothy', 'Mooney', 'Office Manager', 't.mooney@datatables.net', 'London', '2008-12-11 00:00:00', 37, 136200, 28, '7580'),
(41, 'Jackson', 'Bradshaw', 'Director', 'j.bradshaw@datatables.net', 'New York', '2008-09-26 00:00:00', 65, 645750, 34, '1042'),
(42, 'Olivia', 'Liang', 'Support Engineer', 'o.liang@datatables.net', 'Singapore', '2011-02-03 00:00:00', 64, 234500, 4, '2120'),
(43, 'Bruno', 'Nash', 'Software Engineer', 'b.nash@datatables.net', 'London', '2011-05-03 00:00:00', 38, 163500, 3, '6222'),
(44, 'Sakura', 'Yamamoto', 'Support Engineer', 's.yamamoto@datatables.net', 'Tokyo', '2009-08-19 00:00:00', 37, 139575, 31, '9383'),
(45, 'Thor', 'Walton', 'Developer', 't.walton@datatables.net', 'New York', '2013-08-11 00:00:00', 61, 98540, 11, '8327'),
(46, 'Finn', 'Camacho', 'Support Engineer', 'f.camacho@datatables.net', 'San Francisco', '2009-07-07 00:00:00', 47, 87500, 10, '2927'),
(47, 'Serge', 'Baldwin', 'Data Coordinator', 's.baldwin@datatables.net', 'Singapore', '2012-04-09 00:00:00', 64, 138575, 44, '8352'),
(48, 'Zenaida', 'Frank', 'Software Engineer', 'z.frank@datatables.net', 'New York', '2010-01-04 00:00:00', 63, 125250, 42, '7439'),
(49, 'Zorita', 'Serrano', 'Software Engineer', 'z.serrano@datatables.net', 'San Francisco', '2012-06-01 00:00:00', 56, 115000, 27, '4389'),
(50, 'Jennifer', 'Acosta', 'Junior Javascript Developer', 'j.acosta@datatables.net', 'Edinburgh', '2013-02-01 00:00:00', 43, 75650, 49, '3431'),
(51, 'Cara', 'Stevens', 'Sales Assistant', 'c.stevens@datatables.net', 'New York', '2011-12-06 00:00:00', 46, 145600, 15, '3990'),
(52, 'Hermione', 'Butler', 'Regional Director', 'h.butler@datatables.net', 'London', '2011-03-21 00:00:00', 47, 356250, 9, '1016'),
(53, 'Lael', 'Greer', 'Systems Administrator', 'l.greer@datatables.net', 'London', '2009-02-27 00:00:00', 21, 103500, 25, '6733'),
(54, 'Jonas', 'Alexander', 'Developer', 'j.alexander@datatables.net', 'San Francisco', '2010-07-14 00:00:00', 30, 86500, 33, '8196'),
(55, 'Shad', 'Decker', 'Regional Director', 's.decker@datatables.net', 'Edinburgh', '2008-11-13 00:00:00', 51, 183000, 43, '6373'),
(56, 'Michael', 'Bruce', 'Javascript Developer', 'm.bruce@datatables.net', 'Singapore', '2011-06-27 00:00:00', 29, 183000, 16, '5384'),
(57, 'Donna', 'Snider', 'Customer Support', 'd.snider@datatables.net', 'New York', '2011-01-25 00:00:00', 27, 112000, 19, '4226');

-- --------------------------------------------------------

--
-- Table structure for table `dept`
--

CREATE TABLE IF NOT EXISTS `dept` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `dept`
--

INSERT INTO `dept` (`id`, `name`) VALUES
(1, 'IT'),
(2, 'Sales'),
(3, 'Pre-Sales'),
(4, 'Marketing'),
(5, 'Senior Management'),
(6, 'Accounts'),
(7, 'Support');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(250) NOT NULL,
  `filesize` int(11) NOT NULL,
  `web_path` varchar(250) NOT NULL,
  `system_path` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=115 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `filename`, `filesize`, `web_path`, `system_path`) VALUES
(1, 'auction-hammer.png', 705, '/upload/1.png', '/home/jamesdose/public_html/upload/1.png'),
(2, 'anchor.png', 694, '/upload/2.png', '/home/jamesdose/public_html/upload/2.png'),
(3, 'auction-hammer.png', 705, '/upload/3.png', '/home/jamesdose/public_html/upload/3.png'),
(4, 'anchor.png', 694, '/upload/4.png', '/home/jamesdose/public_html/upload/4.png'),
(5, 'auction-hammer.png', 705, '/upload/5.png', '/home/jamesdose/public_html/upload/5.png'),
(6, 'auction-hammer.png', 705, '/upload/6.png', '/home/jamesdose/public_html/upload/6.png'),
(7, 'auction-hammer.png', 705, '/upload/7.png', '/home/jamesdose/public_html/upload/7.png'),
(8, 'anchor.png', 694, '/upload/8.png', '/home/jamesdose/public_html/upload/8.png'),
(9, 'LECHON-BAKA_407_image.JPG', 40813, '/upload/9.JPG', '/home/jamesdose/public_html/upload/9.JPG'),
(10, 'auction-hammer.png', 705, '/upload/10.png', '/home/jamesdose/public_html/upload/10.png'),
(11, 'anchor.png', 694, '/upload/11.png', '/home/jamesdose/public_html/upload/11.png'),
(12, 'auction-hammer.png', 705, '/upload/12.png', '/home/jamesdose/public_html/upload/12.png'),
(13, 'auction-hammer.png', 705, '/upload/13.png', '/home/jamesdose/public_html/upload/13.png'),
(14, 'auction-hammer.png', 705, '/upload/14.png', '/home/jamesdose/public_html/upload/14.png'),
(15, 'auction-hammer.png', 705, '/upload/15.png', '/home/jamesdose/public_html/upload/15.png'),
(16, 'auction-hammer.png', 705, '/upload/16.png', '/home/jamesdose/public_html/upload/16.png'),
(17, 'anchor.png', 694, '/upload/17.png', '/home/jamesdose/public_html/upload/17.png'),
(18, 'auction-hammer.png', 705, '/upload/18.png', '/home/jamesdose/public_html/upload/18.png'),
(19, 'anchor.png', 694, '/upload/19.png', '/home/jamesdose/public_html/upload/19.png'),
(20, 'auction-hammer.png', 705, '/upload/20.png', '/home/jamesdose/public_html/upload/20.png'),
(21, 'anchor.png', 694, '/upload/21.png', '/home/jamesdose/public_html/upload/21.png'),
(22, 'auction-hammer.png', 705, '/upload/22.png', '/home/jamesdose/public_html/upload/22.png'),
(23, 'anchor.png', 694, '/upload/23.png', '/home/jamesdose/public_html/upload/23.png'),
(24, '22.png', 705, '/upload/24.png', '/home/jamesdose/public_html/upload/24.png'),
(25, 'anchor.png', 694, '/upload/25.png', '/home/jamesdose/public_html/upload/25.png'),
(26, 'auction-hammer.png', 705, '/upload/26.png', '/home/jamesdose/public_html/upload/26.png'),
(27, 'anchor.png', 694, '/upload/27.png', '/home/jamesdose/public_html/upload/27.png'),
(28, 'auction-hammer.png', 705, '/upload/28.png', '/home/jamesdose/public_html/upload/28.png'),
(29, 'anchor.png', 694, '/upload/29.png', '/home/jamesdose/public_html/upload/29.png'),
(30, 'auction-hammer.png', 705, '/upload/30.png', '/home/jamesdose/public_html/upload/30.png'),
(31, 'auction-hammer.png', 705, '/upload/31.png', '/home/jamesdose/public_html/upload/31.png'),
(32, 'anchor.png', 694, '/upload/32.png', '/home/jamesdose/public_html/upload/32.png'),
(33, 'auction-hammer.png', 705, '/upload/33.png', '/home/jamesdose/public_html/upload/33.png'),
(34, 'auction-hammer.png', 705, '/upload/34.png', '/home/jamesdose/public_html/upload/34.png'),
(35, 'anchor.png', 694, '/upload/35.png', '/home/jamesdose/public_html/upload/35.png'),
(36, 'auction-hammer.png', 705, '/upload/36.png', '/home/jamesdose/public_html/upload/36.png'),
(37, 'anchor.png', 694, '/upload/37.png', '/home/jamesdose/public_html/upload/37.png'),
(38, 'auction-hammer.png', 705, '/upload/38.png', '/home/jamesdose/public_html/upload/38.png'),
(39, 'anchor.png', 694, '/upload/39.png', '/home/jamesdose/public_html/upload/39.png'),
(40, 'auction-hammer.png', 705, '/upload/40.png', '/home/jamesdose/public_html/upload/40.png'),
(41, 'anchor.png', 694, '/upload/41.png', '/home/jamesdose/public_html/upload/41.png'),
(42, 'auction-hammer.png', 705, '/upload/42.png', '/home/jamesdose/public_html/upload/42.png'),
(43, 'auction-hammer.png', 705, '/upload/43.png', '/home/jamesdose/public_html/upload/43.png'),
(44, 'auction-hammer.png', 705, '/upload/44.png', '/home/jamesdose/public_html/upload/44.png'),
(45, 'anchor.png', 694, '/upload/45.png', '/home/jamesdose/public_html/upload/45.png'),
(46, 'anchor.png', 694, '/upload/46.png', '/home/jamesdose/public_html/upload/46.png'),
(47, 'auction-hammer.png', 705, '/upload/47.png', '/home/jamesdose/public_html/upload/47.png'),
(48, 'auction-hammer.png', 705, '/upload/48.png', '/home/jamesdose/public_html/upload/48.png'),
(49, 'auction-hammer.png', 705, '/upload/49.png', '/home/jamesdose/public_html/upload/49.png'),
(50, 'anchor.png', 694, '/upload/50.png', '/home/jamesdose/public_html/upload/50.png'),
(51, 'auction-hammer.png', 705, '/upload/51.png', '/home/jamesdose/public_html/upload/51.png'),
(52, 'anchor.png', 694, '/upload/52.png', '/home/jamesdose/public_html/upload/52.png'),
(53, 'auction-hammer.png', 705, '/upload/53.png', '/home/jamesdose/public_html/upload/53.png'),
(54, 'auction-hammer.png', 705, '/upload/54.png', '/home/jamesdose/public_html/upload/54.png'),
(55, 'auction-hammer.png', 705, '/upload/55.png', '/home/jamesdose/public_html/upload/55.png'),
(56, 'auction-hammer.png', 705, '/upload/56.png', '/home/jamesdose/public_html/upload/56.png'),
(57, 'anchor.png', 694, '/upload/57.png', '/home/jamesdose/public_html/upload/57.png'),
(58, 'auction-hammer.png', 705, '/upload/58.png', '/home/jamesdose/public_html/upload/58.png'),
(59, 'anchor.png', 694, '/upload/59.png', '/home/jamesdose/public_html/upload/59.png'),
(60, 'auction-hammer.png', 705, '/upload/60.png', '/home/jamesdose/public_html/upload/60.png'),
(61, 'auction-hammer.png', 705, '/upload/61.png', '/home/jamesdose/public_html/upload/61.png'),
(62, 'auction-hammer.png', 705, '/upload/62.png', '/home/jamesdose/public_html/upload/62.png'),
(63, 'auction-hammer.png', 705, '/upload/63.png', '/home/jamesdose/public_html/upload/63.png'),
(64, 'anchor.png', 694, '/upload/64.png', '/home/jamesdose/public_html/upload/64.png'),
(65, 'auction-hammer.png', 705, '/upload/65.png', '/home/jamesdose/public_html/upload/65.png'),
(66, 'auction-hammer.png', 705, '/upload/66.png', '/home/jamesdose/public_html/upload/66.png'),
(67, 'auction-hammer.png', 705, '/upload/67.png', '/home/jamesdose/public_html/upload/67.png'),
(68, 'anchor.png', 694, '/upload/68.png', '/home/jamesdose/public_html/upload/68.png'),
(69, 'auction-hammer.png', 705, '/upload/69.png', '/home/jamesdose/public_html/upload/69.png'),
(70, 'anchor.png', 694, '/upload/70.png', '/home/jamesdose/public_html/upload/70.png'),
(71, 'auction-hammer.png', 705, '/upload/71.png', '/home/jamesdose/public_html/upload/71.png'),
(72, 'auction-hammer.png', 705, '/upload/72.png', '/home/jamesdose/public_html/upload/72.png'),
(73, 'auction-hammer.png', 705, '/upload/73.png', '/home/jamesdose/public_html/upload/73.png'),
(74, 'anchor.png', 694, '/upload/74.png', '/home/jamesdose/public_html/upload/74.png'),
(75, 'auction-hammer.png', 705, '/upload/75.png', '/home/jamesdose/public_html/upload/75.png'),
(76, 'auction-hammer.png', 705, '/upload/76.png', '/home/jamesdose/public_html/upload/76.png'),
(77, 'auction-hammer.png', 705, '/upload/77.png', '/home/jamesdose/public_html/upload/77.png'),
(78, 'auction-hammer.png', 705, '/upload/78.png', '/home/jamesdose/public_html/upload/78.png'),
(79, 'auction-hammer.png', 705, '/upload/79.png', '/home/jamesdose/public_html/upload/79.png'),
(80, 'auction-hammer.png', 705, '/upload/80.png', '/home/jamesdose/public_html/upload/80.png'),
(81, 'auction-hammer.png', 705, '/upload/81.png', '/home/jamesdose/public_html/upload/81.png'),
(82, 'auction-hammer.png', 705, '/upload/82.png', '/home/jamesdose/public_html/upload/82.png'),
(83, 'auction-hammer.png', 705, '/upload/83.png', '/home/jamesdose/public_html/upload/83.png'),
(84, 'multiply.png', 455, '/upload/84.png', '/home/jamesdose/public_html/upload/84.png'),
(85, 'multiply.png', 455, '/upload/85.png', '/home/jamesdose/public_html/upload/85.png'),
(86, 'multiply.png', 455, '/upload/86.png', '/home/jamesdose/public_html/upload/86.png'),
(87, 'multiply.png', 455, '/upload/87.png', '/home/jamesdose/public_html/upload/87.png'),
(88, 'multiply.png', 455, '/upload/88.png', '/home/jamesdose/public_html/upload/88.png'),
(89, '2368201_honcho_concept.png', 14634, '/upload/89.png', '/home/jamesdose/public_html/upload/89.png'),
(90, '2368201_honcho_concept.png', 14634, '/upload/90.png', '/home/jamesdose/public_html/upload/90.png'),
(91, 'auction-hammer.png', 705, '/upload/91.png', '/home/jamesdose/public_html/upload/91.png'),
(92, 'auction-hammer.png', 705, '/upload/92.png', '/home/jamesdose/public_html/upload/92.png'),
(93, 'auction-hammer.png', 705, '/upload/93.png', '/home/jamesdose/public_html/upload/93.png'),
(94, 'anchor.png', 694, '/upload/94.png', '/home/jamesdose/public_html/upload/94.png'),
(95, 'auction-hammer.png', 705, '/upload/95.png', '/home/jamesdose/public_html/upload/95.png'),
(96, 'anchor.png', 694, '/upload/96.png', '/home/jamesdose/public_html/upload/96.png'),
(97, 'multiply.png', 455, '/upload/97.png', '/home/jamesdose/public_html/upload/97.png'),
(98, 'manila-streets.jpg', 43268, '/upload/98.jpg', '/home/jamesdose/public_html/upload/98.jpg'),
(99, 'manila-streets.jpg', 43268, '/upload/99.jpg', '/home/jamesdose/public_html/upload/99.jpg'),
(100, 'manila-streets.jpg', 43268, '/upload/100.jpg', '/home/jamesdose/public_html/upload/100.jpg'),
(101, 'anchor.png', 694, '/upload/101.png', '/home/jamesdose/public_html/upload/101.png'),
(102, '1545738_10152743292613067_3888786068487445116_n.jpg', 28931, '/upload/102.jpg', '/home/jamesdose/public_html/upload/102.jpg'),
(103, 'Cquym4jWgAAZgL0.jpg', 130690, '/upload/103.jpg', '/home/jamesdose/public_html/upload/103.jpg'),
(104, 'CrrV5wxUEAAwcqf.jpg', 82336, '/upload/104.jpg', '/home/jamesdose/public_html/upload/104.jpg'),
(105, 'CrJvdFqWYAADbcV.jpg', 72581, '/upload/105.jpg', '/home/jamesdose/public_html/upload/105.jpg'),
(106, 'CrNsI9aXEAAuowW.jpg', 66951, '/upload/106.jpg', '/home/jamesdose/public_html/upload/106.jpg'),
(107, 'CrJvdFqWYAADbcV.jpg', 72581, '/upload/107.jpg', '/home/jamesdose/public_html/upload/107.jpg'),
(108, '1545738_10152743292613067_3888786068487445116_n.jpg', 28931, '/upload/108.jpg', '/home/jamesdose/public_html/upload/108.jpg'),
(109, 'tumblr_n1cnjnwQe11s58x28o1_500.jpg', 157893, '/upload/109.jpg', '/home/jamesdose/public_html/upload/109.jpg'),
(110, '17.jpg', 71281, '/upload/110.jpg', '/home/jamesdose/public_html/upload/110.jpg'),
(111, 'DaysofBeingWild5.jpg', 19328, '/upload/111.jpg', '/home/jamesdose/public_html/upload/111.jpg'),
(112, 'otto_logo_gray.JPG', 14332, '/upload/112.JPG', '/home/jamesdose/public_html/upload/112.JPG'),
(113, 'SEA+Gallery-57.jpg', 75809, '/upload/113.jpg', '/home/jamesdose/public_html/upload/113.jpg'),
(114, '8cAbGeMXi.jpg', 46218, '/upload/114.jpg', '/home/jamesdose/public_html/upload/114.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `name`) VALUES
(1, 'pending'),
(2, 'pickup'),
(3, 'onroad'),
(4, 'accident'),
(5, 'cancelled');

-- --------------------------------------------------------

--
-- Table structure for table `todo`
--

CREATE TABLE IF NOT EXISTS `todo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL DEFAULT '',
  `done` tinyint(1) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `todo`
--

INSERT INTO `todo` (`id`, `item`, `done`, `priority`) VALUES
(1, 'Details incomplete', 0, 1),
(2, 'Awaiting pick-up', 1, 2),
(3, 'Picked Up', 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `address` text,
  `postcode` varchar(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `registered_date` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `user_type` varchar(225) DEFAULT NULL,
  `cartype` varchar(225) DEFAULT NULL,
  `filter` varchar(225) DEFAULT NULL,
  `site` int(11) DEFAULT NULL,
  `image` int(11) DEFAULT NULL,
  `car` text,
  `how_to_find` varchar(225) DEFAULT NULL,
  `national_insurance` text NOT NULL,
  `licence_number` text NOT NULL,
  `bank_card_number` int(11) NOT NULL,
  `bank_expiry_date` int(11) NOT NULL,
  `notes` text,
  `email` text NOT NULL,
  `mobile` text,
  `pickup_date` text,
  `flag` text NOT NULL,
  `shortterm_check` int(11) NOT NULL,
  `shortterm_car` int(11) NOT NULL,
  `motoroffence` int(11) NOT NULL,
  `criminalconviction` int(11) NOT NULL,
  `motorinsurance` int(11) NOT NULL,
  `claims` int(11) NOT NULL,
  `dob` text NOT NULL,
  `residentamount` int(11) NOT NULL,
  `licencetype` text NOT NULL,
  `datetestpassed` text NOT NULL,
  `taxilicencenumber` text NOT NULL,
  `taxilicenceage` int(11) NOT NULL,
  `deposit` int(11) NOT NULL,
  `ordersig` text NOT NULL,
  `ordersigdate` text NOT NULL,
  `ordersigotto` text NOT NULL,
  `ordersigdateotto` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=180 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `title`, `first_name`, `last_name`, `phone`, `address`, `postcode`, `updated_date`, `registered_date`, `active`, `user_type`, `cartype`, `filter`, `site`, `image`, `car`, `how_to_find`, `national_insurance`, `licence_number`, `bank_card_number`, `bank_expiry_date`, `notes`, `email`, `mobile`, `pickup_date`, `flag`, `shortterm_check`, `shortterm_car`, `motoroffence`, `criminalconviction`, `motorinsurance`, `claims`, `dob`, `residentamount`, `licencetype`, `datetestpassed`, `taxilicencenumber`, `taxilicenceage`, `deposit`, `ordersig`, `ordersigdate`, `ordersigotto`, `ordersigdateotto`) VALUES
(172, NULL, 'etet', 'fgdfgff', '', '', NULL, '2016-10-12 15:12:30', NULL, NULL, 'candidate', NULL, 'Waiting', 7, NULL, '1', 'Leaflet', 'fgdg4356546', '', 0, 0, '', '', '465654654657', NULL, '', 0, 0, 0, 0, 0, 0, 'Wednesday 12 October 2016', 0, 'Full UK', 'Wednesday 12 October 2016', '', 0, 0, '', '', '', ''),
(168, NULL, 'Mamood', 'Ashby', '0890890984', '22, Delta Street, London LE3 WE1', NULL, '2016-10-09 15:51:57', NULL, NULL, 'driver', NULL, 'Pass', 2, NULL, 'TESTCAR1', 'Leaflet', '', '', 0, 0, 'This was a good guy', 'Mamood.ashby@gmail.com', '', '', '', 1, 0, 0, 0, 0, 0, '2006-01-04', 0, '', '2006-04-19', 'GSFGS43', 0, 0, 'ASHBY', '2016-10-21', '', ''),
(174, NULL, 'dfdgfdg', '', '', '', NULL, '2016-10-12 16:21:49', NULL, NULL, '', NULL, '', NULL, NULL, '', '', '', '', 0, 0, '', '', NULL, NULL, '', 0, 0, 0, 0, 0, 0, 'Wednesday 12 October 2016', 1, 'Full UK', 'Wednesday 12 October 2016', '', 0, 0, '', '', '', ''),
(173, NULL, '', '', '', '', NULL, '2016-10-12 15:12:47', NULL, NULL, 'candidate', NULL, 'Waiting', 7, NULL, '1', 'Leaflet', '', '', 0, 0, '', '', NULL, NULL, '', 0, 0, 0, 0, 0, 0, 'Wednesday 12 October 2016', 0, 'Full UK', 'Wednesday 12 October 2016', '', 0, 0, '', '', '', ''),
(175, NULL, 'vfgdgdfg', 'dsfdsgdg', '', 'dfgdgdfg', NULL, '2016-10-12 16:31:07', NULL, NULL, NULL, NULL, NULL, 4, NULL, 'TESTCAR3', NULL, '', '', 0, 0, '', '', '', '', '', 1, 0, 0, 0, 0, 0, '', 0, '', '', '', 0, 0, '', '', '', ''),
(177, NULL, 'test', 'fghfg', '45645645', 'fdgdfg', NULL, '2016-10-13 11:05:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4ytyrt', '456546456', 0, 0, 'gdfgdf', 'test@gmail.com', '567567', '', '', 0, 0, 0, 0, 0, 0, '', 0, '', '', '', 0, 0, '', '', '', ''),
(178, NULL, 'gh', '45654', '45645', 'fgdgdfg', NULL, '2016-10-13 11:10:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '54645', '654645', 0, 0, 'fdgdfg', 'test@gmail.com', '45645', '', '', 0, 0, 0, 0, 0, 0, '', 0, '', '', '', 0, 0, '', '', '', ''),
(179, NULL, 'Alexa', 'Wilder', '1-727-307-1997', 'dfgfd', NULL, '2016-10-13 11:11:30', NULL, NULL, 'candidate', NULL, 'Waiting', 7, NULL, '1', 'Leaflet', '35454', '', 0, 0, '', 'alexa@gmail.com', '', NULL, '', 0, 0, 0, 0, 0, 0, 'Thursday 13 October 2016', 0, 'Full UK', 'Thursday 13 October 2016', '', 0, 0, '', '', '', '');

--
-- Triggers `users`
--
DROP TRIGGER IF EXISTS `users_insert`;
DELIMITER //
CREATE TRIGGER `users_insert` BEFORE INSERT ON `users`
 FOR EACH ROW SET NEW.updated_date = IFNULL(NEW.updated_date, NOW())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `users_update`;
DELIMITER //
CREATE TRIGGER `users_update` BEFORE UPDATE ON `users`
 FOR EACH ROW SET NEW.updated_date = IFNULL(NEW.updated_date, NOW())
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users_files`
--

CREATE TABLE IF NOT EXISTS `users_files` (
  `user_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_files`
--

INSERT INTO `users_files` (`user_id`, `file_id`) VALUES
(137, 114);

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `access_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`access_id`),
  KEY `access_id` (`access_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access`
--

INSERT INTO `user_access` (`user_id`, `access_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(2, 1),
(2, 4),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(5, 2),
(6, 6),
(7, 2),
(8, 1),
(9, 2),
(10, 1),
(10, 2),
(10, 3),
(11, 4),
(11, 6),
(12, 1),
(12, 2),
(12, 5),
(13, 1),
(13, 2),
(13, 3),
(13, 6),
(18, 1),
(18, 2),
(18, 3),
(20, 1),
(20, 2),
(20, 3),
(21, 2),
(21, 4),
(22, 2),
(22, 3),
(22, 6),
(30, 1),
(30, 3),
(30, 5),
(31, 3),
(32, 4),
(33, 6),
(34, 1),
(34, 2),
(34, 3),
(35, 2),
(36, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_dept`
--

CREATE TABLE IF NOT EXISTS `user_dept` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `dept_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`dept_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `dept_id` (`dept_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_dept`
--

INSERT INTO `user_dept` (`user_id`, `dept_id`) VALUES
(1, 1),
(2, 4),
(3, 7),
(4, 3),
(5, 2),
(6, 6),
(7, 2),
(8, 1),
(9, 2),
(10, 3),
(11, 4),
(12, 5),
(13, 6),
(14, 4),
(15, 3),
(16, 6),
(17, 3),
(18, 7),
(19, 7),
(20, 1),
(21, 2),
(22, 6),
(23, 3),
(24, 4),
(25, 5),
(26, 6),
(27, 7),
(28, 2),
(29, 3),
(30, 1),
(31, 3),
(32, 4),
(33, 6),
(34, 7),
(35, 2),
(36, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
