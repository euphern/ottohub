<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;


	if($_POST) {

		$usersid_data=@explode('_',$_POST['row_id']);
	 	$usersid=$usersid_data[1];
		//$dir_name=$_POST['dir_name'].'_'.$usersid;
		$dir_name = $_POST['dir_name'] ? $_POST['dir_name'] : 'images';
		$files=$_POST['files'];
		$files_all=implode(',',$files);
                $user_dir=$_POST['user'];
		
		$result=$db->sql("select * from files where id in($files_all)" );
		$lastresult = $result->fetchAll();

		//print_r($lastresult);

		$access_token = 'LZGo3bViDbAAAAAAAAAAFy4TSVcBpnehWszlJYi6WYeLoNbV63X3Dx5JmZqQ1zmS';
		   
	   	if(count($lastresult)>0){
			foreach($lastresult as $k=>$v){
				if(empty($v['dropbox_path'])){


					$img_name_details=@explode('.',$v['filename']);
					$ext=$img_name_details[1];

					//$dropbox_file_path = '/ottohubtest/'.$user_dir.'/'.$dir_name.'/'.@$v['id'].'.'.$ext;
					$dropbox_file_path = '/Ottohubtest/drivers/'.$dir_name.'/'.@$v['id'].'.'.$ext;

					 $image_path=$_SERVER['DOCUMENT_ROOT'].'/ottohub/upload/'.$user_dir.'/'.@$v['id'].'.'.$ext;

					//Set parameters for "Dropbox-API-Arg" header
					$dropbox_api_arg = array(
						'path' => $dropbox_file_path, 
						'mode' => 'add', //There are four value for mode. blank, add, overwrite, update
						'autorename' => true, // There are two value for autorename. true, false
						'mute' => false, // There are two value for mute. true, false
					);

					/*echo "<pre>";
					echo json_encode($dropbox_api_arg);
					echo "</pre>";*/

					// Set headers
				  	$headers = array(
				  		'User-Agent: api-explorer-client',
				  		'Authorization: Bearer '.$access_token, // This is the access token for a particular user
				  		'Content-Type: application/octet-stream',
				  		'Dropbox-API-Arg: '.stripslashes(json_encode($dropbox_api_arg)) // This is the array set at the very begining
				  	);

				  	/*echo "<pre>";
					print_r($headers);
					echo "</pre>";*/
					 
						
					// Open the file which is to be uploaded
					@chmod('0777',$image_path);
					$f = fopen($image_path, "rb");

				  	// Get cURL resource
					$curl = curl_init();

					// Set some options - we are passing in a useragent too here
					curl_setopt_array($curl, array(
							CURLOPT_RETURNTRANSFER => TRUE,
							CURLOPT_URL => 'https://content.dropboxapi.com/2/files/upload',
							CURLOPT_HTTPHEADER => $headers,
							CURLOPT_POST => TRUE,
							//CURLOPT_BINARYTRANSFER => TRUE,
							CURLOPT_POSTFIELDS => fread($f, filesize($image_path))
						)
					);

					// Send the request & save response to $resp
					$res = curl_exec($curl);

					// Convert the json response into array
					$result = json_decode($res, 1);

					/*echo "<pre>";
					print_r($result);
					echo "</pre>";*/

					// Close opened file
					fclose($f);

					// Close request to clear up some resources
					curl_close($curl);

					if(!empty($result['name']) && !empty($result['id'])) {
						echo "<br/>";
						echo "File successfully uploaded to DropBox";
						echo "<br/>";
						echo "Open file : <a href='https://www.dropbox.com/home".$result['path_display']."' target='_blank'>".$result['name']."</a>";

						$path_dropbox="https://www.dropbox.com/home".$result['path_display'];

			$result=$db->sql('update files set `dropbox_path`="'.$path_dropbox.'" where  id= "'.$v['id'].'"');
					}

				} 
			}
		}
	}

?>