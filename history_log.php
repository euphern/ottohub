<?php
include("php/DataTables.php");
$user_id = $_GET['user_id'];
$result_user = $db->sql('select *  from users where id=' . $user_id);
$lastresult_user = $result_user->fetchAll();
//print_r($lastresult_user);
$user = $lastresult_user[0];
$lines = file(__FILE__);
/*echo '<pre>';
print_r($lines);
echo '</pre>';*/
      //query to update user data

//echo $user['car'];
$result2 = $db->sql('update history_log set vehicle_reg = "'.$user['car'].'" where uid="'.$user_id.'"');
//$result2=$db->sql($query2);

$query = 'select * from history_log where uid="'.$user_id.'"';
$result1=$db->sql($query);
$lastresult1 = $result1->fetchAll();
$rowlength=sizeof($lastresult1);
//print_r($lastresult1);
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
    <title>History Log | Ottohub</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta name="robots" content="noindex">
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.3.min.js">
    </script>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic);
        @import url(http://fonts.googleapis.com/css?family=Arapey);
        @import url(http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700);
        @import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800);
        body {
            font-family: "Libre Baskerville", serif;
            font-size:16px;
            line-height:1.5em;
            color:#000;
            margin:0;
            background:#fff;
        }
        @media (max-width:600px) {
            body { font-size:14px; }
        }
        #content {
            max-width: 600px;
            margin: 0 auto;
            margin-bottom: 3em;
            padding: 0 2em;
            background: #fff;
        }
    </style>
    <script type="text/javascript" language="javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/editor.dataTables.css">
    <link rel="stylesheet" type="text/css" href="css/otto.css">
    <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <script src="http://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js">
    </script>
    <script type="text/javascript" src="http://momentjs.com/downloads/moment.js"></script>
    <script type="text/javascript" language="javascript" src="js/dataTables.editor.js">
    </script>
    <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js">
    </script>
    <script type="text/javascript" language="javascript" src="examples/resources/demo.js">
    </script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script>
        $(document).ready(function () {
            var table = $('#history_log').DataTable({
                dom: "Bfrtip",
                //ajax: "examples/php/upload-many-master.php",
                columns: [
                    {data: "history_log.event"},
                    {data: "history_log.event_date"},
                    {data: "history_log.vehicle_reg"},
                    {data: "history_log.db_link"},
                ],
                select: true,
                buttons: [
                    //{extend: "create", editor: editor},
                    //{extend: "edit", editor: editor},
                    //{extend: "remove", editor: editor}
                ]

            });
        });
    </script>
</head>
<body>
    <div class="container">
    <div style="width: 20%;margin: 5px auto;">
        <p><img  src="images/otto_logo.JPG" width="200px" height="100px"></p>
    </div>
    <h2>Otto Driver Hub Activity</h2>
    <div id="user_info" style="background:#ccc;">
        <div class="user_info_details" style="padding: 10px;">
         <h2 style="color:#0082ca">
         <b><?php echo $user['first_name'] . '&nbsp;' . $user['last_name'];?></b></h2>
         <p>Email :  <?php echo $user['email'] ;?></p>
         <p>Tel : <?php echo $user['mobile'] ;?></p><p></p>
        </div>
    </div>
    <div id="history" class="filter" style="width:100%">
        <table id="history_log" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Input Date</th>
                    <th>Vehicle Registration</th>
                    <th>Dropbox Document</th>
                </tr>
            </thead>

            <tbody>
                <?php 
                for ($row = 0; $row < $rowlength; $row++) {
                    echo "<tr><td>".$lastresult1[$row]['event']."</td><td>".$lastresult1[$row]['event_date']."</td>
                    <td>".$lastresult1[$row]['vehicle_reg']."</td><td><p>";

                    if($lastresult1[$row]['db_link']){
                       echo '<span style="color:#ff0000;"><a href="'.$lastresult1[$row]["db_link"].'" target="_blank">Click Here</a></span></p>';
                        }
                        else{
                        echo '<span style="color:red;">PDF File is not Generated</span></p>';
                        }
                    echo "</td></tr>";
                }
                ?>
            </body>

            <tfoot>
                <tr>
                    <th>Event Name</th>
                    <th>Input Date</th>
                    <th>Vehicle Registration</th>
                    <th>Dropbox Document</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
</body>
</html>