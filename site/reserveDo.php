<?php
require_once("inc/ActiveCampaign.class.php");

$ac = new ActiveCampaign(ACTIVECAMPAIGN_URL, ACTIVECAMPAIGN_API_KEY);

if (!isset($_POST)) { 
    header('HTTP/1.1 400 Bad Request');
    echo("Invalid request");
    exit();
}

if ($_POST['scheme'] == 'PCO Hire') {
    $tags = 'New PCO Hire reservation';
} else {
    $tags = 'New Rent-2-Buy reservation';
}

$data = array(
    'first_name'                    =>		$_POST["firstName"],
    'last_name'                     =>		$_POST["lastName"],
    'email'                         =>		$_POST["emailAddress"],
    'phone'                         =>		$_POST["phoneNumber"],
    'field[%SCHEME_PREFERENCE%,0]'  =>		$_POST["scheme"],
    'field[%REFERRED%,0]'           =>		$_POST["referred"],
    'field[%VEHICLE_NAME%,0]'       =>	    $_POST["vehicleName"],
    'ip4'                           =>      $_POST["ip4"],
    'tags'                          =>		$tags,
    'p[16]'                         =>		16, // list ID
    'status[16]'                    =>      1,
    'instantresponders[16]'         =>      1
);

$contact_sync = $ac->api("contact/sync", $data);

if (!(int)$contact_sync->success) {
    // request failed
    header('HTTP/1.1 400 Bad Request');
    echo("There was a problem communicating with our marketing platform (ERR: ".$contact_sync->error.")");
    exit();
}
        
// successful request
echo "Thank you, you will receive confirmation shortly";
exit();
?>

<script>
// FB extented : CompleteRegistration
// Track when a registration form is completed (ex. complete subscription, sign up for a service)
fbq('track', 'CompleteRegistration');
</script>

