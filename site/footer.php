<!-- SECTION: CONTACT -->
        <section id="contact" class="b-section b-section_theme_slate" itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="b-section__item">
                <div class="b-contact b-contact_type_single">
                    <div class="b-contact__icon">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#otto-icon-phone" />
                        </svg>
                    </div>
                    <div class="b-contact__body">
                        <a href="tel:+442087407444">
                            <span itemprop="telephone" class="tel-number">020 8740 7444</span>
                        </a>
                    </div>
                </div>
                <div class="b-contact b-contact_type_single b-contact__email">
                    <div class="b-contact__icon">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#otto-icon-mail" />
                        </svg>
                    </div>
                    <div class="b-contact__body">
                        <a href="mailto:info@ottocar.co.uk" itemprop="email">info@ottocar.co.uk</a>
                    </div>
                </div>
            </div>
            <div class="b-section__item b-contact_type_multiline">
                <div class="b-contact">
                    <div class="b-contact__icon">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#otto-icon-location" />
                        </svg>
                    </div>
                    <div class="b-contact__body">
                        <a itemprop="url" href="http://www.ottocar.co.uk">
                            <span itemprop="name">Otto Car Limited</span>
                        </a><br>
                        <span class="hidden" itemprop="description">Eager to earn good money with Uber? Get on the road by renting a PCO Prius from the best team in the business</span>
                        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                            <span itemprop="streetAddress">
                               3 Sussex Place 
                            </span>
                            <br>
                            <span itemprop="addressLocality">Hammersmith</span><br>
                            <span itemprop="addressRegion">London </span>
                            <span itemprop="postalCode">W6 9EA</span>
                            <span class="hidden" itemprop="addressCountry">United Kingdom</span>
                        </span>
                    </div>
                </div>
            </div>
        </section>
       
        <!-- SECTION: FOOTER -->
        <footer id="footer" class="b-footer b-section b-section_theme_dark-slate b-section_type_skinny">
            <div class="b-footer__links">
                <div class="b-footer__extras">
                    <a class="b-footer__link" href="/privacy.php">Privacy statement</a>
                    <a class="b-footer__link" href="/faq.php">FAQs</a>
                </div>
                <div class="b-footer__social">

                     <a class="b-social-icon__container" target="_blank" href="https://www.facebook.com/ottocaruk/">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#social-facebook" />
                        </svg>
                    </a>
                    <a class="b-social-icon__container" target="_blank" href="https://twitter.com/OttoCarUK">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#social-twitter" />
                        </svg>
                    </a>
                    <a class="b-social-icon__container" target="_blank" href="https://www.instagram.com/ottocaruk/">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#social-instagram" />
                        </svg>
                    </a>
                    <a class="b-social-icon__container" target="_blank" href="https://plus.google.com/u/0/116042554559219278958">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 192 192">
                            <use xlink:href="#social-googleplus" />
                        </svg>
                    </a>
                </div>
            </div>
            <hr class="b-footer__divider">
            <div class="b-footer__legal">
                <div class="b-footer__logo">
                    <a class="b-footer__logo-link" href="/">
                        <svg class="b-image b-image_type_svg" viewBox="0 0 140 65.41">
                            <use xlink:href="#otto-logo" />
                        </svg>
                    </a>
                </div>
                <div class="b-footer__legal-statement">
                    <p class="b-typography__paragraph">The ‘Otto’ name and logotype are registered trademarks of Otto Car Limited, a private limited company, registered in England and Wales: 08479726. Otto is authorised and regulated by the Financial Conduct Authority (FCA) Registration number: 702499</p>
                </div>
            </div>
        </footer>

    </main>

    <!-- SCRIPTS -->
    <!-- injector:js -->
    <script src="/js/vendor/countUp.js"></script>
    <script src="/js/vendor/jquery-1.12.3.min.js"></script>
    <script src="/js/vendor/jquery.bem.js"></script>
    <script src="/js/vendor/jquery.event.move.js"></script>
    <script src="/js/vendor/jquery.event.swipe.js"></script>
    <script src="/js/vendor/jquery.validate.min.js"></script>
    <script src="/js/vendor/jquery.validate.x.additional-methods.min.js"></script>
    <script src="/js/vendor/modernizr-custom.js"></script>
    <script src="/js/vendor/unslider-min.js"></script>
    <script src="/js/vendor/velocity.min.js"></script>
    <script src="/js/vendor/velocity.ui.js"></script>
    <script src="/js/base.js"></script>
    <script src="/js/global.js"></script>
    <script src="/js/blocks/carousel.js"></script>
    <script src="/js/blocks/form.js"></script>
    <script src="/js/blocks/google-map.js"></script>
    <script src="/js/blocks/hero.js"></script>
    <script src="/js/blocks/mobile-nav.js"></script>
    <script src="/js/blocks/nav-button.js"></script>
    <script src="/js/blocks/otto-icon.js"></script>
    <script src="/js/blocks/otto-logo.js"></script>
    <script src="/js/blocks/partner-logo.js"></script>
    <script src="/js/blocks/pricebox.js"></script>
    <script src="/js/blocks/social-icon.js"></script>
    <script src="/js/blocks/ui-icon.js"></script>
    <script src="/js/blocks/vehicles.js"></script>
    <script src="/js/blocks/select.js"></script>
    <!-- endinjector -->
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 935551950;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/935551950/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>