<!doctype html>
<!--[if lt IE 7]>      <html class="b-html no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="b-html no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="b-html no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="b-html no-js" lang="en">
<!--<![endif]-->

<head>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title id="page-title"><?php echo $page['title']; ?> | Otto Car&trade;</title>
    <meta name="description" content="All vehicles Uber-approved and available for PCO Car Hire and Rent-2-Buy, including Prius, Alhambra and Octavia. Prices start from &pound;199/wk.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--	<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
    <!--    <link rel="shortcut icon" href="img/favicon-32.png">-->

    <!-- Open Graph Meta -->
    <meta property="og:url" content="http://www.ottocar.co.uk">
    <meta property="og:site_name" content="Otto">
    <meta property="og:title" content="London’s largest PCO fleet | Otto Car&trade;">
    <meta property="og:description" content="All vehicles Uber-approved and available for PCO Car Hire and Rent-2-Buy, including Prius, Alhambra and Octavia. Prices start from &pound;165/wk.">
    <meta property="og:locale" content="en_GB">
    <meta property="og:type" content="website">
    <meta property="og:image" content="http://www.ottocar.co.uk/img/otto-og-thumb.png" />
    <meta property="og:image:type" content="image/png" />

    <link href="/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
    <link href="/css/print.css" media="print" rel="stylesheet" type="text/css" />
   <link rel="icon" type="image/png" href="http://www.ottocar.co.uk/img/blocks/ui-icon/favicon.png">

    <script src="https://use.typekit.net/wgy8hbk.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '693151224159636');
    fbq('track', "PageView");

    // ViewContent
    // Track key page views (ex: product page, landing page or article)
    fbq('track', 'ViewContent');


    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=693151224159636&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google Maps API -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-BUiRpphfCcHf2sR-Aq8r89ZAR0oCzoA"></script>
    <script type="text/javascript">
        (function (a, e, c, f, g, b, d) {
            var h = {
                ak: "948239510"
                , cl: "2xJiCMv_-V0QlvmTxAM"
            };
            a[c] = a[c] || function () {
                (a[c].q = a[c].q || []).push(arguments)
            };
            a[f] || (a[f] = h.ak);
            b = e.createElement(g);
            b.async = 1;
            b.src = "//www.gstatic.com/wcm/loader.js";
            d = e.getElementsByTagName(g)[0];
            d.parentNode.insertBefore(b, d);
            a._googWcmGet = function (b, d, e) {
                a[c](2, b, h, d, null, new Date, e)
            }
        })(window, document, "_googWcmImpl", "_googWcmAk", "script");
    </script>
    <!-- End Google Maps API -->

    <!-- Google Tag Manager -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64507724-1', 'auto');
  ga('send', 'pageview');

</script>


	<!-- End Google Tag Manager -->


</head>

<body id="<?php echo $page['id']; ?>" class="b-body" onload="_googWcmGet('tel-number', '+442087407444')">



	<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
   
    <div id="svg-sprites" class="hidden"></div>

    <main class="b-main">

        <!-- SECTION: HEADER -->
        <header class="b-header">
            <a class="b-header__logo-container" href="/">
                <svg class="b-image b-image_type_svg" viewBox="0 0 140 67">
                    <use xlink:href="#otto-logo" />
                </svg>
            </a>
            <nav class="b-nav" role="navigation">
                <ul class="b-nav__list">
                    <li class="b-nav__item">
                        <a class="b-nav__link<?php echo ($page['id'] == 'r2b' ? ' b-nav__link_current' : ''); ?>" href="rent-to-buy">Rent-2-Buy</a>
                    </li>
                    <li class="b-nav__item">
                        <a class="b-nav__link<?php echo ($page['id'] == 'pco' ? ' b-nav__link_current' : ''); ?>" href="/pco-car-hire">PCO Hire</a>
                    </li>
                    <li class="b-nav__item">
                        <a class="b-nav__link<?php echo ($page['id'] == 'about' ? ' b-nav__link_current' : ''); ?>" href="/about-us">About us</a>
                    </li>
                    <li class="b-nav__item">
                        <a class="b-nav__link<?php echo ($page['id'] == 'faq' ? ' b-nav__link_current' : ''); ?>" href="/faq">FAQs</a>
                    </li>
                    <li class="b-nav__item" style="display:none">
                        <a class="b-button b-button_theme_cta" href="/enquire">Enquire now</a>
                    </li>
                </ul>
            </nav>
            <a class="b-nav-button b-button b-button_theme_slate">
                <div class="b-nav-button__hamburger">
                    <div class="b-nav-button__bar b-nav-button__bar"></div>
                    <div class="b-nav-button__bar b-nav-button__bar"></div>
                    <div class="b-nav-button__bar b-nav-button__bar"></div>
                </div>
                <span class="b-nav-button__text">Menu</span>
            </a>
        </header>

        <nav class="b-mobile-nav" role="navigation">
            <ul class="b-mobile-nav__list">
                <li class="b-mobile-nav__item">
                    <a class="b-mobile-nav__link<?php echo ($page['id'] == 'r2b' ? ' b-mobile-nav__link_current' : ''); ?>" href="rent-to-buy">Rent-2-Buy</a>
                </li>
                <li class="b-mobile-nav__item">
                    <a class="b-mobile-nav__link<?php echo ($page['id'] == 'pco' ? ' b-mobile-nav__link_current' : ''); ?>" href="/pco-car-hire">PCO Hire</a>
                </li>
                <li class="b-mobile-nav__item">
                    <a class="b-mobile-nav__link<?php echo ($page['id'] == 'about' ? ' b-mobile-nav__link_current' : ''); ?>" href="/about-us">About us</a>
                </li>
                <li class="b-mobile-nav__item">
                    <a class="b-mobile-nav__link<?php echo ($page['id'] == 'faq' ? ' b-mobile-nav__link_current' : ''); ?>" href="/faq">FAQs</a>
                </li>
                <li class="b-mobile-nav__item" style="display:none">
                    <a class="b-button b-button_theme_cta" href="/enquire">Enquire now</a>
                </li>
            </ul>
        </nav>