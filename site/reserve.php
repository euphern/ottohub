<?php
$page['title'] = 'Reserve vehicle';
$page['id'] = 'reserve';
require_once($_SERVER["DOCUMENT_ROOT"].'/otto/site/header.php');

if (isset($_GET)) {
    $vehicle = $_GET['vehicle'];
    $vehicle_age = $_GET['vehicle_age'];
    $scheme = $_GET['scheme'];
}

?>


    <!-- SECTION: RESERVATION FORM -->
    <section id="reservation-form" class="b-section b-section_theme_grey">
       <div class="b-section__item b-section__item_cols_two b-section__item_mobile-prio_two">
        <form class="b-form" name="reservationForm" id="reservationForm" action="reserveDo.php" content-type="application/x-www-form-urlencoded" method="post">
            <h3 class="b-heading b-heading_level_three"><?=($scheme == 'pcoHire') ? 'PCO Hire: ' : 'Rent-2-Buy: '; ?>Reserve a vehicle</h3>
            <div class="b-form__row">
                <div class="b-form__element">
                    <label class="b-form__label" for="firstName">First name</label>
                    <input class="b-form__input" type="text" name="firstName" id="firstName" placeholder="Joe" required aria-required="true">
                </div>
                <div class="b-form__element">
                    <label class="b-form__label" for="lastName">Last name</label>
                    <input class="b-form__input" type="text" name="lastName" id="lastName" placeholder="Bloggs" required aria-required="true">
                </div>
            </div>
            <div class="b-form__row">
                <div class="b-form__element">
                    <label class="b-form__label" for="emailAddress">E-mail address</label>
                    <input class="b-form__input" type="email" name="emailAddress" id="emailAddress" placeholder="joe.bloggs@gmail.com" required aria-required="true">                    
                </div>
                <div class="b-form__element">
                    <label class="b-form__label" for="phoneNumber">Phone number</label>
                    <input class="b-form__input" type="tel" name="phoneNumber" id="phoneNumber" placeholder="+441234567890" required aria-required="true">                    
                </div>
            </div>
            
            <?php
            if ($scheme == 'pcoHire') {
            ?>
            
            <input type="hidden" name="vehicleName" id="vehicleName" value="Toyota Prius (<?=$vehicle_age?>)">
            <input type="hidden" name="scheme" id="scheme" value="PCO Hire">
            
            <?php
            } else {
            ?>
            
            <div class="b-form__row">
                <div class="b-form__element">
                    <label class="b-form__label" for="vehicleName">Which vehicle would you like to reserve?</label>
                    <input type="hidden" name="scheme" id="scheme" value="Rent-2-Buy">
                    <select class="b-form__select" name="vehicleName" id="vehicleName">
                        <option class="b-form__option" value="Skoda Octavia Hatch SE Diesel"<?php
                                echo (($vehicle == 'skodaOctavia')?' selected':"");
                            ?>>Skoda Octavia Hatch SE Diesel</option>
                        <option class="b-form__option" value="Toyota Prius Hybrid Active"<?php
                                echo (($vehicle == 'toyotaPrius')?' selected':"");
                            ?>>Toyota Prius Hybrid Active</option>
                        <option class="b-form__option" value="Seat Alhambra SE Diesel Auto"<?php
                                echo (($vehicle == 'seatAlhambra')?' selected':"");
                            ?>>Seat Alhambra SE Diesel Auto</option>
                        <option class="b-form__option" value="Used Toyota Prius Hybrid"<?php
                                echo (($vehicle == 'usedPrius')?' selected':"");
                            ?>>Used Toyota Prius Hybrid</option>
                    </select>
                </div>
            </div>
            
            <?php
            }
            ?>
            
            <div class="b-form__row">
                <div class="b-form__element">
                    <label class="b-form__label" for="referred">How did you find out about Otto?</label>
                    <select class="b-form__select" name="referred" id="referred">
                        <option class="b-form__option" value="Leaflet">Leaflet</option>
                        <option class="b-form__option" value="Word of mouth">Word of mouth</option>
                        <option class="b-form__option" value="Gumtree">Gumtree</option>
                        <option class="b-form__option" value="Uber Marketplace">Uber Marketplace</option>
                        <option class="b-form__option" value="Google Search">Google Search</option>
                        <option class="b-form__option" value="Facebook">Facebook</option>
                        <option class="b-form__option" value="Twitter">Twitter</option>
                        <option class="b-form__option" value="Google+">Google+</option>
                        <option class="b-form__option" value="Stickers on car">Stickers on car</option>
                        <option class="b-form__option" value="Online advert">Online advert</option>
                        <option class="b-form__option" value="Other">Other</option>
                    </select>
                </div>
            </div>
            <div class="b-form__row">
                <div class="b-form__element b-form__element_noshrink">
                    <button class="b-form__submit b-button b-button_theme_green" type="submit" name="submit" id="submit" value="reserve">Reserve now</button>
                </div>
                <div class="b-form__element">
<!--                   <div class="b-form__loader">Loading…</div>-->
                    <div class="b-form__response">Placeholder</div>
                </div>
            </div>
        </form>
        </div>
<div class="cardContainer">
         <div class="b-card b-card_theme_white b-card_shadow_one b-section__item b-section__item_cols_two b-section__item_mobile-prio_one">
           <div class="b-card__title">
            <h4 class="b-card__title-content">What you need to join Otto</h4>
             </div>
             <hr class="b-card__divider b-card__divider_theme_light">
           <div class="b-card__body">
            <ul class="b-list b-list_style_checklist b-typography__paragraph">
            
                    <?=($scheme == 'r2b') ? '' : '    <li class="b-list__item"> First weeks rent   </li>' ?>
             
                <li id="whatDownpayment" class="b-list__item">
                  <?=($scheme == 'pcoHire' || 'r2b' && $vehicle == 'seatAlhambra') ? '&pound;500 deposit' : '&pound;500 deposit' ?> 
                </li>
                <li class="b-list__item">PCO license &amp; badge</li>
                <li class="b-list__item">DVLA photocard license</li>
                <li class="b-list__item">Utility bill or bank statement <br>(must match DVLA address)</li>
            </ul>
             </div>
        </div>
  <div class="b-card b-card_theme_white b-card_shadow_one b-section__item b-section__item_cols_two b-section__item_mobile-prio_one">
           <div class="b-card__title">
            <h4 class="b-card__title-content">Eligibility Requirements:</h4>
             </div>
             <hr class="b-card__divider b-card__divider_theme_light">
           <div class="b-card__body">
            <ul class="b-list b-list_style_checklist b-typography__paragraph">
            
                 <li class="b-list__item">Minimum age is 25</li>
                 <?=($scheme == 'pcoHire') ? '' : '     <li class="b-list__item">Must have 3 months PCO experience</li>' ?>       
<?=($scheme == 'pcoHire') ? '' : '     <li class="b-list__item">Live in London for 2 years</li>' ?>       
                <li class="b-list__item">Must currently live in London</li>
                <li class="b-list__item">Good level of English</li>
            </ul>
             </div>
        </div>
</div>
    </section>
    
<?php require_once($_SERVER["DOCUMENT_ROOT"].'/otto/site/footer.php'); ?>