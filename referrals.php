<!DOCTYPE html>
<html>
<head id="head_part">
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>OttoHub beta</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/editor.dataTables.css">

    <link rel="stylesheet" type="text/css" href="css/otto.css">
    <!--  <link rel="stylesheet" type="text/css" href="css/red.css">
     <link rel="stylesheet" type="text/css" href="css/blue.css">  -->
    <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <script src="http://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>


    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js">
    </script>
    <script type="text/javascript" src="http://momentjs.com/downloads/moment.js"></script>
    <script type="text/javascript" language="javascript" src="js/dataTables.editor.js">
    </script>
    <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js">
    </script>
    <script type="text/javascript" language="javascript" src="examples/resources/demo.js">
    </script>

    <script type="text/javascript" language="javascript" src="js/otto.js">
    </script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" class="init">

      var host= $(location).attr('hostname');
      if(host=='localhost'){
         var  baseurl_ajax='http://localhost/ottohub/examples/';
         var  baseurl_org='http://localhost/ottohub/';
        }else{
         var  baseurl_ajax='http://ottohub.euphern.com/demo/examples/';
         var  baseurl_org='http://ottohub.euphern.com/demo/'; 
        }

        $(function(){

          var host= $(location).attr('hostname');
          if(host=='localhost'){
             var  baseurl_ajax='http://localhost/ottohub/examples/';
             var  baseurl_org='http://localhost/ottohub/';
            }else{
             var  baseurl_ajax='http://ottohub.euphern.com/demo/examples/';
             var  baseurl_org='http://ottohub.euphern.com/demo/'; 
            } 
        })


    var editor; // use a global for the submit and return data rendering in the examples
    var editor1;
    var editor2;
    var editor3;
    var editor4;
    var base_url = location.href;
    var url = base_url.split('hub');
    </script>

    <script>
           
        function car_registration(div_id, model_id) {

            var row = $('#example').find('.selected').attr('id');
            var condd = $('#example #' + row).find('td:eq(7)').text();
            var car = condd.split('-');

            var selected_car = $('#example #' + row).find('td:eq(5)').text();
            var type = $('#example #' + row).find('td:eq(6)').text();
            $.get(baseurl_ajax+'php/upload-many.php?action=carlistwith-registration&id=' + model_id + '&uid=' + row, function (res) {
                var result = $.parseJSON(res);
                var opt = '<option value="">Select</option>';
                var opt_short_term = '';
                for (var i = 0; i < Object.keys(result).length; i++) {
                    if (type == result[i].id) {
                        opt += '<option  selected="selected" value="' + result[i].id + '"  >' + result[i].name + '</option>';
                    } else {
                        opt += '<option value="' + result[i].id + '"  >' + result[i].name + '</option>';
                    }


                }

                $('#' + div_id).html(opt);
            });
        }

        function car_registration_reverse(div_id, model_id) {

            var row = $('#example').find('.selected').attr('id');
            var condd = $('#example #' + row).find('td:eq(7)').text();
            var car = condd.split('-');

            var selected_car = $('#example #' + row).find('td:eq(5)').text();
            var type = $('#example #' + row).find('td:eq(6)').text();
            $.get(baseurl_ajax+'php/upload-many.php?action=carlistwith-registration_rev&id=' + model_id + '&uid=' + row, function (res) {

                if (row == '') {
                    $('#' + div_id).find('option').each(function () {
                        if (res == $(this).val()) {
                            $(this).attr('selected', 'selected');
                        } else if (res == 'nodata') {

                            if ($(this).val() == '') {
                                $(this).attr('selected', 'selected');
                            } else {
                                $(this).removeAttr('selected');
                            }
                        } else {
                            $(this).removeAttr('selected');
                        }

                    });
                }

            });
        }

        //--------------------------Example 2-------------------Stock------------------//
        $(document).ready(function () {

            editor1 = new $.fn.dataTable.Editor({
                ajax: baseurl_ajax+"php/upload-many-master-referrals.php",
                table: "#referrals",
                fields: [
                    {
                        label: "First Name:",
                        name: "users.first_name"
                    },
                    {
                        label: "Last Name:",
                        name: "users.last_name",
                    }, 
                    {
                        label: "Car:",
                        name: "users.car",
                        type: "select"
                    },
                    {
                        label: "Scheme:",
                        name:"users.scheme",
                    }, {
                        label: "Date:",
                        name: "users.pickup_date",
                        type: "datetime",
                        /* def:   function () { return new Date(); }
                         },*/
                        def: function () {
                            return new Date();
                        },
                        format: 'D-MMM-YYYY',
                        fieldInfo: ''
                    }, {
                        label: "Source:",
                        name: "users.how_to_find",
                    }
                ]
            });

            editor1.on('open', function (e, type) {


                $('#custom_css_red').remove();
                $('#custom_css_blue').remove();
                createnew_car();
                $('.DTE_Form_Buttons_left').remove();
                /*var print_btn='<div class="DTE_Form_Buttons_left"><button class="printbtn"  >Print</button></div>';
                 $('.DTE_Footer').prepend(print_btn);*/
                /*var btn='<button class="bluebtn">Details</button>';
                 $('.DTE_Form_Buttons').prepend(btn);*/

                var opt1 = '';
                var condd_sel_pc = '';
                var condd_sel = '';

                var row = $('#referrals').find('.selected').attr('id');
                if (row) {
                    var condd = $('#' + row).find('td:eq(2)').text();
                    //alert(condd);
                    if (condd == 'used') {
                        condd_sel_pc = 'selected="selected"';
                    }
                    if (condd == 'new') {
                        condd_sel = 'selected="selected"';
                    }
                    opt1 += '<option value="">Select</option>';
                    opt1 += '<option value="new" ' + condd_sel + ' >New</option>';
                    opt1 += '<option value="used" ' + condd_sel_pc + ' >Used</option>';
                } else {

                    opt1 += '<option value="">Select</option>';
                    opt1 += '<option value="new">New</option>';
                    opt1 += '<option value="used">Used</option>';
                }

                $('#DTE_Field_cars-condition').html(opt1);


                $('.redbtn').bind('click', changered);
                $('.bluebtn').bind('click', changeblue);
                $('.insrancebtn').bind('click', changeinsurance);
                $('#details').remove();

                var details = '<div id="details" style="display:none;">';
                details += '<div>';
                details += '<div>';
                details += '<label>Registration Number:-</label>';
                details += '<div></div>';
                details += '</div>';
                details += '<div>';
                details += '<label>Car Model:-</label>';
                details += '<div></div>';
                details += '<div>';
                details += '<label>Driver:-</label>';
                details += '<div></div>';
                details += '</div>';
                details += '</div>';
                details += '</div>';

                $('.DTE_Body_Content').prepend(details);

                $('.DTE_Form_Content').show();

                $('.printbtn').bind('click', createPopup);

            });

            editor1.on('preSubmit', function (e, type) {
                // Type is 'main', 'bubble' or 'inline'
                console.log(type);
                var row = $('#referrals').find('.selected').attr('id');
                var cond_id = $('#DTE_Field_cars-condition').val();
                //var plan = $('#DTE_Field_cars-plan').val();
                //var site_id=$('#DTE_Field_users-site').val();
                var vid = 0;
                if (cond_id == 'new') {
                    vid = 0;
                }
                if (cond_id == 'used') {
                    vid = 0;
                }
                /*if (cond_id == 'withdriver') {
                    vid = 1;
                }*/
                var item = type.data;
                $.each(item, function (index, itv) {
                    itv.cars.condition = cond_id;
                    itv.cars.availability = vid;
                    //itv.cars.plan = plan;

                });
            });

            $(document).on('click', '.btn', function () {
                var row = $('#referrals').find('.selected').attr('id');
                if (row) {
                var idsrow = row.split("_");
                var user_id = idsrow[1];

                    $.ajax({
                        url: baseurl_ajax+"php/upload-many-master-referrals.php",
                        type: "post",
                        data: {user_id: user_id},
                        success: function (response) {
                        },

                    });
                }
            });

            editor1.on('submitSuccess', function (e, type) {
                getTotalCount();
                getTotalCountAvl();
                getTotalCountCars();
            });

            var table = $('#referrals').DataTable({
                dom: "Bfrtip",
                ajax: baseurl_ajax+"php/upload-many-master-referrals.php",
                deferRender: true,
                columns: [
                    {data: "users.first_name"},
                    {data: "users.last_name"},
                    {
                        data: null,
                        render: function (data, type, row) {
                            // Combine the first and last names into a single table field
                            return data.car_type.model;
                        }
                    },
                    {data: "users.scheme"},
                    {data: "users.pickup_date"},
                    {data: "users.how_to_find"},
                ],
                select: true,
                buttons: [
                    /*{
                        extend: "create",
                        editor: editor1
                    },
                    {
                        extend: "edit",
                        editor: editor1
                    },
                    {
                        extend: "remove",
                        editor: editor1
                    }*/
                ]
            });

            $('#referrals tbody').on('dblclick', 'tr', function () {
                //$('#example2 tbody tr').removeClass('selected');
                $(this).addClass('selected');
                $('#referrals_wrapper .buttons-edit').trigger('click');
                console.log(table.row(this).data());
                var data = table.row(this).data();
                var details = '';
                details += '<div>';
                details += '<div>';
                details += '<label>Registration Number:-</label>';
                details += '<div>' + data.cars.reg + '</div>';
                details += '</div>';
                details += '<div>';
                details += '<label>Car Model:-</label>';
                details += '<div>' + data.car_type.model + '</div>';
                details += '<div>';
                details += '<label>Driver-</label>';
                details += '<div>' + data.users.first_name + ' ' + data.users.last_name + '</div>';
                details += '</div>';
                details += '</div>';

                $('#details').html(details);

            });
        });

        $(document).ready(function () {
            getTotalCount();
            getTotalCountAvl();
            getTotalCountCars();
            getTotalCandidateFilterWise();

            //$('.carfilters .filter').show();
            //$('.carfilters  p').show();
            $('.filters  p').show();
            $(' .filter').show();
            //$('#referrals_filter').css('display','none');
            $('#referrals_filter').hide();
            //$(' .carfilters').show();
        });

        function getTotalCount() {
            $.get(baseurl_ajax+'php/collection_custom.php?action=total&id=', function (res) {
                var result = $.parseJSON(res);
                //console.log(result);
                var totaldriver = 0;
                for (var i = 0; i < Object.keys(result).length; i++) {
                    if (result[i].site == 1
                            || result[i].site == 5
                            || result[i].site == 4
                            || result[i].site == 3
                            || result[i].site == 2
                            || result[i].site == 7
                            || result[i].site == 10
                            || result[i].site == 11

                    ) {
                        totaldriver += parseInt(result[i].totaluser);
                    }
                    $('#site' + result[i].site).text(result[i].totaluser);
                }
                $('#site_all').text(totaldriver);
            });
        }

        function getTotalCountAvl() {
            $.get(baseurl_ajax+'php/collection_custom.php?action=totalavl&id=', function (res) {
                var result = $.parseJSON(res);
                for (var i = 0; i < Object.keys(result).length; i++) {
                    $('#site_car').text(result[i].totaluser);
                }
            });
        }

        function getTotalCountCars() {
            $.get(baseurl_ajax+'php/collection_custom.php?action=totalavlcar&id=', function (res) {
                var result = $.parseJSON(res);
                var totalcar = 0;
                for (var i = 0; i < Object.keys(result).length; i++) {

                    if (result[i].availability == 1
                            || result[i].availability == '-1'
                            || result[i].availability == 0
                    ) {
                        totalcar += parseInt(result[i].totaluser);
                    }
                    $('#site_car' + result[i].availability).text(result[i].totaluser);
                }
                $('#site_car_all').text(totalcar);
            });
        }

        function getTotalCandidateFilterWise() {
            $.get(baseurl_ajax+'php/collection_custom.php?action=totalCandidateFilterWise&filter=Pass&id=', function (res) {
                var result = $.parseJSON(res);
                //console.log(result.pass);
                $('#total_fail').html(result.pass);//Rent-2-Buy
                $('#total_waiting').html(result.waiting);
                $('#total_pass').html(result.fail);//PCO Hire

                var all_candidate = parseInt(result.pass) + parseInt(result.waiting) + parseInt(result.fail);
                $('#total_all').html(all_candidate);
            });
        }

        /* function getTotalBlacklist() {
            $.get('../php/collection_custom.php?action=Blacklist', function (res) {
                     var result = $.parseJSON(res);
                   console.log(result);
                   $('#total_pass').html(result.pass);
                       $('#total_waiting').html(result.waiting);
                      $('#total_fail').html(result.fail);
    
              var all_candidate = parseInt(result.pass) + parseInt(result.waiting) + parseInt(result.fail);
              $('#total_all').html(all_candidate);
            });
        }*/


        function createnew() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/bluer2bdrivers.css"
            });
        }

        function createnew_car() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/bluecars.css"
            });
        }

        function createnew_candidate() {

            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                    //href: "../../css/green.css"
            });
        }

        function changered() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_red',
                rel: "stylesheet",
                type: "text/css",
                    //href: "../../css/red.css"
            });
            $('#details').hide();
            $('.DTE_Form_Content').show();
        }

        function changered_pop() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_red_pop',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/red-bootstrap.css"
            });
        }

        function changered_shtterm() {

            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_red',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/redshorterm.css"
            });
        }

        function changeblue_shtterm() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/bluer2shortterm.css"
            });

            //$('#details').show();
            //$('.DTE_Form_Content').hide();
        }

        function changeblue() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                   //href: "../../css/blue.css"
            });

            //$('#details').show();
            //$('.DTE_Form_Content').hide();
        }

        function changeblue_candidate_details() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/Details_candidate1.css"
            });


            $(this).parents('.DTE_Action_Edit').find('form').show();

            $(this).parents('.DTE_Action_Edit .DTE_Form_Content').find('input').attr('disabled', 'disabled');
            $(".DTE_Action_Edit .DTE_Form_Content :input").prop("disabled", true);


            var row = $('#examplecandidate').find('.selected').attr('id');


            var idsrow = row.split("_");
            var user_id = idsrow[1];


            /*  $.ajax({
             type: "POST",
             url: baseurl_ajax+"php/candidate_details.php",
             data: {
             user_id:user_id
             },

             success: function (data) {
             $('#details').remove();

             var details = '<div id="details" >';
             details += '</div>';
             $('.DTE_Body_Content').prepend(details);

             $('#details').html(data);
             //$('#details').show();

             //$('#details_container .DTE_Form_Content').show();




             }
             });*/

            $('#details').hide();
            $('.DTE_Form_Content').show();
        }

        function shot_term_data_show() {
            if ($(this).is(':checked')) {
                $('.usersshortterm_car').show();
            } else {
                $('.usersshortterm_car').hide();
            }
        }

        //$('#DTE_Field_users-shortterm_check_0').bind('click',);

        function changeinsurance() {

            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_green_pop',
                rel: "stylesheet",
                type: "text/css",
                   // href: "../../css/green.css"
            });

            $('#details').hide();
            $('.DTE_Form_Content').show();
        }

        function changeblue_pop() {
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue_pop',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/blue-bootstrap.css"
            });
        }

        function showUploadFileOption() {
            //adding new field in upload section
            var mydome = "";
            mydome +="<div id='mynewdiv' style='padding:3%; width:100%;margin-bottom:60px'><div style='width:33%;float:left'><label>First Name*</label><br><input type='text' name='first_name' id='mfirst_name'></div><div style='width:33%;float:left'><label>Last Name*</label><br><input type='text' name='last_name' id='mlast_name' ></div><div style='width:33%;float:left'><label>DVLA Licence Number*</label><br><input type='text' name='dvla' id='mdvla'></div></div>";
            // $( ".DTE_Field_Type_uploadMany" ).prepend(mydom );
               $(mydome).insertBefore('.DTE_Field_Type_uploadMany');
            $(".DTE_Action_Edit .DTE_Form_Content :input").prop("disabled", false);$('#details').remove(); 
            // upload btn show harapriya 25january
            $('.DTE_Form_Content').show();if ($(this).parents('.DTE_Action_Create').length) {
            $(this).parents('.DTE_Action_Create').find('.DTE_Field').each(function (index, item) {
            if ($(item).hasClass('DTE_Field_Type_uploadMany')) {
            $(item).show();
            } else {
            $(item).hide();}
            });
            } else {
            $('#details').hide();$(this).parents('.DTE_Action_Edit').find('form').show();
            $(this).parents('.DTE_Action_Edit').find('.DTE_Field').each(function (index, item) {
            if ($(item).hasClass('DTE_Field_Type_uploadMany')) {
            $(item).show();
            } else {
            $(item).hide();}
            });
            }

            $('.extra_field').hide();
            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();$("head").append("");

            $('.upload button.btn').text('File Upload');
            $('button.btn.remove').text('x');

            var css = $("head").children(":last");css.attr({
            id: 'custom_css_blue',
            rel: "stylesheet",type: "text/css",
            href: "../../css/file_upload_driver.css"
            });
        }

        function hideUploadFileOption() {

            $('.edit_r2b').show();
            $('.bluebtn_r2b').hide();
            //$('.extra_field').hide();

            // $(this).parents('.DTE_Action_Edit .DTE_Form_Content').find('input').attr('disabled', 'disabled');
          //$(".DTE_Action_Edit .DTE_Form_Content :input").prop("disabled", true);
            //return false;

            //$(this).parents('.DTE_Action_Create').find('.DTE_Field_Type_uploadMany').hide();
            if ($(this).parents('.DTE_Action_Create').length) {
                $(this).parents('.DTE_Action_Create').find('.DTE_Field').each(function (index, item) {
                    if ($(item).hasClass('DTE_Field_Type_uploadMany')) {
                        $(item).hide();
                    } else {
                        $(item).show();
                    }
                });

                $(this).parents('.DTE_Action_Create').find('.file_upload').hide();
                $(this).parents('.DTE_Action_Create').find('.saveBtn').hide();

                $(this).parents('.DTE_Action_Create').find('form').show();
                //$(this).parents('.DTE_Action_Create').find('.extra_field').show();

            } else {

                //$('.DTE_Form_Content').hide();
                $('#details').remove();
                $(this).parents('.DTE_Action_Edit').find('form').show();
                $('.DTE_Action_Edit').find('.DTE_Field').each(function (index, item) {
                    if ($(item).hasClass('DTE_Field_Type_uploadMany')) {
                        $(item).hide();
                    } else {
                        $(item).show();
                        $(item).find('input[type="text"]').attr('disabled', 'disabled');
                        $(item).find('input[type="checkbox"]').attr('disabled', 'disabled');
                        $(item).find('select').attr('disabled', 'disabled');
                        $(item).find('textarea').attr('disabled', 'disabled');
                    }
                    if ($(item).hasClass('extra_field')) {$(item).hide();}
                });


                var row = $('#example').find('.selected').attr('id');


                var idsrow = row.split("_");
                var user_id = idsrow[1];


                /*$.ajax({
                 type: "POST",
                 url: baseurl_ajax+"php/driver_details.php",
                 data: {
                 user_id: user_id
                 },

                 success: function (data) {
                 $('#details').remove();

                 var details = '<div id="details" >';
                 details += '</div>';
                 $('.DTE_Body_Content').prepend(details);

                 //$('#details').html(data);
                 //$('#details').show();


                 $('.DTE_Form_Content').append(data);
                 $('.DTE_Action_Edit').find('.DTE_Field').each(function (index, item) {
                 $(item).hide();
                 });


                 }
                 });*/


            }

            $('#custom_css_red_pop').remove();
            $('#custom_css_blue_pop').remove();
            $('#custom_css_green_pop').remove();
            $('#custom_css_red').remove();
            $('#custom_css_blue').remove();
            $("head").append("<link>");
            var css = $("head").children(":last");
            css.attr({
                id: 'custom_css_blue',
                rel: "stylesheet",
                type: "text/css",
                href: "../../css/details_driver.css"
            });
        }

        function showEditSection(e) {
            $('#allDetails').remove();
            $('.bluebtn_r2b').show();
            $('.edit_r2b').hide();

            $(this).parents('.DTE_Action_Edit').find('.DTE_Field').each(function (index, item) {
                if ($(item).hasClass('DTE_Field_Type_uploadMany')) {
                    $(item).hide();
                } else {
                    $(item).show();
                    $(item).find('input[type="text"]').removeAttr('disabled');
                    $(item).find('input[type="checkbox"]').removeAttr('disabled');
                    $(item).find('select').removeAttr('disabled');
                    $(item).find('textarea').removeAttr('disabled');
                }
                if ($(item).hasClass('extra_field')) {$(item).hide();}//hides r2b contract
            });
            $('.DTE_Form_Content').show();
        }

        function uploadToDropbox(type,user) {
            //console.log("We are in upload function");

            var files = [];
            var fileObjs = type.data[0].files;
            $.each(fileObjs, function (index, item) {
                files.push(item.id);
            });
            //sending new 3 fields parameters
            var first_name = $('input#mfirst_name').val();
            var last_name = $('input#mlast_name').val();
            var dvla = $('input#mdvla').val();
                        var row_id = type.data[0].DT_RowId;
            if(user=='enquiry'){
              var row = $('#examplecandidate').find('.selected').attr('id'); 
            if(first_name==''){

                var person8 = prompt("Please Enter First Name","");

                if (person8 != null) {
                   var first_name = person8;
                }
            }

            if(last_name==''){

                var person9 = prompt("Please Enter Last Name","");

                if (person9 != null) {
                   var last_name = person9;
                }
            }

            if(dvla==''){

                var person2 = prompt("Please DVLA Licence Number","");

                if (person2 != null) {
                   var dvla = person2;
                }
            }              
                }else{
                    var row = $('#example').find('.selected').attr('id');
                 $.get(baseurl_ajax+'php/client_action.php',{user_id:user_id},function(res){
                     var user=$.parseJSON(res); 
                     var first_name = user[0].first_name;
                     var last_name = user[0].last_name;
                     var dvla = user[0].taxilicencenumber;

                     $('input#mfirst_name').val(first_name);
                     $('input#mlast_name').val(last_name);
                     $('input#mdvla').val(dvla);

                        });
            first_name = $('input#mfirst_name').val();
            last_name = $('input#mlast_name').val();
            dvla = $('input#mdvla').val();
                        }
                var idsrow = row.split("_");
                var user_id = idsrow[1];

                console.log(type.data[0].users.dob);

                var dvla = (type.data[0].users.taxilicencenumber) ? type.data[0].users.taxilicencenumber : '';

               /* var dob = (type.data[0].users.dob) ? type.data[0].users.dob : '';
                var dateOfBirth = null;
                if( dob ) {
                    dateOfBirth = new Date(dob);
                } else {
                    dateOfBirth = new Date('1970-01-01');
                }

                console.log("dateOfBirth : " + dateOfBirth);

                var date = dateOfBirth.getDate();
                date = (date < 10) ? '0' + date : date;

                var month = dateOfBirth.getMonth() + 1;
                month = (month < 10) ? '0' + month : month;

                var year = dateOfBirth.getFullYear();

                dob = date + '-' + month + '-' + year;*/







                var dir_name = type.data[0].users.first_name.trim()+'_'+type.data[0].users.last_name.trim()+'_'+dvla.trim();
                console.log(dir_name);
                //var dir_name = type.data[0].users.first_name + '_' + type.data[0].users.last_name +_+ type.data[0].users.dob;
                //return false;

                $.post(baseurl_ajax+'php/dropbox_upload.php', {
                    'files': files,
                    'row_id': row_id,
                    'dir_name': dir_name,
                     'user':user,
                     'first_name':first_name,
                     'last_name':last_name,
                     'dvla':dvla,
                     'user_id':user_id
                }, function (res) {
                    console.log(res);
                });
        }

        function copyLinkClickHandler(e) {
            var element = $(e.currentTarget).parents('.extra_field').find('.copyText');
            console.log(element);
            copyToClipboard(element);
        }

        function copyToClipboard(elem) {
            //console.log(elem);

            var targetId = "_hiddenCopyText_";
            var isInput = elem[0].tagName === "INPUT" || elem[0].tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                console.log("isInput");
                // can just use the original source element for the selection and copy
                target = elem[0];
                origSelectionStart = elem[0].selectionStart;
                origSelectionEnd = elem[0].selectionEnd;
            } else {
                console.log("else");
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                //target.textContent = elem[0].textContent;
                target.textContent = elem[0].value;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem[0].setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }

        /*function enterPressHandler( e ) {
            console.log('ok');
            //$(e.currentTarget).parents('.DTE').find('.btn').removeAttr('tabindex');
            //$(e.currentTarget).parents('.DTE').find('.saveBtn').removeAttr('tabindex');
            //$(e.currentTarget).parents('.DTE').find('.saveBtn').preventDefault();
            if (e.keyCode == 13) {
                editor.edit(this, {
                    submitOnReturn: false
                });
            }
        }*/

        /********* stock filter data and labels***********/
        $(document).ready(function () {
            editor2 = new $.fn.dataTable.Editor({
                ajax: baseurl_ajax+"php/upload-many-master-car.php?action=showtabledata&id=&aid=",
                table: "#example4",
                fields: [{
                    label: "Registration Number:",
                    name: "cars.reg"
                }, {
                    label: "Car Model:",
                    name: "cars.type",
                    className: "users_last_name",
                    type: "select"
                }, {
                    label: "Car Condition:",
                    name: "cars.condition",
                    type: "select"
                },
                    {
                        label: "Driver:",
                        name: "users.first_name",
                        type: "readonly"
                    }, {
                        label: "Car Availability:",
                        name: "cars.availability",
                        type: "hidden"
                    }]
            });

            editor2.on('open', function (e, type) {

                $('#custom_css_red').remove();
                $('#custom_css_blue').remove();
                createnew();
                $('.DTE_Form_Buttons_left').remove();
                /*var print_btn='<div class="DTE_Form_Buttons_left"><button class="printbtn"  >Print</button></div>';
                 $('.DTE_Footer').prepend(print_btn);*/
                var btn = '<button class="bluebtn">Details</button>';
                $('.DTE_Form_Buttons').prepend(btn);
                $('.redbtn').bind('click', changered);
                $('.bluebtn').bind('click', changeblue);
                $('.insrancebtn').bind('click', changeinsurance);


                $('#details').remove();

                var details = '<div id="details" style="display:none;">';
                details += '<div>';
                details += '<div>';
                details += '<label>Registration Number:-</label>';
                details += '<div></div>';
                details += '</div>';
                details += '<div>';
                details += '<label>Car Model:-</label>';
                details += '<div></div>';
                details += '<div>';
                details += '<label>Car Plan:-</label>';
                details += '<div></div>';
                details += '</div>';
                details += '</div>';
                details += '</div>';
                $('.DTE_Body_Content').prepend(details);

                $('.DTE_Form_Content').show();

                $('.printbtn').bind('click', createPopup);

                //$('#myModal').modal('hide');

                var opt1 = '';
                var condd_sel_pc = '';
                var condd_sel = '';

                var row = $('#example4').find('.selected').attr('id');
                if (row) {
                    var condd = $('#' + row).find('td:eq(2)').text();
                    //alert(condd);
                    if (condd == 'used') {
                        condd_sel_pc = 'selected="selected"';
                    }
                    if (condd == 'new') {
                        condd_sel = 'selected="selected"';
                    }
                    opt1 += '<option value="">Select</option>';
                    opt1 += '<option value="new" ' + condd_sel + ' >New</option>';
                    opt1 += '<option value="used" ' + condd_sel_pc + ' >Used</option>';
                } else {

                    opt1 += '<option value="">Select</option>';
                    opt1 += '<option value="new">New</option>';
                    opt1 += '<option value="used">Used</option>';
                }

                $('#DTE_Field_cars-condition').html(opt1);
            });

            editor2.on('preSubmit', function (e, type) {
                // Type is 'main', 'bubble' or 'inline'
                console.log(type);
                var row = $('#example4').find('.selected').attr('id');
                var cond_id = $('#DTE_Field_cars-condition').val();
                //var site_id=$('#DTE_Field_users-site').val();
                //var plan = $('#DTE_Field_cars-plan').val();
                var vid = 0;
                if (cond_id == 'new') {
                    vid = 0;
                }
                if (cond_id == 'used') {
                    vid = 1;
                }
                var item = type.data;
                $.each(item, function (index, itv) {
                    itv.cars.condition = cond_id;
                    itv.cars.availability = vid;
                    //itv.cars.plan = plan;
                });
            });

            editor2.on('submitSuccess', function (e, type) {
                getTotalCount();
                getTotalCountAvl();
                getTotalCountCars();
            });
        });

        function showtables_car(id) {
            if (id == 'all') {
                $('#all_car').show();
                $('#filter_car').hide();

            } else {

                $('#all_car').hide();
                $('#filter_car').show();
                $('.tabs').find('li').each(function (index, item) {

                    if ($(this).attr('data-id') == 'example2') {
                        // $('li').removeClass('active');
                        //$(this).addClass('active');
                        //$('.js').hide();
                        // $('.css').show();
                        $('#custom_css_blue_pop').remove();
                        $('#custom_css_red_pop').remove();

                        var tbl = '<table id="example4" class="display" cellspacing="0" width="100%">\
                                                                <thead>\
                                                                        <tr>\
                                                                                <th>Reg</th>\
                                                                                <th>Type</th>\
                                                                                <th>Condition</th>\
                                                                                <th>Status</th>\
                                                                                <th>Driver</th>\
                                                                        </tr>\
                                                                </thead>\
                                                        </table>';

                        $('#filter_car').html(tbl);

                        var table = $('#example4').DataTable({
                            dom: "Bfrtip",
                            ajax: baseurl_ajax+"php/upload-many-master-car.php?action=showtabledata&id=4&aid=" + id,
                            deferRender: true,
                            columns: [
                                {data: "cars.reg"},
                                {data: "car_type.model"},
                                {data: "cars.condition"},
                                {
                                    data: null,
                                    render: function (data, type, row) {
                                        // Combine the first and last names into a single table field
                                        if (data.cars.availability == 0) {
                                            var sitename = 'Available';
                                        }
                                        if (data.cars.availability == '-1') {
                                            var sitename = 'Accident';
                                        }
                                        if (data.cars.availability == 1) {
                                            var sitename = 'Car with Driver';
                                        }

                                        return (data.cars.availability == 0) ? 'Available' : sitename;
                                    }
                                },
                                //{data: "cars.plan"}
                                {
                                    data: null,
                                    render: function (data, type, row) {
                                        // Combine the first and last names into a single table field
                                        //return data.cars.condition;
                                        if (data.users.first_name) {
                                            return data.users.first_name + ' ' + data.users.last_name;
                                        } else {
                                            return '';
                                        }
                                    }
                                },

                            ],
                            select: true,
                            buttons: [{
                                extend: "create",
                                editor: editor2
                            }, {
                                extend: "edit",
                                editor: editor2
                            }, {
                                extend: "remove",
                                editor: editor2
                            }]

                        });


                        $('#example4 tbody').on('dblclick', 'tr', function () {
                            //$('#example4 tbody tr').removeClass('selected');
                            $(this).addClass('selected');
                            $('#example4_wrapper .buttons-edit').trigger('click');
                            console.log(table.row(this).data());

                            var data = table.row(this).data();
                            var details = '';
                            details += '<div>';
                            details += '<div>';
                            details += '<label>Registration Number:-</label>';
                            details += '<div>' + data.cars.reg + '</div>';
                            details += '</div>';
                            details += '<div>';
                            details += '<label>Car Model:-</label>';
                            details += '<div>' + data.car_type.model + '</div>';
                            details += '<div>';
                            details += '<label>Driver:-</label>';
                            details += '<div>' + data.users.first_name + ' ' + data.users.last_name + '</div>';
                            details += '</div>';
                            details += '</div>';

                            $('#details').html(details);


                        });

                        if (id == 1) {
                            $('.hdtitle').text('Car with Driver');
                        }
                        if (id == 0) {
                            $('.hdtitle').text('Car Available');
                        }
                        if (id == '-1') {
                            $('.hdtitle').text('Car Accident');
                        }

                        //$('#myModal').show();
                        //$('#myModal').modal('show');
                    }
                });
            }
        }

        function createPopup() {
            var id = $(this).data('id');
            Popup($('#details').html(), $('#head_part').html());
        }

        function Popup(data, head) {
            var mywindow = window.open('', 'my div', 'height=400,width=600');
            mywindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write(head);
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);

            mywindow.document.write('</body></html>');

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10

            mywindow.print();
            mywindow.close();

            return true;
        }

        function fade(obj, opacity) {

            $(obj).stop().fadeTo('fast', opacity);
        }

        function logout(){ 

            localStorage.clear();
             location.href='/ottohub/login.html';

            return false;
        }
    </script>
    <style>
        .filters:nth-of-type(1){
          flex-basis: 60%;
          -webkit-flex-basis: 60%;
        }
        .filters:nth-of-type(2){
          flex-basis: 40%;
          -webkit-flex-basis: 40%;
        }
        .filters .filter{
            width:50%;
        }
    </style>
</head>
<body class="dt-example">
<section style="display:block;width:100%;float:left;background-color:#333e48;padding-top:12px;padding-bottom:12px;margin-bottom:30px">
    <header style=" margin: 0 auto;max-width: 1100px;">
        <h1 style="float:left;color:white;margin:0;font-size:2.9em;">Ottohub</h1>

        <img style="float:right;" src="images/otto_logo_gray.JPG" width="150px" height="80px"/>
        <div style="float:right;margin:20px 40px;">
            <a href="referrals.php"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">REFERRALS</a>
            <a href="accidents.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">ACCIDENTS</a>
            <a href="index.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">DRIVERS</a>
            <a href="cars.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">CARS</a>
            <a style="color:white;font-size:1.4em;text-decoration:underline;" href="javascript:void(0);"
                 onclick="logout();">LogOut</a>
        </div>
    </header>
</section>
<div class="clearfix"></div>
<div class="container">
    <section>
        <div class="info" style="display:none"></div>
        <ul class="tabs" style="margin-top:20px">
            <li data-id="example2" class="active">Referrals</li>
        </ul>

        <div class="tabs">
            <div class="css"><!-- Referrals table structure -->
                <div id="filter_car" class="filter" style="display:none;width:100%">
                </div>
                <div id="all_car" class="filter" style="width:100%">
                    <table id="referrals" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Car</th>
                            <th>Scheme</th>
                            <th>Date</th>
                            <th>Source</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Car</th>
                            <th>Scheme</th>
                            <th>Date</th>
                            <th>Source</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
// DataTables PHP library
include("php/DataTables.php");

//echo date("d-m-Y", strtotime("+6 week"));
//$sixWeek = time() + (7 * 6 * 24 * 60 * 60);// 7 days; 24 hours; 60 mins; 60 secs

$now=date('Y-m-d');
$result_user = $db->sql('select pickup_date from users where how_to_find=" FarePilot " and site="3"');
$lastresult = $result_user->fetchAll();
    if (count($lastresult) > 0) {
       // foreach ($lastresult as $k => $v) {
        foreach ($lastresult as $k) {
           //echo "{$k} => {$v['pickup_date']} ";
           $pickup_date = $k['pickup_date'];          
           //echo 'Pick Up Date :'.$pickup_date.'<br>';
           $sixthweek = date('Y-m-d', strtotime($pickup_date. ' + 49 days'));
           //echo '6th Week :'.$sixthweek.'<br>';
           if ($sixthweek==$now){
                //sending mail
                $to = 'richard@ottocar.co.uk';
                $from = 'info@ottocar.co.uk';
                $subject = 'FarePilot Payment';

                $headers = "MIME-Version: 1.0" . "\r\n";
                //$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                $headers.= "Content-Type: text/plain; charset=ISO-8859-1\r\n"; 
                //$headers.= "X-Priority: 1\r\n"; 
                $headers .= "From:".$from."\r\n";
                $headers .= "Reply-To:".$from."\r\n";
                //$headers .= 'Cc: waqas@ottocar.co.uk' . "\r\n";
                $headers .= 'Cc: testeuphern@gmail.com' . "\r\n";
                $result_user = $db->sql('select * from users where pickup_date="'.$pickup_date.'"');
                 $lastresult = $result_user->fetchAll();
                 $user = $lastresult[0];
                 //print_r($user['first_name']);
                $message = 'This is a automated message to say that Mr. '.$user["first_name"].' has been on the road for 7 weeks and FarePilot are due a £100 payment. Please check driver details on Prius Rent Sheet and Otto Hub';
                //echo $message;
                $sendmail = mail($to,$subject,$message,$headers);
                if($sendmail){
                 //echo  "Email send Successfully<br>";
                }else{
                  //echo "mail sending failed<br>";
                }
            }
        }
    }
?>
</body>
</html>
