<?php
ini_set('display_errors', 1);
error_reporting('E_ALL');
//load the database configuration file
include 'libs/DBActiveRecord.php';
$db = new DB_active_record();

if(isset($_POST['importSubmit'])){
    
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');

    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode

            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            //$line = fgetcsv($csvFile);VehicleRegistration
            //parse data from csv file line by line
            $data=array();
            $csv_st=$db->select('csv')->from('csv_state')->where(array('csv'=>$_POST['csvsource']))->get()->result_array();
            //var_dump($csv_st);
            @$file_name = $_FILES['file']['name'];
            if(empty($csv_st)){
              $db->insert_data('csv_state',array('csv'=>$_POST['csvsource'],'csv_name'=>$file_name));
              
                while(($line = fgetcsv($csvFile)) !== FALSE){
                     //$data=array_combine($csv_fields, $line);
                  include 'csv_sc/'.$_POST['csvsource'].'.php';
                }  
            $qstring = '?status=succ';
            }else{
                
                $qstring = '?status=alris';
            }

            //close opened csv file
            fclose($csvFile);
            
           
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
}

//redirect to the listing page
header("Location: import.php".$qstring);