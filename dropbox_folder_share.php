<?php
	//Dropbox Access Token
	$access_token = 'LZGo3bViDbAAAAAAAAAAFy4TSVcBpnehWszlJYi6WYeLoNbV63X3Dx5JmZqQ1zmS';
	
	//die(json_encode(array('msg'=>'here')));
	
	if( !empty($_POST['share_folder']) ) {
		
		
		//Folder path which will be shared
		$dropbox_folder_path = '/Ottohub/Drivers/'.$_POST['share_folder'];
		
		//Set parameters for "Dropbox-API-Arg" header
		$dropbox_api_arg = array(
			'path' => $dropbox_folder_path,
			'member_policy' => 'team',
			'acl_update_policy' => 'editors',
			'shared_link_policy' => 'anyone',
			'force_async' => false
		);
		
		// Set headers
	  	$headers = array(
	  		'User-Agent: api-explorer-client',
	  		'Authorization: Bearer '.$access_token, // This is the access token for a particular user
	  		'Content-Type: application/json',
	  	);
	  	
	  	// Get cURL resource
		$curl = curl_init();
	
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_URL => 'https://api.dropboxapi.com/2/sharing/share_folder',
				CURLOPT_HTTPHEADER => $headers,
				CURLOPT_POST => TRUE,
				//CURLOPT_BINARYTRANSFER => TRUE,
				CURLOPT_POSTFIELDS => stripslashes(json_encode($dropbox_api_arg))
			)
		);
	
		// Send the request & save response to $resp
		$res = curl_exec($curl);
		// echo $res;
	
		// Convert the json response into array
		$result = json_decode($res, 1);
	
		/*echo "<pre>";
		print_r($result);
		echo "</pre>";*/
		
		$shared_folder_id = 0;
		if( !empty($result['.tag']) && $result['.tag'] == 'complete' ){
			$shared_folder_id = $result['shared_folder_id'];
		} else if( !empty($result['error']['bad_path']['shared_folder_id']) ) {
			$shared_folder_id = $result['error']['bad_path']['shared_folder_id'];
		}
		
		if( !empty($shared_folder_id) && $shared_folder_id != 0 ){
			
			$emails = isset($_POST['emails']) ? $_POST['emails'] : '' ;
			$operation = isset($_POST['operation']) ? $_POST['operation'] : 'editor' ;
			
			if( $emails != '' ) {
				$emails = explode(';', $emails);
				
				$members = array();
				foreach($emails as $key=>$email){
					$member = array(
						"member" => array(
							".tag" => "email",
                					"email" => $email
						),
						"access_level" => array(
							".tag" => $operation
						)
					);
					
					array_push($members, $member);
				}
				
				/*echo "<pre>";
				print_r($members);
				echo "</pre>";*/
				
				if( count($members) > 0 ) {
					$custom_message = isset($_POST['custom_message']) ? $_POST['custom_message'] : '' ;
					
					//Set parameters for "Dropbox-API-Arg" header
					$dropbox_api_arg = array(
						'shared_folder_id' => $shared_folder_id,
						'members' => $members,
						'quiet' => false,
						'custom_message' => $custom_message
					);
					
					// Set headers
				  	$headers = array(
				  		'User-Agent: api-explorer-client',
				  		'Authorization: Bearer '.$access_token, // This is the access token for a particular user
				  		'Content-Type: application/json',
				  	);
				  	
				  	// Get cURL resource
					$curl = curl_init();
					
					/*echo "<pre>";
					print_r($dropbox_api_arg);
					echo json_encode($dropbox_api_arg);
					echo "</pre>";*/
				
					// Set some options - we are passing in a useragent too here
					curl_setopt_array($curl, array(
							CURLOPT_RETURNTRANSFER => TRUE,
							CURLOPT_URL => 'https://api.dropboxapi.com/2/sharing/add_folder_member',
							CURLOPT_HTTPHEADER => $headers,
							CURLOPT_POST => TRUE,
							//CURLOPT_BINARYTRANSFER => TRUE,
							CURLOPT_POSTFIELDS => stripslashes(json_encode($dropbox_api_arg))
						)
					);
				
					// Send the request & save response to $resp
					$res = curl_exec($curl);
					// echo $res;
				
					// Convert the json response into array
					$result = json_decode($res, 1);
					
					/*echo "<pre>";
					print_r($result);
					echo "</pre>";*/
					
					//die(json_encode($result));
					if( empty($result) ) {
						$responseData = array(
							'status'=>1,
							'message'=>"Successful",
							'data'=>$result
						);
						die(json_encode($responseData));
					} else {
						$responseData = array(
							'status'=>0,
							'message'=>"Oops! An error occured"
						);
						die(json_encode($responseData));
					}
					
				} else {
					$responseData = array(
						'status'=>0,
						'message'=>"Oops! An error occured"
					);
					die(json_encode($responseData));
				}
				
			} else {
				$responseData = array(
					'status'=>0,
					'message'=>"Email cannot be blank"
				);
				die(json_encode($responseData));
			}
		} else {
			$responseData = array(
				'status'=>0,
				'message'=>"Oops! An error occured"
			);
			die(json_encode($responseData));
		}
		
		
		//die(json_encode($result));
		
	}
	
	

?>