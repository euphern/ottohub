<!DOCTYPE html>
<html>
<head id="head_part">
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>OttoHub beta</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="css/editor.dataTables.css">

    <link rel="stylesheet" type="text/css" href="css/otto.css">
    <!--  <link rel="stylesheet" type="text/css" href="css/red.css">
     <link rel="stylesheet" type="text/css" href="css/blue.css">  -->
    <link rel="stylesheet" type="text/css" href="examples/resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="examples/resources/demo.css">
    <script src="http://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>


    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js">
    </script>
    <script type="text/javascript" src="http://momentjs.com/downloads/moment.js"></script>
    <script type="text/javascript" language="javascript" src="js/dataTables.editor.js">
    </script>
    <script type="text/javascript" language="javascript" src="examples/resources/syntax/shCore.js">
    </script>
    <script type="text/javascript" language="javascript" src="examples/resources/demo.js">
    </script>

    <script type="text/javascript" language="javascript" src="js/otto.js">
    </script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" class="init">

      var host= $(location).attr('hostname');
      if(host=='192.168.200.13'){
         var  baseurl_ajax='http://192.168.200.13/otohub/examples/';
         var  baseurl_org='http://192.168.200.13/otohub/';
        }else{
         var  baseurl_ajax='http://ottohub.euphern.com/demo/examples/';
         var  baseurl_org='http://ottohub.euphern.com/demo/'; 
        }

        $(function(){

          var host= $(location).attr('hostname');
          if(host=='192.168.200.13'){
             var  baseurl_ajax='http://192.168.200.13/otohub/examples/';
             var  baseurl_org='http://192.168.200.13/otohub/';
            }else{
             var  baseurl_ajax='http://ottohub.euphern.com/demo/examples/';
             var  baseurl_org='http://ottohub.euphern.com/demo/'; 
            } 
        })


    var editor; // use a global for the submit and return data rendering in the examples
    var editor1;
    var editor2;
    var editor3;
    var editor4;
    var base_url = location.href;
    var url = base_url.split('hub');
  
        function fade(obj, opacity) {
            $(obj).stop().fadeTo('fast', opacity);
        }

        function logout(){ 

            localStorage.clear();
             location.href='/ottohub/login.html';

            return false;
        }
    </script>
  <style>
  .filtersContainer {
    display: block!important;
    width: 100%;
}
    .filters:nth-of-type(1){
      flex-basis: 60%;
      -webkit-flex-basis: 60%;
    }
    .filters:nth-of-type(2){
      flex-basis: 40%;
      -webkit-flex-basis: 40%;
    }
    .filters .filter{
        width:50%;
    }
    .msg{ width: 100%; clear: both; float: left; }
    input[type="file"] {display: inline;}

  </style>
  <script type="text/javascript">
      function import_process(){
        $.ajax({
                        type: "POST",
                        url: "process.php",
                        data: { process: 1},
                        success: function(data){
                            //$('.success').fadeIn(1000);
                            
                             //location.reload();
                             //window.location.replace("/?status=importsucc");
                             window.location.href = baseurl_org+"import.php/?status=importsucc";
                        }
                            /*success : function (serverResponse) { alert('mail sent successfully.'); },
                            error   : function (jqXHR, textStatus, errorThrown) {alert('error sending mail');}*/
                    });
      }
  </script>
</head>
<?php
//load the database configuration file
    include 'libs/DBActiveRecord.php';
    $db = new DB_active_record();

?>
<body class="dt-example">
<section style="display:block;width:100%;float:left;background-color:#333e48;padding-top:12px;padding-bottom:12px;margin-bottom:30px">
    <header style=" margin: 0 auto;max-width: 1100px;">
        <h1 style="float:left;color:white;margin:0;font-size:2.9em;">Ottohub</h1>

        <img style="float:right;" src="images/otto_logo_gray.JPG" width="150px" height="80px"/>
        <div style="float:right;margin:20px 40px;">
            <a href="referrals.php"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">REFERRALS</a>
            <a href="accidents.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">ACCIDENTS</a>
            <a href="index.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">DRIVERS</a>
            <a href="cars.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">CARS</a>
            <a style="color:white;font-size:1.4em;text-decoration:underline;" href="javascript:void(0);"
                 onclick="logout();">LogOut</a>
        </div>
    </header>
</section>
<div class="clearfix"></div>
<div class="container">
    <section>
        <div class="info" style="display:none"></div>
        <ul class="tabs" style="margin-top:20px">
            <li data-id="example2" class="active">CSV IMPORT</li>
            
        </ul>

        <div class="tabs">
            <div class="css"><!-- Stock table structure -->
                <section class="filtersContainer" style="min-height: 200px">
                    <?php 
                     if(!empty($_GET['status'])){
                        switch($_GET['status']){
                            case 'succ':
                                $statusMsgClass = 'alert-success';
                                $statusMsg = 'Csv data has been inserted successfully.';
                                break;
                            case 'importsucc':
                                $statusMsgClass = 'alert-success';
                                $statusMsg = 'Csv data has been Imported successfully.';
                                break;
                            case 'err':
                                $statusMsgClass = 'alert-danger';
                                $statusMsg = 'Some problem occurred, please try again.';
                                break;
                            case 'alris':
                                $statusMsgClass = 'alert-danger';
                                $statusMsg = 'Csv already inserted, please try another.';
                                break;    
                            case 'invalid_file':
                                $statusMsgClass = 'alert-danger';
                                $statusMsg = 'Please upload a valid CSV file.';
                                break;
                            default:
                                $statusMsgClass = '';
                                $statusMsg = '';
                         }
                    }
                    if(!empty($statusMsg)){
                            echo '<span class="msg alert '.$statusMsgClass.'">'.$statusMsg.'</span>';

                     } ?>
                       <form style="width: 100%;" action="importData.php" method="post" enctype="multipart/form-data" id="importFrm"> 

                       <h2><b>Select CSV File source and choose file </b></h2>
                        <select name="csvsource">
                           <option value="csv_1"> CSV Source 1</option>
                           <option value="csv_2"> CSV Source 2</option>
                           <option value="csv_3"> CSV Source 3</option>
                           <!--<option value="csv_4"> CSV Source 4</option> -->
                        </select>
                        <input type="file" name="file" />
                        <input type="submit" class="btn btn-primary" name="importSubmit" value="IMPORT">
                    </form>
                    <h4>Uploaded Files</h4>
                   <?php $csv_st=$db->select('*')->from('csv_state')->get()->result_array();
                  
                   if(count($csv_st)>0){
                       echo '<ul>';
                       foreach ($csv_st as $key => $value) {
                          echo '<li>'.$value['csv_name'].'</li>';
                       }
                       echo '</ul>';
                     }
            //var_dump($csv_st);
            
            if(count($csv_st)>=3){
                echo  '<hr><h2> 3 csv files are successfully inserted, please process Now ! </h2><a href="javascript:void(0);" onclick="import_process();" class="btn btn-primary" >Import Process</a>';
            }

            ?> 
                </section>
               
            </div>
            
        </div>
    </section>
</div>
</body>
</html>