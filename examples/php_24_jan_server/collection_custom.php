<?php


/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library


// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;




$action=$_REQUEST['action'];

if(function_exists($action)){
	if( $_REQUEST['id'] ) {
		$action( $_REQUEST['id'] );

	}/* else if( $_REQUEST['filter'] ) {
		$action( $_REQUEST['filter'] );

	}*/ else{  
		$action();
	}
	//$action($_REQUEST['id'] ? $_REQUEST['id'] : '');
}


function total(){

	include( "../../php/DataTables.php" );

	// Build our Editor instance and process the data coming from _POST
	$result=$db->sql('select site,count(*) as totaluser from users where user_type="driver" group by site');
	$lastresult=$result->fetchAll();
	echo json_encode($lastresult);

}

function totalavl(){
	include( "../../php/DataTables.php" );

	// Build our Editor instance and process the data coming from _POST
	$result=$db->sql('SELECT count(*) as totaluser from cars where availability=0');
	$lastresult=$result->fetchAll();
	echo json_encode($lastresult);
}

function totalavlcar(){
	include( "../../php/DataTables.php" );

	// Build our Editor instance and process the data coming from _POST
	$result=$db->sql('SELECT availability,count(availability) as totaluser from cars where 1 group by availability');
	$lastresult=$result->fetchAll();
	echo json_encode($lastresult);
}

function totalCandidateFilterWise(){
	include( "../../php/DataTables.php" );

	$pass = $db->sql("SELECT count(`filter`) AS filter FROM `users` WHERE `filter`='Pass' AND `user_type`='driver'");
	$total_pass = $pass->fetch();
	
	$waiting = $db->sql("SELECT count(`filter`) AS filter FROM `users` WHERE `filter`='Waiting' AND 
		`user_type`= 'candidate'");
	$total_waiting = $waiting->fetch();

	$fail = $db->sql("SELECT count(`filter`) AS filter FROM `users` WHERE `filter`='Fail' AND `user_type`='candidate'");
	$total_fail = $fail->fetch();

	$result = array(
		'pass' => $total_pass['filter']?$total_pass['filter']:'0',
		'waiting' => $total_waiting['filter']?$total_waiting['filter']:'0',
		'fail' => $total_fail['filter']?$total_fail['filter']:'0',
	);

	echo json_encode($result);
}

function showtabledata($id){

	include( "../../php/DataTables.php" );
	
	// Build our Editor instance and process the data coming from _POST
	Editor::inst( $db, 'users' )
		->fields(
			Field::inst( 'first_name' ),
			Field::inst( 'last_name' ),
			Field::inst( 'phone' ),
			Field::inst( 'city' ),
			Field::inst( 'image' )
				->setFormatter( 'Format::ifEmpty', null )
				->upload( Upload::inst( $_SERVER['DOCUMENT_ROOT'].'/upload/__ID__.__EXTN__' )
					->db( 'files', 'id', array(
						'filename'    => Upload::DB_FILE_NAME,
						'filesize'    => Upload::DB_FILE_SIZE,
						'web_path'    => Upload::DB_WEB_PATH,
						'system_path' => Upload::DB_SYSTEM_PATH
					) )
					->validator( function ( $file ) {
						return$file['size'] >= 50000 ?
							"Files must be smaller than 50K" :
							null;
					} )
					->allowedExtensions( [ 'png', 'jpg', 'gif' ], "Please upload an image" )
				)
		)
		->where('site',$id)
		->process( $_POST )
		->json();
}








