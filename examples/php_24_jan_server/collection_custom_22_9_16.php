<?php


/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library


// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;




$action=$_REQUEST['action'];
if(function_exists($action)){
	$action($_REQUEST['id']?$_REQUEST['id']:'');
}


function total(){

include( "../../php/DataTables.php" );




	// Build our Editor instance and process the data coming from _POST
   $result=$db->sql('select site,count(*) as totaluser from users where 1 group by site');
   $lastresult=$result->fetchAll();
	echo json_encode($lastresult);

}

function totalavl(){
	include( "../../php/DataTables.php" );




	// Build our Editor instance and process the data coming from _POST
   $result=$db->sql('select count(*) as totaluser from cars where availability=0');
   $lastresult=$result->fetchAll();
	echo json_encode($lastresult);
}


function showtabledata($id){


	include( "../../php/DataTables.php" );




	
// Build our Editor instance and process the data coming from _POST
Editor::inst( $db, 'users' )
	->fields(
		Field::inst( 'first_name' ),
		Field::inst( 'last_name' ),
		Field::inst( 'phone' ),
		Field::inst( 'city' ),
		Field::inst( 'image' )
			->setFormatter( 'Format::ifEmpty', null )
			->upload( Upload::inst( $_SERVER['DOCUMENT_ROOT'].'/upload/__ID__.__EXTN__' )
				->db( 'files', 'id', array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH,
					'system_path' => Upload::DB_SYSTEM_PATH
				) )
				->validator( function ( $file ) {
					return$file['size'] >= 50000 ?
						"Files must be smaller than 50K" :
						null;
				} )
				->allowedExtensions( [ 'png', 'jpg', 'gif' ], "Please upload an image" )
			)
	)
	->where('site',$id)
	->process( $_POST )
	->json();


}








