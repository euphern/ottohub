<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
if($_POST){

 $id=0;
		if($_POST['data']){
			foreach($_POST['data'] as $k=>$v){
				$c=explode('_',$k);
				$row[$k]=$c[1];
				$id=$k;
			}

			

				if(!empty($_POST['data'][$k]['users']['car'])){
			$result=$db->sql('update cars set availability='.$row[$id].' where  reg= "'.$_POST['data'][$k]['users']['car'].'"');
				}

		}
}

Editor::inst( $db, 'users' )
	->fields(
		Field::inst( 'users.first_name' ),
		Field::inst( 'users.last_name' ),
		Field::inst( 'users.phone' ),
		Field::inst( 'users.site' )
			->options( 'sites', 'id', 'name' ),
		Field::inst( 'sites.name' ),
		Field::inst( 'users.car' )
			->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
			Field::inst('cars.type')
			
		
	)
	->join(
		Mjoin::inst( 'files' )
			->link( 'users.id', 'users_files.user_id' )
			->link( 'files.id', 'users_files.file_id' )
			->fields(
				Field::inst( 'id' )
					->upload( Upload::inst( $_SERVER['DOCUMENT_ROOT'].'/upload/__ID__.__EXTN__' )
						->db( 'files', 'id', array(
							'filename'    => Upload::DB_FILE_NAME,
							'filesize'    => Upload::DB_FILE_SIZE,
							'web_path'    => Upload::DB_WEB_PATH,
							'system_path' => Upload::DB_SYSTEM_PATH
						) )
						->validator( function ( $file ) {
							return$file['size'] >= 50000 ?
								"Files must be smaller than 50K" :
								null;
						} )
						->allowedExtensions( [ 'png', 'jpg' ], "Please upload an image" )
					)
			)
	)
	->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
	->leftJoin( 'cars', 'cars.reg', '=', 'users.car' )
	
	->process( $_POST )
	->json();
