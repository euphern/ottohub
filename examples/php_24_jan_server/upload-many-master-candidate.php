<?php



/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;


	

// Build our Editor instance and process the data coming from _POST
if($_POST){

 $id=0;
		if(@$_POST['data'][0]){


		}else if($_POST['action']=='remove'){

				if(isset($_POST['data'])){
					foreach(@$_POST['data'] as $k=>$v){
						$c=explode('_',$k);
						$row[$k]=@$c[1];
						$id=$k;
                                               $user_id=@$c[1];

					}

					

					$car_reg=@$_POST['data'][$id]['cars']['reg'];
						if($car_reg){
			$result_1=$db->sql('update cars set `availability`="0" , `customer_id`="0" , `condition`="used"  where `reg`= "'.$car_reg.'"');
			
				       }
				}


		}
		else if($_POST['action']=='edit'){
				if(isset($_POST['data'])){
					foreach(@$_POST['data'] as $k=>$v){
						$c=explode('_',$k);
						$row[$k]=@$c[1];
						$id=$k;
                                                $user_id=@$c[1];

					}



					
						
			}

		}
}

if(isset($_GET['action'])){
	$id=$_GET['id'];

			if($id=='pass'){



				Editor::inst( $db, 'users' )
				->fields(
					Field::inst( 'users.first_name' ),
					Field::inst( 'users.last_name' ),
                                        Field::inst( 'users.address' ), 
                                        Field::inst( 'users.dob' ),
			                Field::inst( 'users.email' ),
			                Field::inst( 'users.mobile' ),
					Field::inst( 'users.phone' ), 
					Field::inst( 'users.national_insurance' ),

			                Field::inst( 'users.residentamount' ), 
                                        Field::inst( 'users.licencetype' ), 
                                        Field::inst( 'users.datetestpassed' ), 
                                        Field::inst( 'users.taxilicencenumber' ), 
                                        Field::inst( 'users.taxilicenceage' ), 

                                        Field::inst( 'users.motoroffence' ), 
                                        Field::inst( 'users.criminalconviction' ), 
                                        Field::inst( 'users.motorinsurance' ), 
                                        Field::inst( 'users.claims' ), 

			                Field::inst( 'users.user_type' ), 
                                        Field::inst( 'users.blacklist' ),
                    Field::inst( 'users.candidate_comment' ),
                                         Field::inst( 'users.notes' ), 
                                        Field::inst( 'users.filter' ), 

			                Field::inst( 'users.how_to_find' ), 
							Field::inst( 'users.site' )
								->options( 'sites', 'id', 'name' ),
							Field::inst( 'sites.name' ),
							Field::inst( 'users.car' )
								//->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
							->options( 'car_type', 'id','model'  ),
							Field::inst('car_type.model'),
							Field::inst('car_type.id')
					
				)
->join(
                Mjoin::inst('files')
                    ->link('users.id', 'users_files.user_id')
                    ->link('files.id', 'users_files.file_id')
                    ->fields(
                        Field::inst('id')
                            ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')
                                ->db('files', 'id', array(
                                    'filename' => Upload::DB_FILE_NAME,
                                    'filesize' => Upload::DB_FILE_SIZE,
                                    'web_path' => Upload::DB_WEB_PATH,
                                    'system_path' => Upload::DB_SYSTEM_PATH
                                ))
                                ->validator(function ($file) {
                                    return $file['size'] >= 1024000 ?
                                        "Files must be smaller than 1MB" :
                                        null;
                                })
                                ->allowedExtensions(['png', 'jpg'], "Please upload an image")
                            )
                    )
            )
				->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
				->leftJoin( 'car_type', 'car_type.id', '=', 'users.car' )
				->where('users.filter',$id)
				->where('users.user_type','driver')
				->where('filter','pass')
				->process( $_POST )
				->json();



			}else if($id=='all'){

					Editor::inst( $db, 'users' )
				->fields(
					Field::inst( 'users.first_name' ),
					Field::inst( 'users.last_name' ),
                                        Field::inst( 'users.address' ), 
                                        Field::inst( 'users.dob' ),
			                Field::inst( 'users.email' ),
			                Field::inst( 'users.mobile' ),
			                Field::inst( 'users.national_insurance' ),
					Field::inst( 'users.phone' ), 
			                Field::inst( 'users.residentamount' ), 
                                        Field::inst( 'users.licencetype' ), 
                                        Field::inst( 'users.datetestpassed' ), 
                                        Field::inst( 'users.taxilicencenumber' ), 
                                        Field::inst( 'users.taxilicenceage' ), 
                                        Field::inst( 'users.motoroffence' ), 
                                        Field::inst( 'users.criminalconviction' ), 
                                        Field::inst( 'users.motorinsurance' ), 
                                        Field::inst( 'users.claims' ), 
			                Field::inst( 'users.user_type' ),
                    Field::inst( 'users.candidate_comment' ),
                                        Field::inst( 'users.blacklist' ), 
                                        Field::inst( 'users.notes' ), 
                                        Field::inst( 'users.filter' ), 
			                Field::inst( 'users.how_to_find' ),
							Field::inst( 'users.site' )
								->options( 'sites', 'id', 'name' ),
							Field::inst( 'sites.name' ),
							
							Field::inst( 'users.car' )
								//->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
							->options( 'car_type', 'id','model'  ),
							Field::inst('car_type.model'),
							Field::inst('car_type.id')
					
				)
->join(
                Mjoin::inst('files')
                    ->link('users.id', 'users_files.user_id')
                    ->link('files.id', 'users_files.file_id')
                    ->fields(
                        Field::inst('id')
                            ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')
                                ->db('files', 'id', array(
                                    'filename' => Upload::DB_FILE_NAME,
                                    'filesize' => Upload::DB_FILE_SIZE,
                                    'web_path' => Upload::DB_WEB_PATH,
                                    'system_path' => Upload::DB_SYSTEM_PATH
                                ))
                                ->validator(function ($file) {
                                    return $file['size'] >= 1024000 ?
                                        "Files must be smaller than 1MB" :
                                        null;
                                })
                                ->allowedExtensions(['png', 'jpg'], "Please upload an image")
                            )
                    )
            )
				
				->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
				->leftJoin( 'car_type', 'car_type.id', '=', 'users.car' )
				->where('users.user_type','candidate')
				//->where('cars.car','1')
				->process( $_POST )
				->json();

			}else{
	

			Editor::inst( $db, 'users' )
				->fields(
					Field::inst( 'users.first_name' ),
					Field::inst( 'users.last_name' ),
                                        Field::inst( 'users.address' ), 
                                        Field::inst( 'users.dob' ),
			                Field::inst( 'users.email' ),
			                Field::inst( 'users.mobile' ),
			                Field::inst( 'users.national_insurance' ),
					Field::inst( 'users.phone' ), 
			                Field::inst( 'users.residentamount' ), 
                                        Field::inst( 'users.licencetype' ), 
                                        Field::inst( 'users.datetestpassed' ), 
                                        Field::inst( 'users.taxilicencenumber' ), 
                                        Field::inst( 'users.taxilicenceage' ), 
                                        Field::inst( 'users.motoroffence' ), 
                                        Field::inst( 'users.criminalconviction' ), 
                                        Field::inst( 'users.motorinsurance' ), 
                                        Field::inst( 'users.claims' ), 
			                Field::inst( 'users.user_type' ),
                                        
                                        Field::inst( 'users.blacklist' ),

                    Field::inst( 'users.candidate_comment' ),
                                        Field::inst( 'users.notes' ),  
                                        Field::inst( 'users.filter' ), 
			                Field::inst( 'users.how_to_find' ), 
			     
							Field::inst( 'users.site' )
								->options( 'sites', 'id', 'name' ),
							Field::inst( 'sites.name' ),
							Field::inst( 'users.car' )
								//->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
							->options( 'car_type', 'id','model'  ),
							Field::inst('car_type.model'),
							Field::inst('car_type.id')
					
				)
->join(
                Mjoin::inst('files')
                    ->link('users.id', 'users_files.user_id')
                    ->link('files.id', 'users_files.file_id')
                    ->fields(
                        Field::inst('id')
                            ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')
                                ->db('files', 'id', array(
                                    'filename' => Upload::DB_FILE_NAME,
                                    'filesize' => Upload::DB_FILE_SIZE,
                                    'web_path' => Upload::DB_WEB_PATH,
                                    'system_path' => Upload::DB_SYSTEM_PATH
                                ))
                                ->validator(function ($file) {
                                    return $file['size'] >= 1024000 ?
                                        "Files must be smaller than 1MB" :
                                        null;
                                })
                                ->allowedExtensions(['png', 'jpg'], "Please upload an image")
                            )
                    )
            )
				
				->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
				->leftJoin( 'car_type', 'car_type.id', '=', 'users.car' )
				->where('users.filter',$id)
				->where('users.user_type','candidate')
				//->where('cars.car','1')
				->process( $_POST )
				->json();

			}
			


}else{

			Editor::inst( $db, 'users' )
				->fields(
					Field::inst( 'users.first_name' ),
					Field::inst( 'users.last_name' ),
                                        Field::inst( 'users.dob' ),
			                Field::inst( 'users.email' ),
			                Field::inst( 'users.mobile' ),
					Field::inst( 'users.phone' ),
                     Field::inst( 'users.national_insurance' ),
                                        Field::inst( 'users.residentamount' ),
                                        Field::inst( 'users.licencetype' ),
                                        Field::inst( 'users.datetestpassed' ),
                                        Field::inst( 'users.taxilicencenumber' ),
                                        Field::inst( 'users.taxilicenceage' ),


                                        Field::inst( 'users.motoroffence' ),
                                        Field::inst( 'users.criminalconviction' ),
                                        Field::inst( 'users.motorinsurance' ),
                                        Field::inst( 'users.claims' ),
			                Field::inst( 'users.user_type' ),
                                        Field::inst( 'users.blacklist' ),

                    Field::inst( 'users.candidate_comment' ),
                                        Field::inst( 'users.notes' ),
                                        Field::inst( 'users.filter' ),
			                Field::inst( 'users.how_to_find' ),
							Field::inst( 'users.address' ),
							Field::inst( 'users.site' )
								->options( 'sites', 'id', 'name' ),
							Field::inst( 'sites.name' ),
							Field::inst( 'users.car' )
								//->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
							->options( 'car_type', 'id','model'  ),
							Field::inst('car_type.model'),
							Field::inst('car_type.id')

				)
->join(
                Mjoin::inst('files')
                    ->link('users.id', 'users_files.user_id')
                    ->link('files.id', 'users_files.file_id')
                    ->fields(
                        Field::inst('id')
                            ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')
                                ->db('files', 'id', array(
                                    'filename' => Upload::DB_FILE_NAME,
                                    'filesize' => Upload::DB_FILE_SIZE,
                                    'web_path' => Upload::DB_WEB_PATH,
                                    'system_path' => Upload::DB_SYSTEM_PATH
                                ))
                                ->validator(function ($file) {
                                    return $file['size'] >= 1024000 ?
                                        "Files must be smaller than 1MB" :
                                        null;
                                })
                                ->allowedExtensions(['png', 'jpg'], "Please upload an image")
                            )
                    )
            )

				->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
				->leftJoin( 'car_type', 'car_type.id', '=', 'users.car' )
				->where('users.user_type','candidate')
				//->where('cars.car','1')
				->process( $_POST )
				->json();

}
