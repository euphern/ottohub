<?php
// INCLUDE THE phpToPDF.php FILE
require('phpToPDF.php');


$dir_name = $_POST['dir_name'] ? $_POST['dir_name'] : 'pdfs';
$oldmask = umask(0);
mkdir($_SERVER['DOCUMENT_ROOT'] . '/otto/pdf/' . $dir_name, 0777);
umask($oldmask);

$file_name = $_POST['file_name'] ? $_POST['file_name'] : 'sada_new.pdf';

$url = $_POST['url'] ? $_POST['url'] : 'https://www.google.co.in/';


$pdf_options = array(
    "source_type" => 'url',
    "source" => $url,
    "action" => 'save',
    "save_directory" => $_SERVER['DOCUMENT_ROOT'] . '/otto/pdf/' . $dir_name,
    "file_name" => $file_name
);
//echo "<pre>";
//print_r($pdf_options);
//echo "</pre>";
// CALL THE phptopdf FUNCTION WITH THE OPTIONS SET ABOVE
phptopdf($pdf_options);


$access_token = 'LZGo3bViDbAAAAAAAAAAFy4TSVcBpnehWszlJYi6WYeLoNbV63X3Dx5JmZqQ1zmS';

$dropbox_file_path = '/ottohub/drivers/' . $dir_name . '/' . $file_name;

$image_path = $_SERVER['DOCUMENT_ROOT'] . '/otto/pdf/' . $dir_name . '/' . $file_name;

//Set parameters for "Dropbox-API-Arg" header
$dropbox_api_arg = array(
    'path' => $dropbox_file_path,
    'mode' => 'add', //There are four value for mode. blank, add, overwrite, update
    'autorename' => true, // There are two value for autorename. true, false
    'mute' => false, // There are two value for mute. true, false
);

/*echo "<pre>";
echo json_encode($dropbox_api_arg);
echo "</pre>";*/

// Set headers
$headers = array(
    'User-Agent: api-explorer-client',
    'Authorization: Bearer ' . $access_token, // This is the access token for a particular user
    'Content-Type: application/octet-stream',
    'Dropbox-API-Arg: ' . stripslashes(json_encode($dropbox_api_arg)) // This is the array set at the very begining
);

/*echo "<pre>";
print_r($headers);
echo "</pre>";*/


// Open the file which is to be uploaded
@chmod('0777', $image_path);
$f = fopen($image_path, "rb");

// Get cURL resource
$curl = curl_init();

// Set some options - we are passing in a useragent too here
curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_URL => 'https://content.dropboxapi.com/2/files/upload',
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_POST => TRUE,
        //CURLOPT_BINARYTRANSFER => TRUE,
        CURLOPT_POSTFIELDS => fread($f, filesize($image_path))
    )
);

// Send the request & save response to $resp
$res = curl_exec($curl);

// Convert the json response into array
$result = json_decode($res, 1);

/*echo "<pre>";
print_r($result);
echo "</pre>";*/

// Close opened file
fclose($f);

// Close request to clear up some resources
curl_close($curl);


// OPTIONAL - PUT A LINK TO DOWNLOAD THE PDF YOU JUST CREATED
echo '../../pdf/' . $dir_name . '/' . $file_name;
?> 
