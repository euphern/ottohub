<?php


/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library


// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

$action = $_GET['action'];
if (function_exists($action)) {
    $action($_REQUEST['id'] ? $_REQUEST['id'] : '', $_REQUEST['aid'] ? $_REQUEST['aid'] : '');
}


function showtabledata($id, $aid)
{   

    include("../../php/DataTables.php");

    if ($id) { 

        if ($id == 4) {//data from filter click
            if ($aid == 4) {//available click
                $id = 0;
            } 
            //else if($aid == 1){//with driver click

            //} 
            else
            {
             $id = $aid;
            }

            if ($aid == 9) {
                // Build our Editor instance and process the data coming from _POST

                Editor::inst($db, 'cars')
                    ->fields(
                        Field::inst('cars.reg'),
                        Field::inst('cars.plan'),
                        Field::inst('cars.type')
                            ->options('car_type', 'id', 'model'),
                        Field::inst('cars.condition'),
                        Field::inst('car_type.model'),
                        Field::inst('cars.availability')


                    )
                    ->leftJoin('car_type', 'cars.type', '=', 'car_type.id')
                    ->where('cars.availability', 9)
                    ->where('cars.car', '0')
                    //->groupby('cars.reg')
                    ->process($_POST)
                    ->json();
            } 
           else {
                Editor::inst($db, 'cars')
                    ->fields(
                        Field::inst('cars.reg'),
                        Field::inst('cars.plan'),
                        Field::inst('cars.type')
                            ->options('car_type', 'id', 'model'),
                        Field::inst('cars.condition'),
                        Field::inst('car_type.model'),
                        Field::inst('cars.availability')
                    )
                    ->leftJoin('car_type', 'cars.type', '=', 'car_type.id')
                    ->where('cars.availability', $id)
                    ->where('cars.car', '0')
                    //->groupby('cars.reg')
                    ->process($_POST)
                    ->json();
            }
        } else {


            // Build our Editor instance and process the data coming from _POST
            Editor::inst($db, 'cars')
                ->fields(
                    Field::inst('cars.reg'),
                    Field::inst('cars.type'),
                    Field::inst('cars.plan'),
                    Field::inst('cars.availability')
                        ->options('users', 'id', 'car'),
                    Field::inst('users.first_name'),
                    Field::inst('users.last_name'),
                    Field::inst('users.site'),
                    //->options( 'sites', 'id', 'name' ),
                    //Field::inst( 'sites.name' ),
                    Field::inst('users.car')

                )

                ->leftJoin('users', 'cars.reg', '=', 'users.car')
                //->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
                ->where('users.site', $id)
                ->where('cars.car', '0')
                //->groupby('cars.reg')
                ->process($_POST)
                ->json();
        }
    } else { 

        if (@$_POST['action'] == 'edit') { 


            foreach (@$_POST['data'] as $k => $v) {
                $c = explode('_', $k);
                $row[$k] = @$c[1];
                $id = $k;
                $user_id = @$c[1];

            }

            /*if (@$_POST['data'][$id]['cars']['condition'] == 'new') {

                $result_user = $db->sql('select * from cars where  id="' . $user_id . '"');
                $lastresult = $result_user->fetchAll();
                if (count($lastresult) > 0) {
                    foreach ($lastresult as $k => $v) {
                        if ($v['customer_id']) {
                            $result_1 = $db->sql('update users set `site`="4"  where  id="' . $v['customer_id'] . '"');
                        }
                    }
                }
            }*/
        }
        
        if (@$_POST['action'] == 'remove') { 
         
         
         foreach (@$_POST['data'] as $k => $v) {
                $c = explode('_', $k);
                $row[$k] = @$c[1];
                $id = $k;
                $user_id = @$c[1];

            }
         
         
         /*$result_user = $db->sql('select * from cars where  id="' . $user_id . '"');
            $lastresult = $result_user->fetchAll(); 
            if (count($lastresult) > 0) {
                foreach ($lastresult as $k => $v) {   
                    $db->sql('update users set `site`="1" where car="' . $v['reg'] . '"');
                }
            }*/ 
        }

        // Build our Editor instance and process the data coming from _POST
        Editor::inst($db, 'car_type')
        ->fields(
            Field::inst('car_type.frequency'),
            Field::inst('car_type.rental_number'),
            Field::inst('car_type.rental_price'),
            Field::inst('car_type.nett'),
            Field::inst('car_type.vat'),
            Field::inst('car_type.gross'),
            Field::inst('car_type.ft_deposit'),
            Field::inst('car_type.pt_deposit'),
            Field::inst('car_type.model')
        )
        //->leftJoin('users', 'cars.reg', '=', 'users.car')
        //->leftJoin('car_type', 'cars.type', '=', 'car_type.id')
        //->where('cars.car', '0')
        ->process($_POST)
        ->json();
    }
}







