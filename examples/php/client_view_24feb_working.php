<?php

// DataTables PHP library
include_once("../../php/DataTables.php");

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;

  //Get data from enquiry tab click
    if (isset($_GET['userid'])) {
        if ($_GET['userid'] != 'undefined') {
            
            $usersid =$_GET['userid'];
            $result = $db->sql('select *  from users where id=' . $usersid);
           $lastresult = $result->fetchAll();
          //print_r($lastresult);
           //echo $lastresult['0']['first_name'];
            //echo $user=json_encode($lastresult);
            //echo $lastresult['first_name'];
        }

        $userid=$lastresult['0']['id'];
		    $first_name=$lastresult['0']['first_name'];
        $last_name=$lastresult['0']['last_name'];
		$address=$lastresult['0']['address'];
		$email=$lastresult['0']['email'];
		$dob=$lastresult['0']['dob'];
		//$dob=explode(' ', $dob);
	    //$date= $dob['1'];
		//$month=str_split($dob['2'],3);
		//$month=strtoupper($month[0]);
		//$dob['3'];
		//$arr = array($date,$month,$dob['3']);
        //echo $dob=implode("-",$arr);
		//echo $today = date("D j F Y "); 
		$national_insurance=$lastresult['0']['national_insurance'];
		$mobile=$lastresult['0']['mobile'];
		$phone=$lastresult['0']['phone'];
	  $residentamount=$lastresult['0']['residentamount'];

		//$licence_number=$lastresult['0']['licence_number'];
		$datetestpassed=$lastresult['0']['datetestpassed'];
		$licencetype=$lastresult['0']['licencetype'];
		$taxilicencenumber=$lastresult['0']['taxilicencenumber'];
                $pcolicencenumber=$lastresult['0']['pcolicencenumber'];
		$taxilicenceage=$lastresult['0']['taxilicenceage'];

		$car_id=$lastresult['0']['car'];
		$claims=$lastresult['0']['claims'];
	  $motoroffence=$lastresult['0']['motoroffence'];
		$criminalconviction=$lastresult['0']['criminalconviction'];
		$motorinsurance=$lastresult['0']['motorinsurance'];

    }
?>
<!DOCTYPE html>
<html>
<head>
<title>Client View Form</title>
<meta content="noindex, nofollow" name="robots">
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="client_view.css">
<link rel="stylesheet" href="../../js/jquery.signature.css">
<!-- https://github.com/brinley/jSignature/blob/master/README.md -->
<style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic);
@import url(http://fonts.googleapis.com/css?family=Arapey);
@import url(http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700);
@import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800);

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

body {
    font-size: 14px;
    line-height: 1.42857143;
    color: #333;
    background-color: #fff;
    padding: .01px;
    font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
}
@media (max-width:600px) {
    body { font-size:14px; }
}
@media print {
   .noprint { display:none !important; }
}
.hidden {
    display:none;
}
#content {
    max-width: 600px;
    margin: 0 auto;
    margin-bottom: 3em;
    padding: 0 2em;
    background: #fff;
}

h1 {
    font-size: 2em;
    letter-spacing: 0.1em;
    font-family: "Arapey", serif;
    font-weight: normal;
    margin: 1em 0;
    position: relative;
    text-align: center;
    padding: .15em 0;
}
h2 {
    font-size:1.2em;
    line-height:1.2em;
    letter-spacing:.05em;
    font-family:"Open Sans Condensed",sans-serif;
    font-weight:700;
}


#signature {
    width:auto;
    border:dashed 2px #53777A;
    margin:0;
    text-align:center;
}
#hk,
#dev_signature {
    max-width:333px;
    display:block;
}
#date-ip {
    font-size:1.2em;
    line-height:1.2em;
    letter-spacing:.05em;
    font-family:"Open Sans Condensed",sans-serif;
    font-weight:400;
}
#print-pdf {
    text-align:center;
    padding:1.5em 0;
    margin-top:2em;
    border-top:solid 1px #ccc;
}
.buttons {
    text-align:center;
    margin:1.5em auto;
}
button {
    margin: 0 .5em;
    font-size:1.2em;
    line-height:1.5em;
    font-family: "Open Sans Condensed",sans-serif;
    font-weight: 700;
    text-transform:uppercase;
    color: #0a3666;
}
button:hover {
    color: #136fd2;
}



TABLE{width:70%;margin:0px 0 60px 0;
  font-family: verdana,arial,sans-serif;
  font-size:12px;
  color:#333333;
  border-width: 1px;
  border-color: #666666;
  border-collapse: collapse;
        border:1px solid #666666;
        font-weight:bold;
}

TABLE TR{ border:1px solid #666666;}

TABLE TR TD {padding:7px;}

.first{width:30%;border-right:1px solid #000;}
.sub{font-size:0.8em;line-height:1.5em}

.coolgap{
  border: none !important;
}
.coolgap tr{
  border: none !important;
}
.coolgap tr td{
  padding:0px;
}
.coolgap tr td div.asd{
  border: 1px solid #000;
  padding:4px 8px;
  height:40px;
}
.coolgap tr td div.zopy{
  border: 1px solid #000;
  padding:0px 8px;
  border-bottom:0;
}
.coolgap tr td div.zopy1{
  border: 1px solid #000;
  padding:0px 8px;
}


</style>

<script src="http://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
            <link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.min.css" />
            <script type="text/javascript" src="jsDatePick.min.1.3.js"></script>
            <!--<script src="js/form.js"></script>-->

            <script src="js/jSignature.min.js"></script>
    <script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"inputField",
			dateFormat:"%j-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
		new JsDatePick({
			useMode:2,
			target:"inputField1",
			dateFormat:"%j-%M-%Y"
			/*selectedDate:{				This is an example of what the full configuration offers.
				day:5,						For full documentation about these settings please see the full version of the code.
				month:9,
				year:2006
			},
			yearsRange:[1978,2020],
			limitToToday:false,
			cellColorScheme:"beige",
			dateFormat:"%m-%d-%Y",
			imgPath:"img/",
			weekStartDay:1*/
		});
	};
</script>
</head>
<body>
<!-- header start -->
<section style="display:block;width:100%;float:left;background-color:#333e48;padding-top:12px;padding-bottom:12px;margin-bottom:30px">
    <header style=" margin: 0 auto;max-width: 1100px;">
        <h1 style="float:left;color:white;margin:0;font-size:2.9em;">Ottohub <span>beta</span></h1>

        <img style="float:right;" src="http://www.bangalan.com/ottohub/images/otto_logo_gray.JPG" width="90px" height="40px">
        <div style="float:right;margin-right:40px;">
            <!--<a href="https://www.dropbox.com/home/Ottohub/Drivers"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">Dropbox</a>-->
            <a href="dropbox_folders_list.php" style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">Dropbox</a>
            <a href="upload-many-master.html" style="color:white;font-size:1.4em;text-decoration:underline;">Database</a>

               <a href="javascript:void(0);" onclick="logout();">LogOut</a>
        </div>
    </header>
</section>
<div class="content">
<!-- Multistep Form -->
<div class="main">
<form id="form1" action="http://www.bangalan.com/ottohub/" class="regform" method="post">
<!-- Progress Bar -->
<ul id="progressbar" class="flow_chart">
<li class="active"><div class="color_cercle">1</div>Details</li>
<li><div class="color_cercle">2</div>Proposal Form</li>
<li><div class="color_cercle">3</div>Order Form</li>
<li id="thanks"><span></span></li>
</ul>
<!-- Fieldsets -->
<input id="user_id" name="user_id" type="hidden" value="<?php echo $userid?>">
<fieldset id="first">
	<div class="mthird_border">
    <div style="float:left">
    <div style="width:45%;float:left">
  	<input id="uname" class="text_field" name="uname" placeholder="First Name of Driver" type="text" value="<?php echo $first_name ?>">
  	<input id="lname" class="text_field" name="lname" placeholder="Last Name of Driver" type="text" value="<?php echo $last_name ?>">

  	<input id="home_addr" class="text_field" name="home_addr" placeholder="Current Home Address" type="text"
  	value="<?php echo $address?>">

  	<div>
  	<input style="width:48%" id="inputField" class="text_field" name="dob" placeholder="Date of Birth" type="text"
  	value="<?php echo $dob;?>">
  	<input style="width:48%; float:right;" id="uemail" class="text_field" name="email" placeholder="Email" type="text"
  	value="<?php echo $email;?>">
  	</div>

  	<div>
  	<input style="width:48%" id="ninsurance" class="text_field" name="national_insurance" placeholder="National Insurance" type="text" value="<?php echo $national_insurance;?>">
  	<input style="width:48%; float:right;" id="mobile" class="text_field" name="mobile" placeholder="Mobile" type="text"
  	value="<?php echo $mobile;?>">
  	</div>
  	<div>
  	<span style="width:48%; float:left;padding-top:11px">
  		<label>How Long Resident in UK(years):</label>
  	</span>
  	<span style="width:48%;float:right">
  	<select id="residentamount" name="residentamount" class="options1">
  	<option value="0" <?php if($residentamount=='0'){echo 'selected="selected"';}?>>0</option>
  	<option value="1" <?php if($residentamount=='1'){echo 'selected="selected"';}?>>1 Year</option>
  	<option value="2" <?php if($residentamount=='2'){echo 'selected="selected"';}?>>2 Years</option>
  	<option value="3" <?php if($residentamount=='3'){echo 'selected="selected"';}?>>3 Years</option>
  	<option value="4" <?php if($residentamount=='4'){echo 'selected="selected"';}?>>4 Years</option>
  	<option value="5" <?php if($residentamount=='5'){echo 'selected="selected"';}?>>5 Years</option>
  	<option value="6" <?php if($residentamount=='6'){echo 'selected="selected"';}?>>6 Years</option>
  	<option value="7" <?php if($residentamount=='7'){echo 'selected="selected"';}?>>7 Years</option>
  	<option value="8" <?php if($residentamount=='8'){echo 'selected="selected"';}?>>8 Years</option>
  	<option value="9" <?php if($residentamount=='9'){echo 'selected="selected"';}?>>9 Years</option>
  	<option value="10" <?php if($residentamount=='10'){echo 'selected="selected"';}?>>10 Years</option>
  	<option value="11" <?php if($residentamount=='11'){echo 'selected="selected"';}?>>11 Years</option>
  	<option value="12" <?php if($residentamount=='12'){echo 'selected="selected"';}?>>12 Years</option>
  	<option value="13" <?php if($residentamount=='13'){echo 'selected="selected"';}?>>13 Years</option>
  	<option value="14" <?php if($residentamount=='14'){echo 'selected="selected"';}?>>14 Years</option>
  	<option value="15" <?php if($residentamount=='15'){echo 'selected="selected"';}?>>15 Years</option>
  	<option value="16" <?php if($residentamount=='16'){echo 'selected="selected"';}?>>16 Years</option>
  	<option value="17" <?php if($residentamount=='17'){echo 'selected="selected"';}?>>17 Years</option>
  	<option value="18" <?php if($residentamount=='18'){echo 'selected="selected"';}?>>18 Years</option>
  	<option value="19" <?php if($residentamount=='19'){echo 'selected="selected"';}?>>19 Years</option>
  	<option value="20" <?php if($residentamount=='20'){echo 'selected="selected"';}?>>20 Years</option>
  	<option value="21" <?php if($residentamount=='21'){echo 'selected="selected"';}?>>21 Years</option>
  	<option value="22" <?php if($residentamount=='22'){echo 'selected="selected"';}?>>22 Years</option>
  	<option value="23" <?php if($residentamount=='23'){echo 'selected="selected"';}?>>23 Years</option>
  	<option value="24" <?php if($residentamount=='24'){echo 'selected="selected"';}?>>24 Years</option>
  	<option value="25" <?php if($residentamount=='25'){echo 'selected="selected"';}?>>25 Years</option>
  	</select>
  	</span>
  	</div>
   </div>
   <div style="width:45%;float:right;min-height:360px">
    <!-- 2nd step elements -->
    <input id="dlicence" class="text_field" name="taxilicencenumber" placeholder="DVLA Licence Number" type="text"
    value="<?php echo $taxilicencenumber;?>">
    <div>
    <div style="width:40%; float:left;">
      <div style="width:47%; float:left; font-size:14px; line-height:18px; padding-top: 10px;">
        <label>Date Driving Test Passed</label>
      </div>
      <div style="width:50%; float:right;">
        <input  id="inputField1" class="text_field" name="datetestpassed" placeholder="Date Driving Test Pass" type="text" value="<?php echo $datetestpassed;?>">
      </div>
    </div>

    <div style="width:52%; float:right;">
      <div style="width:45%; float:left; margin:12px 0px 0; font-size:14px;">
        <label>Type of Licence held:</label>
      </div>
      <!-- <span style="width:60%;"><label>Type of Licence held:</label></span> -->
      <div style="width:50%; float:right;">
        <select id="licencetype" name="licencetype" class="options2">
        <option value="Full UK" <?php if($licencetype=='Full UK'){echo 'selected="selected"';}?>>Full UK</option>
        <option value="EU" <?php if($licencetype=='EU'){echo 'selected="selected"';}?>>EU</option>
        <option value="International" <?php if($licencetype=='International')
        {echo 'selected="selected"';}?>>International</option>
        <option value="Provisional" <?php if($licencetype=='Provisional')
        {echo 'selected="selected"';}?>>Provisional</option>
        </select>
      </div>
    </div>
    </div>
    <input id="taxi_no" class="text_field" name="pcolicencenumber" placeholder="PCO Taxi Licence Number" type="text"
    value="<?php echo $pcolicencenumber;?>">
    <p><span style="width:50%;float:left;padding-top:22px">
      <label>How Long held licence (years):</label></span>
    <span style="width:50%;float:left">
    <select id="taxilicenceage" name="taxilicenceage" class="options">
    <option value="0" <?php if($taxilicenceage=='0'){echo 'selected="selected"';}?>>0</option>
    <option value="1" <?php if($taxilicenceage=='1'){echo 'selected="selected"';}?>>1 Year</option>
    <option value="2" <?php if($taxilicenceage=='2'){echo 'selected="selected"';}?>>2 Years</option>
    <option value="3" <?php if($taxilicenceage=='3'){echo 'selected="selected"';}?>>3 Years</option>
    <option value="4" <?php if($taxilicenceage=='4'){echo 'selected="selected"';}?>>4 Years</option>
    <option value="5" <?php if($taxilicenceage=='5'){echo 'selected="selected"';}?>>5 Years</option>
    <option value="6" <?php if($taxilicenceage=='6'){echo 'selected="selected"';}?>>6 Years</option>
    <option value="7" <?php if($taxilicenceage=='7'){echo 'selected="selected"';}?>>7 Years</option>
    <option value="8" <?php if($taxilicenceage=='8'){echo 'selected="selected"';}?>>8 Years</option>
    <option value="9" <?php if($taxilicenceage=='9'){echo 'selected="selected"';}?>>9 Years</option>
    <option value="10" <?php if($taxilicenceage=='10'){echo 'selected="selected"';}?>>10 Years</option>
    <option value="11" <?php if($taxilicenceage=='11'){echo 'selected="selected"';}?>>11 Years</option>
    <option value="12" <?php if($taxilicenceage=='12'){echo 'selected="selected"';}?>>12 Years</option>
    <option value="13" <?php if($taxilicenceage=='13'){echo 'selected="selected"';}?>>13 Years</option>
    <option value="14" <?php if($taxilicenceage=='14'){echo 'selected="selected"';}?>>14 Years</option>
    <option value="15" <?php if($taxilicenceage=='15'){echo 'selected="selected"';}?>>15 Years</option>
    <option value="16" <?php if($taxilicenceage=='16'){echo 'selected="selected"';}?>>16 Years</option>
    <option value="17" <?php if($taxilicenceage=='17'){echo 'selected="selected"';}?>>17 Years</option>
    <option value="18" <?php if($taxilicenceage=='18'){echo 'selected="selected"';}?>>18 Years</option>
    <option value="19" <?php if($taxilicenceage=='19'){echo 'selected="selected"';}?>>19 Years</option>
    <option value="20" <?php if($taxilicenceage=='20'){echo 'selected="selected"';}?>>20 Years</option>
    <option value="21" <?php if($taxilicenceage=='21'){echo 'selected="selected"';}?>>21 Years</option>
    <option value="22" <?php if($taxilicenceage=='22'){echo 'selected="selected"';}?>>22 Years</option>
    <option value="23" <?php if($taxilicenceage=='23'){echo 'selected="selected"';}?>>23 Years</option>
    <option value="24" <?php if($taxilicenceage=='24'){echo 'selected="selected"';}?>>24 Years</option>
    <option value="25" <?php if($taxilicenceage=='25'){echo 'selected="selected"';}?>>25 Years</option>
    </select>
    </span>
    </p>
        <div style="height:50px;">
        <div style="width:50%; float:left; margin:17px 0px 0;">
          <label>What Vehicle would you like to reserve:</label>
        </div>
        <div style="width:50%; float:right;">
          <select id="car_id" name="car_id">
            <option value="1" <?php if($car_id=='1'){echo 'selected="selected"';}?>>Skoda Octavia SE Part-Time</option>
            <option value="2" <?php if($car_id=='2'){echo 'selected="selected"';}?>>Used Toyota Prius Hybrid</option>
            <option value="3" <?php if($car_id=='3'){echo 'selected="selected"';}?>>Toyota Auris Touring Sports</option>
            <option value="4" <?php if($car_id=='4'){echo 'selected="selected"';}?>>Toyota Prius Hybrid Active</option>
            <option value="5" <?php if($car_id=='5'){echo 'selected="selected"';}?>>Skoda Octavia Hatch SE Diesel</option>
            <option value="6" <?php if($car_id=='6'){echo 'selected="selected"';}?>>Seat Alhambra SE Diesel Auto</option>
            <option value="7" <?php if($car_id=='7'){echo 'selected="selected"';}?>>Ford Galaxy Titanium</option>
            <option value="8" <?php if($car_id=='8'){echo 'selected="selected"';}?>>Citroen Picasso</option>
            <option value="9" <?php if($car_id=='9'){echo 'selected="selected"';}?>>BMW 2 Series SE Gran Tourer</option>
            <option value="10" <?php if($car_id=='10'){echo 'selected="selected"';}?>>BMW 3 Series SE Saloon</option>
          </select>
        </div>
    </div>
  </div>
</div>
    <!-- 3rd step elements -->
<hr>
    <div style="width:84%;margin:auto;margin-left:50x;float:left">
    

  <p class="licenceQuestion">
              <span class="question">Have you been involved in an accicent or made any insurance claims 
              in the last 3 years, whether your fault or someone else's fault?
            </span>

        <span>

          <input name="radioset1" id="radio1" 
          <?php if($claims=='1'){ echo 'checked=""';}?> value="1"  class="radio" type="radio">
          <label for="radio1">YES</label>
        </span>
        
        <span>
          <input name="radioset1" id="radio2" 
          <?php if($claims=='0'){echo 'checked=""';}?> value="0" class="radio" type="radio">
          <label for="radio2">NO</label>
        </span>
        
  <textarea id="textbox" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>      
  </p>
  
      <p class="licenceQuestion">
            <span class="question">Have you been convicted (or have any pending) of any motoring 
            offences in the last 5 years?

            </span>
      
        <span class="answer">
          <input name="radioset2" id="radio3" 
          <?php if($motoroffence=='1'){ echo 'checked=""';}?> value="1" class="radio" type="radio">
          <label for="radio3">YES</label>
        </span>
        <span class="answer">
          <input name="radioset2" id="radio4" 
          <?php if($motoroffence=='0'){ echo 'checked=""';}?> value="0" class="radio"  type="radio">
          <label for="radio4">NO</label>
        </span> 
        <textarea id="textbox1" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>
      </p>
      
  <p class="licenceQuestion">
            <span class="question">Have you been convicted (or have any pending) of any criminal convictions  in the last 5 years?
            </span>
        <span>
          <input name="radioset3" id="radio5" 
          <?php if($criminalconviction=='1'){ echo 'checked=""';}?> value="1" class="radio" type="radio">
          <label for="radio5">YES</label>
        </span>
        <span>
        <input name="radioset3" id="radio6" 
        <?php if($criminalconviction=='0'){echo 'checked=""';}?> value="0" class="radio"  type="radio">
        <label for="radio6">NO</label>
        </span>
      <textarea id="textbox2" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>  
  </p>
      <p class="licenceQuestion">
            <span class="question">Have you ever been refused motor insurance or had special terms inposed?
            </span>
        <span>
          <input name="radioset4" id="radio7" 
          <?php if($motorinsurance=='1'){echo 'checked=""';}?> value="1" class="radio" type="radio">
          <label for="radio7">YES</label>
        </span>
        <span>
          <input name="radioset4" id="radio8" 
          <?php if($motorinsurance=='0'){echo 'checked=""';}?> value="0" class="radio"  type="radio">
          <label for="radio8">NO</label>
        </span>
        <textarea id="textbox3" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>
      </p>
    </div>
</div><!--mthird end-->
	<input id="contract_trigger" class="next_btn" name="next" type="button" value="Next">
</fieldset>

<!-- <fieldset class="msecond" id="second">
	<div class="mthird_border">
</div>
	<input class="pre_btn" name="previous" type="button" value="Previous">
	<input id="secondnext" class="next_btn" name="next" type="button" value="Next">
</fieldset> -->

<!-- <fieldset class="mthird" id="third">
<div class="mthird_border">  
</div>
<input class="pre_btn" name="previous" type="button" value="Previous">
<input id="contract_trigger" class="next_btn" name="next" type="button" value="Next">
</fieldset> -->

<fieldset  id="proposal">
<div id="contract_form">
</div>
<input class="pre_btn" name="previous" type="button" value="Previous">
<input id="order_trigger" class="next_btn" name="next" type="button" value="Next">
</fieldset>

<fieldset id="order">
<div id="order_form">
</div>
<input class="pre_btn" type="button" value="Previous">
<input class="next_btn_order" name="next" type="button" value="Next">
</fieldset>

<fieldset style="width:100%;text-align:center">
<div class="mthird_border">
    <div>
    <p>Thank you for filling in your details</p>
    <p>Please let the otto team know you've done!</p>
  </div>
  <div class="text-center admin_text">Admin only</div>
  <div>
   <p class="my_print" style="width:25%;margin-right:9%" id="proposal_pdf_button">Print Proposal Contract</p>
   <p class="my_print" style="width:25%" id="order_pdf_button">Print Order Contract</p> 
   <input style="width:25%;float:right" class="submit_btn" name="submit" type="submit" value="Close">
  </div>
</div>
</fieldset>

</form>
</div>
</div>


<script type="text/javascript">
$(document).ready(function() {

//3rd step yes click show textarea
$("input[name='radioset1']").click(function(){
    if($(this).val()=='1'){
       $("#textbox").show();
    }else{
        $("#textbox").hide();
    }
});
$("input[name='radioset2']").click(function(){
    if($(this).val()=='1'){
       $("#textbox1").show();
    }else{
        $("#textbox1").hide();
    }
});
$("input[name='radioset3']").click(function(){
    if($(this).val()=='1'){
       $("#textbox2").show();
    }else{
        $("#textbox2").hide();
    }
});
$("input[name='radioset4']").click(function(){
    if($(this).val()=='1'){
       $("#textbox3").show();
    }else{
        $("#textbox3").hide();
    }
});

       var  baseurl_ajax='http://www.bangalan.com/ottohub/examples/';
       var  baseurl_org='http://www.bangalan.com/ottohub/';

    $("#contract_trigger").bind('click', function () {
		        //1st step data
    var user_id = $('#user_id').val();
    var first_name = $('#uname').val();
    var last_name = $('#lname').val();
    var address = $('#home_addr').val();
    var dob = $('#inputField').val();
    var email = $('#uemail').val();
    var resident_amount = $('#residentamount').val();
    var mobile_number = $('#mobile').val();
    var national_insurance = $('#ninsurance').val();
    //2nd step data
      var taxilicencenumber = $('#dlicence').val();
      var datetestpassed = $('#inputField1').val();
      var licence_type = $('#licencetype').val();
      var pcolicencenumber = $('#taxi_no').val();
      var licence_age = $('#taxilicenceage').val();
    //3rd step data
    var car_id = $("#car_id").val();
		var claims = $("input[name='radioset1']:checked").val();
		var motoroffence = $("input[name='radioset2']:checked").val();
		var criminalconviction = $("input[name='radioset3']:checked").val();
		var motorinsurance = $("input[name='radioset4']:checked").val();

			$.ajax({
		            type: "POST",
		            url: baseurl_ajax+"php/client_update.php",
		            data: {
                  first_name: first_name,
                  last_name: last_name,
                  number: mobile_number,
                  address: address,
                  dob: dob,
                  email: email,
                  resident_amount: resident_amount,
                  national_insurance:national_insurance,
		                claims: claims,
                    car_id : car_id,
		                motoroffence: motoroffence,	                             	                
		                criminalconviction: criminalconviction,
		                motorinsurance: motorinsurance,
                  pcolicencenumber: pcolicencenumber,
                  datetestpassed: datetestpassed,                                               
                  licence_type: licence_type,
                  taxilicencenumber: taxilicencenumber,
                  licence_age: licence_age,
                  user_id:user_id                  
		                
		            }
		    });
        //alert(user_id);

        $.get(baseurl_ajax+'php/client_action.php',{user_id:user_id},function(res){
	        var user=$.parseJSON(res); 
	        var first_name = user[0].first_name;
	        var last_name = user[0].last_name;
	        var mobile = user[0].mobile;
	        var address = user[0].address;
	        var dob = user[0].dob;
	        var phone = user[0].phone;
	        var national_insurance = user[0].national_insurance;
	        var email = user[0].email;
	        var resident_amount = user[0].residentamount;
	        //var resident= resident_aamount.split('/')
	        //var resident_amount=resident['0']+'&nbsp;'+resident['1'];
	        //var users_car = $('#DTE_Field_users-car').find(":selected").text();
	        var driving_pass = user[0].datetestpassed;
	        var taxi_license = user[0].taxilicencenumber;
	        var license_age = user[0].taxilicenceage;
	        var motor_offences = user[0].motoroffence;
	        var criminal_offences = user[0].criminalconviction;
	        var motor_insurance = user[0].motorinsurance;
	        var user_claims = user[0].claims;
	        var license_type = user[0].licencetype;
	        var car_model = user[0].car;


	        $.ajax({
	            type: "POST",
	            url: baseurl_ajax+"php/test_client_contracts.php",
	            data: {
	                first_name: first_name,
	                last_name: last_name,
	                mobile: mobile,
	                address: address,
	                dob: dob,
                        phone:phone,
	                email: email,
	                resident_amount: resident_amount,	                
	                license_type: license_type,
	                national_insurance: national_insurance,
	                car_model: car_model,
	                //users_car: users_car,
	                driving_pass: driving_pass,
	                taxi_license: taxi_license,
	                license_age: license_age,
	                motor_offences: motor_offences,
	                criminal_offences: criminal_offences,
	                motor_insurance: motor_insurance,
	                user_claims: user_claims,
	                user_id: user_id
	                
	            },

	            success: function (data) {
	                $('#contract_form').html(data);
	                $('#contract_form').show();


		                $(document).ready(function() {
						  $("#signature").jSignature();
						  var $sigdiv = $("#signature");
						  var datapair = $sigdiv.jSignature("getData", "svgbase64");

			                $("#signature").bind("change", function(e) {  
		    				var data = $("#signature").jSignature("getData");  
		   					 $("#client_signature").val(data);
		 					 });

		            	});
	            }
	        });
         
        });
 
   

    });

    $("#order_trigger").bind('click', function () {

    	$('#proposal_pdf').trigger('click');

		var user_id = $('#user_id').val();
		$.get(baseurl_ajax+'php/client_action.php',{user_id:user_id},function(res){
	         var user=$.parseJSON(res);

	        var first_name = user[0].first_name;
	        var last_name = user[0].last_name;
	        var number = user[0].phone;
	        var address = user[0].address;
	        var dob = user[0].dob;
	        var mobile_number = user[0].mobile;
	        var national_insurance = user[0].national_insurance;
	        var email = user[0].email;
	        var resident_amount = user[0].residentamount;
	        //var resident= resident_aamount.split('/')
	        //var resident_amount=resident['0']+'&nbsp;'+resident['1'];
	        //var users_car = $('#DTE_Field_users-car').find(":selected").text();
	        var driving_pass = user[0].datetestpassed;
	        var taxi_license = user[0].taxilicencenumber;
	        var license_age = user[0].taxilicenceage;
	        var motor_offences = user[0].motoroffence;
	        var criminal_offences = user[0].criminalconviction;
	        var motor_insurance = user[0].motorinsurance;
	        var user_claims = user[0].claims;
	        var license_type = user[0].licencetype;
	        var car_model = user[0].car;
            $.ajax({
                type: "POST",
                url: baseurl_ajax+"php/test_client_order.php",
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    number: number,
                    address: address,
                    dob: dob,
                    mobile_number: mobile_number,
                    email: email,
                    resident_amount: resident_amount,
                    users_car: car_model,
                    driving_pass: driving_pass,
                    taxi_license: taxi_license,
                    license_age: license_age,
                    motor_offences: motor_offences,
                    criminal_offences: criminal_offences,
                    motor_insurance: motor_insurance,
                    user_claims: user_claims,
                    national_insurance: national_insurance,
                    license_type: license_type,
                    user_id: user_id
                    
                },

                success: function (data) {
                	//$('#contract_form').html('');
	               // $('#contract_form').show();
	                $('#order_form').html(data);
	                $('#order_form').show();

	                $(document).ready(function() {
						  $("#signature_order").jSignature();
						  var $sigdiv = $("#signature_order");
						  var datapair = $sigdiv.jSignature("getData", "svgbase64");

			                $("#signature_order").bind("change", function(e) {  
		    				var data = $("#signature_order").jSignature("getData"); 
		   					 $("#client_signature_order").val(data);
		 					 });
                                        $("#reset").click(function(e){
                                          $("#signature_order").jSignature("clear");
                                          var data = $("#signature_order").jSignature("getData");
                                          $("#client_signature_order").val("");
                                          e.preventDefault();driver_
                                                          });

		            	});
                }
            });
        });
    });
});

function generatePdf(user_id,dir_name) {


   var url ="result_client_contracts.php?id="+user_id;
 url+="&form=proposal";


  $.ajax({
        url : url,
        method: "POST",
       
        data: $("#signature_form").serialize(),
        success : function(response){
          console.log(response);  

          var surl=response;
         //var dir_name="' . $first_name . '_' . $last_name . '_' . date('d-m-Y', strtotime($dob)) . '";
 var file_name="proposalcontract.pdf";
  $.post("html_to_pdf.php",{"url":surl,"dir_name":dir_name,"file_name":file_name},
  function(res){
   console.log(res);
         $('#proposal_pdf_button').html(res);

  });

          

          
        },
        error : function(response){
          console.log(response);
        }

      });



  
}
function generateorderPdf(user_id,users_car,dir_name) {


   var url ="result_client_orders.php?id="+user_id+"&car_model="+users_car;
 url+="&form=order";


  $.ajax({
        url : url,
        method: "POST",
       
        data: $("#signature_form_order").serialize(),
        success : function(response){
            

          var surl=response;
         //var dir_name="' . $first_name . '_' . $last_name . '_' . date('d-m-Y', strtotime($dob)) . '";
 var file_name="ordercontract.pdf";
  $.post("html_to_pdf.php",{"url":surl,"dir_name":dir_name,"file_name":file_name},
  function(res){

    $('#order_pdf_button').html(res);

  });

          

          
        },
        error : function(response){
          console.log(response);
        }

      });



  
}
</script>

<script>


$(document).ready(function() {
var count = 0; // To Count Blank Fields
/*------------ Validation Function-----------------*/
$(".submit_btn").click(function(event) {
//var radio_check = $('.rad'); // Fetching Radio Button By Class Name
var input_field = $('.text_field'); // Fetching All Inputs With Same Class Name text_field & An HTML Tag textarea
//var text_area = $('textarea');
// Validating Radio Button
/*if (radio_check[0].checked == false && radio_check[1].checked == false) {
var y = 0;
} else {
var y = 1;
}*/
// For Loop To Count Blank Inputs
for (var i = input_field.length; i > count; i--) {
if (input_field[i - 1].value == '' || text_area.value == '') {
count = count + 1;
} else {
count = 0;
}
}
// Notifying Validation
if (count != 0 || y == 0) {
alert("*All Fields are mandatory*");
event.preventDefault();
} else {
return true;
}
});
/*---------------------------------------------------------*/
$(".next_btn").click(function() { // Function Runs On NEXT Button Click
$(this).parent().next().fadeIn('slow');
$(this).parent().css({
'display': 'none'
});
// Adding Class Active To Show Steps Forward;
$('.active').next().addClass('active');

$('.active').prev().removeClass('active');
//$(".active").siblings().css({"color": "red", "border": "2px solid red"});
});

$(".next_btn_order").click(function() { // Function Runs On NEXT Button Click

	$('#order_pdf').trigger('click');
$(this).parent().next().fadeIn('slow');
$(this).parent().css({
'display': 'none'
});
// Adding Class Active To Show Steps Forward;
$('.active').next().addClass('active');

$('.active').prev().removeClass('active');
//$(".active").siblings().css({"color": "red", "border": "2px solid red"});
        var thanks = $('.flow_chart li:last-child').attr('class');
        if (thanks=='active'){
        //alert(thanks);
          $('#progressbar').css('display','none');
        }
});

$(".pre_btn").click(function() { // Function Runs On PREVIOUS Button Click
$(this).parent().prev().fadeIn('slow');
$(this).parent().css({
'display': 'none'
});
// Removing Class Active To Show Steps Backward;
$('.active:last').removeClass('active');
});
// Validating All Input And Textarea Fields
$(".submit_btn").click(function(e) {
if ($('input').val() == "" || $('textarea').val() == "") {
alert("*All Fields are mandatory*");
return false;
} else {
return true;
}
});
});
</script>

</body>
</html>