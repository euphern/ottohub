<?php

$to = $_REQUEST['to'];
$from = $_REQUEST['from'];
$fname = $_REQUEST['fname'];
$lname = $_REQUEST['lname'];
$subject = $_REQUEST['subject'];
$msg = $_REQUEST['message'];
$message = '<html>
<head>
<title>HTML email</title>

<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body style="font-size: 16px;line-height: 20px; background:#f0f0f0; font-family: "Open Sans", sans-serif;">
<section style="width: 100%;float: left; clear: both;">
    <div style="width: 948px;margin: 0 auto;background: #fff; padding: 20px 0 0;box-shadow: 2px 2px 14px -8px rgba(0, 0, 0, 0.5 );box-sizing: border-box;">
        <div style="padding: 0 100px;">
            <p style="padding:0px 0 0px;"><img src="http://ottohub.euphern.com/demo/examples/php/images/logo.png" alt="logo"></p>
            <p style="padding:5px 0 0; "><img style="margin-bottom: -7px;" src="http://ottohub.euphern.com/demo/examples/php/images/phone.jpg" alt="phone"> <span>020 8740 7444</span></p>
            <p style="padding:5px 0 0;"><img style="margin-bottom: -7px;" src="http://ottohub.euphern.com/demo/examples/php/images/email.jpg" alt="email"> <span style="line-height: 0">info@ottocar.co.uk</span></p>
            <p style="padding:5px 0 0px;">
                 Hi,
            </p>
            <p style="padding:5px 0 0px;">
                <b>'.$fname.' '.$lname.',</b>
            </p>
            <p style="padding:0 0 10px;">
                <b>Congratulations and welcome to the Otto Family.</b>
            </p>
            <p style="padding:0 0 0px;">
                Your document has been signed by all parties and will always be available via the adrdress below:
            </p>

            <div style="background: #8bccfa;padding: 15px 20px;width: 100%;box-sizing: border-box;margin: 15px 0 10px;">
                <p>
                    <img style="margin-top: 5px;" src="http://ottohub.euphern.com/demo/examples/php/images/check.png" alt="checked">
                    RENT TO BUY SKODA AGREEMENT
                </p>
                <p>Please <a href='.$msg.' title="download">click here</a> to download a copy of your document</p>
                <p>
                    Please copy <a href='.$msg.' title="download">'.$msg. '</a>
                </p> 
                <p>
                    into a browser if you can not click the link above
                </p>
            </div>
            <p style="padding:10px 0 20px;">
                Kind regards, <br>Otto Team
            </p>
        </div>
        <div style="width: 100%;clear: both;background: #1E252B;padding:10px 100px;box-sizing: border-box;color: #6d6d6d;float: left;">
            <div style="width: 50%; float: left; margin-right: 20px;line-height: 28px;">
                <p>
                    <img style="margin-bottom: -4px;" src="http://ottohub.euphern.com/demo/examples/php/images/map_icon.png" alt="location">
                    Otto Car Limited, 3 Sussex Place, Hammersmith, London, W6 9EA, United Kingdom
                </p>
            </div>
            <div style="width: auto; float: right;">
                <p style="padding:0 0 3px; color: #6d6d6d;">
                    <img style="line-height: 5px" src="http://ottohub.euphern.com/demo/examples/php/images/phone_icon.png" alt="phone">
                    020 8740 7444
                </p>
                <p style="padding:0 0 3px;color: #6d6d6d; margin-bottom: 0px">
                    <img style="line-height: 5px" src="http://ottohub.euphern.com/demo/examples/php/images/email_icon.png" alt="email">
                   <a style="color: #6d6d6d;" href="email:info@ottocar.co.uk">info@ottocar.co.uk</a>
                </p>
            </div>
        </div>
    </div>
</section>
</body>
</html>';

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
//$header.= "Content-Type: text/plain; charset=ISO-8859-1\r\n"; 
$header.= "X-Priority: 1\r\n"; 

// More headers
$headers .= "From:".$from."\r\n";
$headers .= "Reply-To:".$from."\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

$sendmail = mail($to,$subject,$message,$headers);


if($sendmail){

//$success = "Email send Successfully";
//echo json_encode($success);
echo  "<p class='mail_send'>Email send Successfully</p>";

}
?>