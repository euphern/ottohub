<?php
error_reporting(0);
session_start();
// DataTables PHP library
include_once("../../php/DataTables.php");

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;
    $carresult = $db->sql('select model  from car_type');
    $mycars['0'] = $carresult->fetchAll();
    $cars =$mycars['0'];
    $size=sizeof($cars);
    //echo $size;
?>
<!DOCTYPE html>
<html>
<head>
  <title>New Client</title>
  <meta content="noindex, nofollow" name="robots">
  <meta charset="utf-8">
  <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <link rel="stylesheet" href="client_view.css">
  <link rel="stylesheet" href="../../js/jquery.signature.css">
  <!-- https://github.com/brinley/jSignature/blob/master/README.md -->
  <style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic);
    @import url(http://fonts.googleapis.com/css?family=Arapey);
    @import url(http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700);
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800);

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

    body {
        font-size: 14px;
        line-height: 1.42857143;
        color: #333;
        background-color: #fff;
        padding: .01px;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    }
    @media (max-width:600px) {
        body { font-size:14px; }
    }
    @media print {
       .noprint { display:none !important; }
    }
    .hidden {
        display:none;
    }
    #content {
        max-width: 600px;
        margin: 0 auto;
        margin-bottom: 3em;
        padding: 0 2em;
        background: #fff;
    }

    h1 {
        font-size: 2em;
        letter-spacing: 0.1em;
        font-family: "Arapey", serif;
        font-weight: normal;
        margin: 1em 0;
        position: relative;
        text-align: center;
        padding: .15em 0;
    }
    h2 {
        font-size:1.2em;
        line-height:1.2em;
        letter-spacing:.05em;
        font-family:"Open Sans Condensed",sans-serif;
        font-weight:700;
    }


    #signature {
        width:auto;
        border:dashed 2px #53777A;
        margin:0;
        text-align:center;
    }
    #hk,
    #dev_signature {
        max-width:333px;
        display:block;
    }
    #date-ip {
        font-size:1.2em;
        line-height:1.2em;
        letter-spacing:.05em;
        font-family:"Open Sans Condensed",sans-serif;
        font-weight:400;
    }
    #print-pdf {
        text-align:center;
        padding:1.5em 0;
        margin-top:2em;
        border-top:solid 1px #ccc;
    }
    .buttons {
        text-align:center;
        margin:1.5em auto;
    }
    button {
        margin: 0 .5em;
        font-size:1.2em;
        line-height:1.5em;
        font-family: "Open Sans Condensed",sans-serif;
        font-weight: 700;
        text-transform:uppercase;
        color: #0a3666;
    }
    button:hover {
        color: #136fd2;
    }



    TABLE{width:70%;margin:0px 0 60px 0;
      font-family: verdana,arial,sans-serif;
      font-size:12px;
      color:#333333;
      border-width: 1px;
      border-color: #666666;
      border-collapse: collapse;
            border:1px solid #666666;
            font-weight:bold;
    }

    TABLE TR{ border:1px solid #666666;}

    TABLE TR TD {padding:7px;}

    .first{width:30%;border-right:1px solid #000;}
    .sub{font-size:0.8em;line-height:1.5em}

    .coolgap{
      border: none !important;
    }
    .coolgap tr{
      border: none !important;
    }
    .coolgap tr td{
      padding:0px;
    }
    .coolgap tr td div.asd{
      border: 1px solid #000;
      padding:4px 8px;
      height:40px;
    }
    .coolgap tr td div.zopy{
      border: 1px solid #000;
      padding:0px 8px;
      border-bottom:0;
    }
    .coolgap tr td div.zopy1{
      border: 1px solid #000;
      padding:0px 8px;
    }

    p.mail_send {
        background: #ccc;
        padding: 5px;
        font-weight: bold;
        box-shadow: 0px 0px 5px;
    }
    .error{color:red;}
    #progressbar{display:none;}
  </style>

  <script src="http://code.jquery.com/jquery-3.1.1.min.js"
    integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    crossorigin="anonymous">
  </script>
  <script src="js/jSignature.min.js"></script>
<script>

</script>
</head>
<body>

<!-- header start -->
<section style="display:block;width:100%;float:left;background-color:#333e48;padding-top:12px;padding-bottom:12px;margin-bottom:30px">
    <header style=" margin: 0 auto;max-width: 1100px;">
        <h1 style="float:left;color:white;margin:0;font-size:2.9em;">Ottohub</h1>
          <img style="float:right;" src="../../images/otto_logo_gray.JPG" width="150px" height="80px"/>
            <div style="float:right;margin:20px 40px;">
            <a href="http://ottohub.euphern.com/demo/referrals.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">REFERRALS</a>
            <a href="http://ottohub.euphern.com/demo/accidents.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">ACCIDENTS</a>
            <a href="http://ottohub.euphern.com/demo/index.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">DRIVERS</a>
            <a href="http://ottohub.euphern.com/demo/cars.html"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">CARS</a>
            <a style="color:white;font-size:1.4em;text-decoration:underline;" href="javascript:void(0);"
                 onclick="logout();">LogOut</a>
        </div>
    </header>
</section>
<div class="content">
<?php 

$first_name="";
$last_name="";
$address="";
$address_process_1="";

//$city="";
//$postcode="";


$dob="";

$dob_day="";
$dob_mon="";
$dob_yr="";

$expiry_day="";
$expiry_mon="";
$expiry_yr="";


$startdate_day="";
$startdate_mon="";
$startdate_yr="";

$expiry_date="";
$license_id="";


if(isset($_SESSION['lic_array']))
{
$first_name=$_SESSION['lic_array'][0]["firstname"];
$first_name = preg_replace('/[0-9]+/', '', $first_name);

$last_name=$_SESSION['lic_array'][0]["lastname"];

$address_rep=$_SESSION['lic_array'][0]["address"];
$address_process_1=str_replace("DRIVING LICENCE"," ",$address_rep);
//$address_process=str_replace(".",",",$address_process_1);
//$address_exploded= explode(",",$address_process_1);

//$address=$address_exploded[0];
//$city=$address_exploded[1];
//$postcode=$address_exploded[2];
//$postcode=end($address_exploded); 



$license_id=$_SESSION['lic_array'][0]["licenseid"];

$dob=$_SESSION['lic_array'][0]["dob"];
$dobdashcount=substr_count($dob, '-');
$dobdotcount=substr_count($dob, '.');
if($dobdashcount>=1)
{

$dob_exploded= explode("-",$dob);
$dob_day=$dob_exploded[0];
$dob_mon=$dob_exploded[1];
$dob_yr=$dob_exploded[2];
}
else if($dobdotcount>=1)
{
$dob_exploded= explode(".",$dob);
$dob_day=$dob_exploded[0];
$dob_mon=$dob_exploded[1];
$dob_yr=$dob_exploded[2];  
}



/*$countdobyr=strlen($dobcount_yr);
if($countdobyr<4)
{
$dob_yr="19".$dob_exploded[2];
}
else
{
$dob_yr=$dob_exploded[2]; 
}*/



$expiry_date=$_SESSION['lic_array'][0]["lic_expirydate"];
$expirydashcount=substr_count($expiry_date, '-');
if($expirydashcount>=1)
{
$expiry_exploded= explode("-",$expiry_date);
$expiry_day=$expiry_exploded[0];
$expiry_mon=$expiry_exploded[1];
$expiry_yr=$expiry_exploded[2];
}
else
{
$expiry_exploded= explode(".",$expiry_date);
$expiry_day=$expiry_exploded[0];
$expiry_mon=$expiry_exploded[1];
$expiry_yr=$expiry_exploded[2];  
}


/*$countexpyr=strlen($expirycount_yr);
if($countexpyr<4)
{
$expiry_yr="20".$expiry_exploded[2];
}
else
{
$expiry_yr=$expiry_exploded[2]; 
}*/



$licstart_date=$_SESSION['lic_array'][0]["lic_startdate"];   
$startdashcount=substr_count($licstart_date, '-');
if($startdashcount>=1)
{
$startdate_exploded= explode("-",$licstart_date);
$startdate_day=$startdate_exploded[0];
$startdate_mon=$startdate_exploded[1];
$startdate_yr=$startdate_exploded[2];
}
else
{
$startdate_exploded= explode(".",$licstart_date);
$startdate_day=$startdate_exploded[0];
$startdate_mon=$startdate_exploded[1];
$startdate_yr=$startdate_exploded[2];  
}







}


?>
<div class="upload_pannel">
  <form action="ocr.php" method="post" enctype="multipart/form-data">
    <input type="file" name="fileToUpload" class="browser_btn" id="fileToUpload" required>
    <input type="submit" value="Upload" class="upload_btn" name="submit">
  </form>
</div>
<!-- Multistep Form -->
<div class="main">
<form id="form1" action="http://www.bangalan.com/ottohub/" class="regform" method="post">
<!-- Progress Bar -->
<ul id="progressbar" class="flow_chart">
<li class="active"><div class="color_cercle">1</div>Details</li>
<li><div class="color_cercle">2</div>Proposal Form</li>
<li><div class="color_cercle">3</div>Finish</li>
<li id="thanks"><span></span></li>
<!--<li id="last"><span></span></li>-->
</ul>
<!-- Fieldsets -->

<input id="user_id" name="user_id" type="hidden" value="">
<fieldset id="first">
  <div class="mthird_border">
    <div class="full_width">
      <div class="left_width">
        <label>First Name<?php //print_r($expiry_mon); ?></label>
        <input type="text" placeholder="First Name of Driver" id="uname" name="uname" class="text_field" value="<?php print_r($first_name);?>">
        <p class="uname error"></p>
      </div>
      <div class="right_width">
        <label>Last Name</label>
        <input type="text" placeholder="Last Name of Driver" id="lname" name="lname" class="text_field" value="<?php print_r($last_name);?>">
          <p class="lname error"></p>
      </div>
    </div>
     <div class="full_width">
      

        <label>Postcode</label>
      <!--<input type="text" placeholder="Current Home Address" id="home_addr" name="home_addr" class="text_field" value="<?php //print_r($postcode);?>">-->

       

          <section class="addresslookup" style="width:100%">
          <div id="lookup_field"></div>
          <!-- This is your existing form -->
          <div  class="b-form__element">
          <label class="b-form__label">Address <?php //print_r($address_exploded); ?></label>
          <input  id="first_line" type="text" name="first_line" class="b-form__input text_field" value="<?php print_r($address_process_1);?>">
          </div>

          <div  class="b-form__element">
          <label  class="b-form__label"></label>
          <input   id="second_line" type="text" name="second_line"  class="b-form__input" value="">
          </div>
          <div  class="b-form__element">
          <label  class="b-form__label"></label>
          <input  id="third_line" type="text" name="third_line"  class="b-form__input" value="">
          </div>
          <div  class="b-form__element">
          <label  class="b-form__label">City</label>
          <input  id="post_town" type="text" name="post_town"  class="b-form__input text_field" value="<?php //print_r($city);?>">
          </div>
          <div  class="b-form__element">
          <label  class="b-form__label">Postcode</label>
          <input  id="postcode" type="text" name="postcode"  class="b-form__input text_field" value="<?php //print_r($postcode);?>">
          </div>
        </section>
    </div>
    <div class="full_width">
      <div class="left_width">

          <label>Date of Birth</label>
          <div class="three_select">
          <select id="day" class="select_dob">
          <?php for($i='1';$i<'32';$i++){?>
          <option value="<?php echo $i;?>" <?php if($i==$dob_day){echo 'selected="selected"';}?>><?php echo $i;?></option>
          <?php }?>
          </select>
          <select id="month" class="select_dob">
              <option value="Jan" <?php if("01"==$dob_mon){echo 'selected="selected"';}?>>Jan</option>
              <option value="Feb" <?php if("02"==$dob_mon){echo 'selected="selected"';}?>>FEB</option>
              <option value="Mar" <?php if("03"==$dob_mon){echo 'selected="selected"';}?>>MAR</option>
              <option value="Apr" <?php if("04"==$dob_mon){echo 'selected="selected"';}?>>APR</option>
              <option value="May" <?php if("05"==$dob_mon){echo 'selected="selected"';}?>>MAY</option>
              <option value="Jun" <?php if("06"==$dob_mon){echo 'selected="selected"';}?>>JUN</option>
              <option value="Jul" <?php if("07"==$dob_mon){echo 'selected="selected"';}?>>JUL</option>
              <option value="Aug" <?php if("08"==$dob_mon){echo 'selected="selected"';}?>>AUG</option>
              <option value="Sep" <?php if("09"==$dob_mon){echo 'selected="selected"';}?>>SEP</option>
              <option value="Oct" <?php if("10"==$dob_mon){echo 'selected="selected"';}?>>OCT</option>
              <option value="Nov" <?php if("11"==$dob_mon){echo 'selected="selected"';}?>>NOV</option>
              <option value="Dec" <?php if("12"==$dob_mon){echo 'selected="selected"';}?>>DEC</option>
          </select>
          <select id="year" class="select_dob">
            <?php for($i='1935';$i<'2018';$i++){?>
            <option value="<?php echo $i;?>" <?php if($i==$dob_yr){echo 'selected="selected"';}?>><?php echo $i;?></option>
              <?php }?>
          </select>
          </div>
      </div>
      <div class="right_width">
          <label>Mobile</label>
          <input type="text" placeholder="Mobile" name="mobile" id="mobile" class="text_field" value="">
      </div>
    </div>
    <div class="full_width">
      <div class="left_width">
          <label>Email</label>
        <input type="text" id="uemail" name="email" placeholder="Email" class="text_field" value="">
        <p class="uemail error"></p>
      </div>
      <div class="right_width">
          <label>Insurance Number</label>
          <input type="text" id="ninsurance" name="national_insurance" placeholder="National Insurance" class="text_field" value="">
          <p class="ninsurance error"></p>
      </div>
    </div>
    <div class="full_width">
      <div class="left_width">
          <label>DVLA Licence Number</label>
          <input type="text" id="dlicence" name="taxilicencenumber" placeholder="DVLA Licence Number" class="text_field" value="<?php print_r($license_id);?>">
          <p class="dlicence error"></p>
      </div>
      <div class="right_width">
          <label>PCO Taxi Licence Number</label>
          <input type="text" id="taxi_no" name="pcolicencenumber" placeholder="PCO Taxi Licence Number" class="text_field" value="">
          <p class="taxi_no error"></p>
      </div>
    </div>
    <div class="full_width">
      <div class="left_width">
          <label>How Long Resident in UK(years)</label>
            <select id="residentamount" name="residentamount" class="options1">

             <?php for($i=0;$i<=50;$i++){ ?>
          <option value="<?php echo $i; ?>"><?php echo $i.' Years';?></option>
          <?php } ?>
            
            </select>
      </div>
      <div class="right_width">
          <label>Date Driving Test Passed</label>
           <div class="three_select">
            <select id="drivingday" class="select_dob">
            <?php for($i='1';$i<'32';$i++){?>
            <option value="<?php echo $i;?>" <?php if($i==$startdate_day){echo 'selected="selected"';}?>><?php echo $i;?></option>
            <?php }?>
            </select>
            <select id="drivingmonth" class="select_dob">
              <option value="Jan" <?php if("01"==$startdate_mon){echo 'selected="selected"';}?>>Jan</option>
              <option value="Feb" <?php if("02"==$startdate_mon){echo 'selected="selected"';}?>>FEB</option>
              <option value="Mar" <?php if("03"==$startdate_mon){echo 'selected="selected"';}?>>MAR</option>
              <option value="Apr" <?php if("04"==$startdate_mon){echo 'selected="selected"';}?>>APR</option>
              <option value="May" <?php if("05"==$startdate_mon){echo 'selected="selected"';}?>>MAY</option>
              <option value="Jun" <?php if("06"==$startdate_mon){echo 'selected="selected"';}?>>JUN</option>
              <option value="Jul" <?php if("07"==$startdate_mon){echo 'selected="selected"';}?>>JUL</option>
              <option value="Aug" <?php if("08"==$startdate_mon){echo 'selected="selected"';}?>>AUG</option>
              <option value="Sep" <?php if("09"==$startdate_mon){echo 'selected="selected"';}?>>SEP</option>
              <option value="Oct" <?php if("10"==$startdate_mon){echo 'selected="selected"';}?>>OCT</option>
              <option value="Nov" <?php if("11"==$startdate_mon){echo 'selected="selected"';}?>>NOV</option>
              <option value="Dec" <?php if("12"==$startdate_mon){echo 'selected="selected"';}?>>DEC</option>
            </select>
            <select id="drivingyear" class="select_dob">
              <?php for($i='1935';$i<'2018';$i++){?>
              <option value="<?php echo $i;?>"<?php if($i==$startdate_yr){echo 'selected="selected"';}?>><?php echo $i;?></option>
                <?php }?>
            </select>
          </div>
      </div>
    </div>
    <div class="full_width">
      <div class="left_width">
      <label>What Vehicle would you like to reserve:</label>
<select id="car_id" name="car_id">
        <?php for($i='0';$i<$size; $i++){
/*        echo $cars[$i]['model'];
*/    
    $var='<option value=" '.$cars[$i]['model'].' ">'.$cars[$i]['model'].'</option>';
        echo $var;
        }?> 
</select>
      </div>
      <div class="right_width">
        <label>Car color chosen</label>
        <select id="car_color">
            <option value="black">Black</option>
            <option value="white">White</option>
            <option value="grey">Grey</option>
            <option value="blue">Blue</option>
        </select>
      </div>
    </div>
    <div class="full_width">
      <div class="left_width"> 
        <label>DVLA Expiry Date</label>
        <div class="three_select">
          <select id="dvladay" class="select_dob">
          <?php for($i='1';$i<'32';$i++){?>
          <option value="<?php echo $i;?>" <?php if($i==$expiry_day){echo 'selected="selected"';}?>><?php echo $i;?></option>
          <?php }?>
          </select>
          <select id="dvlamonth" class="select_dob">
              <option value="Jan" <?php if("01"==$expiry_mon){echo 'selected="selected"';}?>>Jan</option>
              <option value="Feb" <?php if("02"==$expiry_mon){echo 'selected="selected"';}?>>FEB</option>
              <option value="Mar" <?php if("03"==$expiry_mon){echo 'selected="selected"';}?>>MAR</option>
              <option value="Apr" <?php if("04"==$expiry_mon){echo 'selected="selected"';}?>>APR</option>
              <option value="May" <?php if("05"==$expiry_mon){echo 'selected="selected"';}?>>MAY</option>
              <option value="Jun" <?php if("06"==$expiry_mon){echo 'selected="selected"';}?>>JUN</option>
              <option value="Jul" <?php if("07"==$expiry_mon){echo 'selected="selected"';}?>>JUL</option>
              <option value="Aug" <?php if("08"==$expiry_mon){echo 'selected="selected"';}?>>AUG</option>
              <option value="Sep" <?php if("09"==$expiry_mon){echo 'selected="selected"';}?>>SEP</option>
              <option value="Oct" <?php if("10"==$expiry_mon){echo 'selected="selected"';}?>>OCT</option>
              <option value="Nov" <?php if("11"==$expiry_mon){echo 'selected="selected"';}?>>NOV</option>
              <option value="Dec" <?php if("12"==$expiry_mon){echo 'selected="selected"';}?>>DEC</option>
          </select>
          <select id="dvlayear" class="select_dob">
            <?php for($i='2017';$i<'2076';$i++){?>
            <option value="<?php echo $i;?>"<?php if($i==$expiry_yr){echo 'selected="selected"';}?>><?php echo $i;?></option>
              <?php }?>
          </select>
        </div>
      </div>
      <div class="right_width">
        <label>Work Hours</label>
        <select id="work_hours">
            <option value="full-time">Full-Time</option>
            <option value="part-time">Part-Time</option>
        </select>
      </div>
    </div>
    <div class="full_width">
      <div class="licenceQuestion">
          <div class="question">Have you been involved in an accicent or made any insurance claims 
            in the last 3 years, whether your fault or someone else's fault?
          </div>
           <div class="puestion_rigt">
              <span>
                <input name="radioset1" id="radio1" value="1"  class="radio" type="radio">
                <label for="radio1">YES</label>
              </span>
              
              <span>
                <input name="radioset1" id="radio2" checked value="0" class="radio" type="radio">
                <label for="radio2">NO</label>
              </span>
          </div>
       <!--  <textarea id="textbox" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>  -->    
      </div>
    </div>
    <div class="full_width">
      <div class="licenceQuestion">
        <div class="question">Have you been convicted (or have any pending) of any motoring 
        offences in the last 5 years?
        </div>
        <div class="puestion_rigt">
            <span class="answer">
              <input name="radioset2" id="radio3" value="1" class="radio" type="radio">
              <label for="radio3">YES</label>
            </span>
            <span class="answer">
              <input name="radioset2" id="radio4" checked value="0" class="radio"  type="radio">
              <label for="radio4">NO</label>
            </span>
        </div>
       <!--  <textarea id="textbox1" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>  -->
      </div>
    </div>
    <div class="full_width">
      <div class="licenceQuestion">
        <div class="question">Have you been convicted (or have any pending) of any criminal convictions  in the last 5 years?
        </div>
        <div class="puestion_rigt">
            <span>
              <input name="radioset3" id="radio5" value="1" class="radio" type="radio">
              <label for="radio5">YES</label>
            </span>
            <span>
            <input name="radioset3" id="radio6" checked value="0" class="radio"  type="radio">
            <label for="radio6">NO</label>
            </span>
        </div>
       <!--  <textarea id="textbox2" class="feedback-input" placeholder="Please specify" style="display: none;"></textarea>  -->
      </div>
    </div>
    <div class="full_width">
      <div class="licenceQuestion">
        <div class="question">Have you ever been refused motor insurance or had special terms imposed?
        </div>
        <div class="puestion_rigt">
            <span>
              <input name="radioset4" id="radio7" value="1" class="radio" type="radio">
              <label for="radio7">YES</label>
            </span>
            <span>
              <input name="radioset4" id="radio8" checked value="0" class="radio"  type="radio">
              <label for="radio8">NO</label>
            </span>
        </div>
       
      </div>
    </div>

     <textarea id="textbox3" class="feedback-input" placeholder="If any of the answers are yes above please specify" style="display: none;"></textarea>
  </div><!--mthird end-->

    <!-- <input id="contract_trigger_save" name="next" type="button" value="Update And Close"> -->

  <input id="contract_trigger" class="next_btn" name="next" type="button" value="Submit">
  <p class="update error"></p>
</fieldset>
<fieldset  id="proposal">
<div id="contract_form">
</div>
<input class="pre_btn" name="previous" type="button" value="Previous">
<input id="order_trigger" class="next_btn" name="next" type="button" value="Next">
</fieldset>

<!--<fieldset id="order">
<div id="order_form">
</div>
<input class="pre_btn" type="button" value="Previous">
<input class="next_btn_order" name="next" type="button" value="Next">
</fieldset>-->

<fieldset style="width:100%;text-align:center">
  <div style="height:280px" class="mthird_border">
      <div>
      <p>Thank you for filling in your details</p>
      <p>Please let the otto team know you've done!</p>
    </div>
    <div class="text-center admin_text">Admin only</div>
    <div>
     <p class="my_print" style="width:30%;" id="proposal_pdf_button">Print Proposal Contract</p>
     <p class="my_print" style="display:none" id="order_pdf_button">Print Order Contract</p> 
     <input id="approved" style="width:25%;float:right" class="submit_btn" name="submit" type="submit" value="Approved">
    </div>
  </div>
  <div style="" class="mthird_border jov">
    <table> 
      <tr>
       <td valign="top">
        <label for="first_name">Email To</label>
    <div class="input_width">
        <input id="mt1" type="text" class="mt0" name="first_name" maxlength="80" size="30" >
    </div>
       </td>
      </tr>
     
      <tr>
       <td valign="top">
        <label for="last_name">Subject</label>
    <div class="input_width">
        <input  id="mt2" type="text" class="mt0" name="last_name" maxlength="80" size="30" value="Ottocar Proposal Form">
    </div>
       </td>
      </tr>
     
      <tr>
       <td valign="top">
        <label for="email">Email From</label>
    <div class="input_width">
        <input  id="mt3" type="text" class="mt0" name="email" maxlength="80" size="30" value="info@ottocar.co.uk">
    </div>
       </td>
      </tr>
     
      <tr>
       <td valign="top">
        <label for="comments">Text *</label>
        <textarea  id="mt4" name="comments" maxlength="1000" cols="25" rows="6">
        </textarea>
       </td>
      </tr> 

    <tr>
     <td><div id="mail_success"></div></td>
    </tr>
    <tr>
     <td style="text-align:center;float:right;">
     <button id="email_this" style="width:100px"  type="button">Email</button></td>
    </tr>
    </table>
  </div>
  <input class="pre_btn" type="button" value="Previous">
</fieldset>
<fieldset>
</fieldset>

</form>
</div>
</div>
<?php unset($_SESSION['lic_array']); ?>

<script type="text/javascript">
$(document).ready(function() {
  var host= $(location).attr('hostname');
  if(host=='localhost'){
     var  baseurl_ajax='http://localhost/ottohub/examples/';
     var  baseurl_org='http://localhost/ottohub/';
    }else{
     var  baseurl_ajax='http://ottohub.euphern.com/demo/examples/';
     var  baseurl_org='http://ottohub.euphern.com/demo/'; 
    }

 //First Name Validation
  $( "#uname" ).focusout(function() {
    var uname = $('#uname').val();
    if (uname == "") {
    $('p.uname.error').html("first Name is mandatory*");
     $( "#uname" ).focus();
    }else{$('p.uname.error').hide();}
  });

 //Last Name Validation
  $( "#lname" ).focusout(function() {
    var lname = $('#lname').val();
    if (lname == "") {
    $('p.lname.error').html("Last Name is mandatory*");
     $( "#lname" ).focus();
    }else{$('p.lname.error').hide();}
  });

 //Email Existance Check and Blank Field Validation
  $( "#uemail" ).focusout(function() {
    var uemail = $('#uemail').val();
    if (uemail == "") {
    $('p.uemail.error').html("Email is mandatory*");
     $( "#uemail" ).focus();
    } else {

      $.get(baseurl_ajax+"php/checkemail.php",{email:uemail},function(res){
        //var res=$.parseJSON(res);
       //alert(res.first_name);
        if(res=='1'){
         $('p.uemail.error').html("Your email already exists");
         $( "#uemail" ).focus();
        }else{$('p.uemail.error').hide();}
       });
    }
  });

 //National Insurance Validation
  $( "#ninsurance" ).focusout(function() {
    var national_insurance = $('#ninsurance').val();
    if (national_insurance == "") {
    $('p.ninsurance.error').html("National Insurance is mandatory*");
     $( "#ninsurance" ).focus();
    } else {
      var nlength =  national_insurance.length;
      console.log(nlength);
      if(nlength<'9' || nlength>'9'){
      $('p.ninsurance.error').html("Please Enter 9 characters National Insurance");
      $( "#ninsurance" ).focus();        
      }else{$('p.ninsurance.error').hide();}
    }
  });

  //DVLA Number Validation
  $( "#dlicence" ).focusout(function() {
    var dlicence = $('#dlicence').val();
    if (dlicence == "") {
    $('p.dlicence.error').html("DVLA Number is mandatory*");
     $( "#dlicence" ).focus();
    } else {
      var nlength =  dlicence.length;
      if(nlength<'16'){
      $('p.dlicence.error').html("Please Enter minimum 16 characters DVLA Number");
     $( "#dlicence" ).focus();        
      }else{$('p.dlicence.error').hide();}
    }
  });
  
  //PCO Licence Number Validation
  $( "#taxi_no" ).focusout(function() {
    var taxi_no = $('#taxi_no').val();
    if (taxi_no == "") {
    $('p.taxi_no.error').html("PCO Licence Number is mandatory*");
     $( "#taxi_no" ).focus();
    } else {
      var nlength =  taxi_no.length;
      if(nlength<'4' || nlength>'6'){
      $('p.taxi_no.error').html("Please Enter 4-6 characters PCO Licence Number");
     $( "#taxi_no" ).focus();        
      }else{$('p.taxi_no.error').hide();}
    }
  });

 

  $('#email_this').click(function(){

    var email_to = $('#mt1').val();
    var subject = $('#mt2').val();
    var email_from = $('#mt3').val();
    var message = $('#mt4').val();
    $.get('email_send.php',{to:email_to,from:email_from,subject:subject,message:message},function(res){
      //var mail = $.parseJSON(res);
      //alert(res);
      //var success = '<p class="mail_success">'+res+'</p>';
      $('div#mail_success').html(res);
      $('#mt1').val('');
      $('#mt2').val('');
      $('#mt3').val('');
      $('#mt4').val('');
    });
  });
  $(".radio").click(function(){
    if($(this).val()=='1'){
       $("#textbox3").show();
    }else{
        $("#textbox3").hide();
    }
  });
  //3rd step yes click show textarea
  $("input[name='radioset1']").click(function(){
      if($(this).val()=='1'){
         $("input[name='radioset1']").removeAttr('checked');
         $(this).prop('checked',true);
      }else{
          $("input[name='radioset1']").removeAttr('checked');
         $(this).prop('checked',true);
      }
  });
  $("input[name='radioset2']").click(function(){
      if($(this).val()=='1'){
         $("input[name='radioset2']").removeAttr('checked');
         $(this).prop('checked',true);
      }else{
          $("input[name='radioset2']").removeAttr('checked');
         $(this).prop('checked',true);
      }
  });
  $("input[name='radioset3']").click(function(){
      if($(this).val()=='1'){
         $("input[name='radioset3']").removeAttr('checked');
         $(this).prop('checked',true);
      }else{
          $("input[name='radioset3']").removeAttr('checked');
         $(this).prop('checked',true);
      }
  });
  $("input[name='radioset4']").click(function(){
      if($(this).val()=='1'){
         $("input[name='radioset4']").removeAttr('checked');
         $(this).prop('checked',true);
      }else{
          $("input[name='radioset4']").removeAttr('checked');
         $(this).prop('checked',true);
      }
  });

 $("#contract_trigger_save").bind('click', function (event) {   
    var user_id = $('#user_id').val();
    var first_name = $('#uname').val();
    var last_name = $('#lname').val();
    var email = $('#uemail').val();
    var taxilicencenumber = $('#dlicence').val();
    //var address = $('#home_addr').val();
    var address = '';
    var line_1 =$('#first_line').val();
    var line_2 =$('#second_line').val();
    var line_3 =$('#third_line').val();
    var post_town =$('#post_town').val();
    var postcode =$('#postcode').val();
    
    address =line_1+','+ line_2+','+ line_3+','+ post_town +','+postcode;

    function my_implode_js(separator,array){
           var temp = '';
           for(var i=0;i<array.length;i++){
               temp +=  array[i] 
               if(i!=array.length-1){
                    temp += separator  ; 
               }
           }//end of the for loop

           return temp;
    }//end of the function

    var dobday = $('#day').val();
    var dobmonth = $('#month').val();
    var dobyear = $('#year').val();
    var array = new Array(dobday,dobmonth,dobyear);
    var dob = my_implode_js('-',array);

    var dvladay = $('#dvladay').val();
    var dvlamonth = $('#dvlamonth').val();
    var dvlayear = $('#dvlayear').val();
    var array = new Array(dvladay,dvlamonth,dvlayear);
    var dvla_expiry = my_implode_js('-',array);

    var drivingday = $('#drivingday').val();
    var drivingmonth = $('#drivingmonth').val();
    var drivingyear = $('#drivingyear').val();
    var datearray = new Array(drivingday,drivingmonth,drivingyear);
    var datetestpassed = my_implode_js('-',datearray);

    var resident_amount = $('#residentamount').val();
    var mobile = $('#mobile').val();
    var national_insurance = $('#ninsurance').val();
    var pcolicencenumber = $('#taxi_no').val();
    var work_hours = $('#work_hours').val();
    var car_color = $('#car_color').val();
    var car_id = $("#car_id").val();
    var   candidate_comment  = $('#textbox3').val();
    var claims = $("input[name='radioset1']:checked").val();
    var motoroffence = $("input[name='radioset2']:checked").val();
    var criminalconviction = $("input[name='radioset3']:checked").val();
    var motorinsurance = $("input[name='radioset4']:checked").val();
    if (first_name == "" || last_name == "" || line_1 == "" || mobile == "" || email == "" || national_insurance == ""|| taxilicencenumber == "" || pcolicencenumber == "") {
    $('p.update.error').html("All Fields are mandatory*");
    //alert("*All Fields are mandatory*");
    //document.location.reload(true);
    
    /*$(this).parent().addClass('active');
    $("#first").removeAttr("style");
    $("#first").removeAttr("class");
    $(this).parent().css({'display': 'block!important'});
    event.preventDefault();
    return false;*/
    }
    else{
    $.ajax({
      type: "POST",
      url: baseurl_ajax+"php/client_new_update.php",
      data: {
        first_name: first_name,
        last_name: last_name,
        mobile: mobile,
        address: address,
        dob: dob,
        work_hours:work_hours,
        email: email,
        resident_amount: resident_amount,
        national_insurance:national_insurance,
        claims: claims,
        car_id : car_id,
        motoroffence: motoroffence,                                               
        criminalconviction: criminalconviction,
        motorinsurance: motorinsurance,
        pcolicencenumber: pcolicencenumber,
        datetestpassed: datetestpassed,                                               
        taxilicencenumber: taxilicencenumber,
        dvla_expiry: dvla_expiry,
        car_color: car_color,
        user_id:user_id,
        candidate_comment:candidate_comment                       
      },
      success: function (data) {
        window.top.close();
      }
    });
    }
  });
  $("#contract_trigger").bind('click', function () {
           
    var user_id = $('#user_id').val();
    var first_name = $('#uname').val();
    var last_name = $('#lname').val();
    //var address = $('#home_addr').val();
    var address = '';
    var line_1 =$('#first_line').val();
    var line_2 =$('#second_line').val();
    var line_3 =$('#third_line').val();
     var post_town =$('#post_town').val();
    var postcode =$('#postcode').val();
    address =line_1+','+ line_2+','+ line_3+','+ post_town+','+postcode;
    var email = $('#uemail').val();
    var taxilicencenumber = $('#dlicence').val();
    function my_implode_js(separator,array){
           var temp = '';
           for(var i=0;i<array.length;i++){
               temp +=  array[i] 
               if(i!=array.length-1){
                    temp += separator  ; 
               }
           }//end of the for loop

           return temp;
    }//end of the function

    var dobday = $('#day').val();
    var dobmonth = $('#month').val();
    var dobyear = $('#year').val();
    var array = new Array(dobday,dobmonth,dobyear);
    var dob = my_implode_js('-',array);

    var dvladay = $('#dvladay').val();
    var dvlamonth = $('#dvlamonth').val();
    var dvlayear = $('#dvlayear').val();
    var array = new Array(dvladay,dvlamonth,dvlayear);
    var dvla_expiry = my_implode_js('-',array);

    var drivingday = $('#drivingday').val();
    var drivingmonth = $('#drivingmonth').val();
    var drivingyear = $('#drivingyear').val();
    var datearray = new Array(drivingday,drivingmonth,drivingyear);
    var datetestpassed = my_implode_js('-',datearray);


    var resident_amount = $('#residentamount').val();
    var mobile = $('#mobile').val();
    var national_insurance = $('#ninsurance').val();
    var pcolicencenumber = $('#taxi_no').val();
    var licence_age = $('#taxilicenceage').val();
    var work_hours = $('#work_hours').val();
    var car_color = $('#car_color').val();
    var car_id = $("#car_id").val();

    var   candidate_comment  = $('#textbox3').val();

    var claims = $("input[name='radioset1']:checked").val();
    var motoroffence = $("input[name='radioset2']:checked").val();
    var criminalconviction = $("input[name='radioset3']:checked").val();
    var motorinsurance = $("input[name='radioset4']:checked").val();
    if (first_name == "" || last_name == "" || line_1 == "" || mobile == "" || email == "" || national_insurance == ""|| taxilicencenumber == "" || pcolicencenumber == "") {
    $('p.update.error').html("All Fields are mandatory*");
    }else{
   $.ajax({
      type: "POST",
      url: baseurl_ajax+"php/client_new_update.php",
      data: {
        first_name: first_name,
        last_name: last_name,
        address: address,
        dob: dob,
        mobile: mobile,
        email: email,
        work_hours:work_hours,
        national_insurance:national_insurance,
        taxilicencenumber: taxilicencenumber,
        dvla_expiry: dvla_expiry,
        pcolicencenumber: pcolicencenumber,
        resident_amount: resident_amount,
        datetestpassed: datetestpassed,
        car_id : car_id,
        car_color: car_color,
        motoroffence: motoroffence,                                               
        criminalconviction: criminalconviction,
        motorinsurance: motorinsurance,
        claims: claims,                                   
        candidate_comment:candidate_comment,
        user_id:user_id
      },
      success: function (res) {
        window.top.close();
        var user=$.parseJSON(res);
        user_id = user.id;
        //console.log(user);
        $('#user_id').val(user.id);
        var user_id = $('#user_id').val();
        //alert(user_id);
       //var user_id = $('#user_id').val();
        $.get(baseurl_ajax+'php/client_action.php',{user_id:user_id},function(res){
          var user=$.parseJSON(res); 
          var first_name = user[0].first_name;
          var last_name = user[0].last_name;
          var mobile = user[0].mobile;
          var address = user[0].address;
          var dob = user[0].dob;
          var national_insurance = user[0].national_insurance;
          var email = user[0].email;
          var resident_amount = user[0].residentamount;
          var driving_pass = user[0].datetestpassed;
          var taxilicencenumber = user[0].taxilicencenumber;
          var pcolicencenumber = user[0].pcolicencenumber;
          var license_age = user[0].taxilicenceage;
          var motor_offences = user[0].motoroffence;
          var criminal_offences = user[0].criminalconviction;
          var motor_insurance = user[0].motorinsurance;
          var user_claims = user[0].claims;
          var car_color = user[0].car_color;
          var work_hours = user[0].work_hours;
          var dvla_expiry = user[0].dvla_expiry;
          var car_id = $("#car_id").val();
          $.ajax({
            type: "POST",
            url: baseurl_ajax+"php/test_client_contracts.php",
            data: {
            first_name: first_name,
            last_name: last_name,
            mobile: mobile,
            address: address,
            dob: dob,
            work_hours:work_hours,
            email: email,
            resident_amount: resident_amount,                 
            national_insurance: national_insurance,
            car_id: car_id,
            driving_pass: driving_pass,
            taxilicencenumber: taxilicencenumber,
            dvla_expiry: dvla_expiry,
            pcolicencenumber: pcolicencenumber,
            license_age: license_age,
            motor_offences: motor_offences,
            criminal_offences: criminal_offences,
            motor_insurance: motor_insurance,
            user_claims: user_claims,
                 car_color:car_color,
            user_id: user_id   
            },
            success: function (data) {
                $('#contract_form').html(data);
                $('#contract_form').show();
                $(document).ready(function() {
    						  $("#signature").jSignature();
    						  var $sigdiv = $("#signature");
    						  var datapair = $sigdiv.jSignature("getData", "svgbase64");

                  $("#signature").bind("change", function(e) {  
    				        var data = $("#signature").jSignature("getData");  
    					      $("#client_signature").val(data);
    				      });
            	  });
            }
          });//ajax close
        });//get closed
      }//success closed
    });
    }
  });
  $("#order_trigger").bind('click', function () {

    $('#proposal_pdf').trigger('click');

    var user_id = $('#user_id').val();
    var car_color = $('#car_color').val();
    $.get(baseurl_ajax+'php/client_action.php',{user_id:user_id},function(res){
      var user=$.parseJSON(res);
      var email = user[0].email;
      var first_name = user[0].first_name;
      var last_name = user[0].last_name;
      var dvla = user[0].taxilicencenumber;
      $('#mt1').val(email);

     var area="";
     area +="Hi,"+first_name+" Congratulations on placing your order with ottocar.Your order confirmation is here ";
     $('textarea#mt4').html(area);
     
      /*var number = user[0].phone;
      var address = user[0].address;
      var dob = user[0].dob;
      var mobile_number = user[0].mobile;
      var national_insurance = user[0].national_insurance;
      var resident_amount = user[0].residentamount;
      //var resident= resident_aamount.split('/')
      //var resident_amount=resident['0']+'&nbsp;'+resident['1'];
      //var users_car = $('#DTE_Field_users-car').find(":selected").text();
      var driving_pass = user[0].datetestpassed;
      var taxi_license = user[0].taxilicencenumber;
      var license_age = user[0].taxilicenceage;
      var motor_offences = user[0].motoroffence;
      var criminal_offences = user[0].criminalconviction;
      var motor_insurance = user[0].motorinsurance;
      var user_claims = user[0].claims;
      var license_type = user[0].licencetype;
      var car_model = $("#car_id").val();
            $.ajax({
                type: "POST",
                url: baseurl_ajax+"php/test_client_order.php",
                data: {
                    first_name: first_name,
                    last_name: last_name,
                    number: number,
                    address: address,
                    dob: dob,
                    mobile_number: mobile_number,
                    email: email,
                    resident_amount: resident_amount,
                    users_car: car_model,
                    driving_pass: driving_pass,
                    taxi_license: taxi_license,
                    license_age: license_age,
                    motor_offences: motor_offences,
                    criminal_offences: criminal_offences,
                    motor_insurance: motor_insurance,
                    user_claims: user_claims,
                    national_insurance: national_insurance,
                    license_type: license_type,
                    car_color:car_color,
                    user_id: user_id
                    
                },

                success: function (data) {
                  //$('#contract_form').html('');
                 // $('#contract_form').show();
                  $('#order_form').html(data);
                  $('#order_form').show();

                  $(document).ready(function() {
                 $("#signature_order").jSignature();
                 var $sigdiv = $("#signature_order");
                 var datapair = $sigdiv.jSignature("getData", "svgbase64");

                      $("#signature_order").bind("change", function(e) {  
                  var data = $("#signature_order").jSignature("getData"); 
                  $("#client_signature_order").val(data);
                   });
                                        $("#reset").click(function(e){
                                          $("#signature_order").jSignature("clear");
                                          var data = $("#signature_order").jSignature("getData");
                                          $("#client_signature_order").val("");
                                          e.preventDefault();driver_
                                                          });

                  });
                }
            });*/
        });
    });
    $("#approved").bind('click', function () {
      var user_id = $('#user_id').val();
      $.get(baseurl_ajax+'php/client_approved.php',{user_id:user_id},function(res){
      });
    });
  });

function generatePdf(user_id,dir_name) {


  var url ="result_client_contracts.php?id="+user_id;
 url+="&form=proposal";


  $.ajax({
        url : url,
        method: "POST",
        data: $("#signature_form").serialize(),
    success : function(response){
     console.log(response);  

     var surl=response;
     //var dir_name="' . $first_name . '_' . $last_name . '_' . date('d-m-Y', strtotime($dob)) . '";
     var file_name="InsuranceDriverProposalForm.pdf";
      $.post("html_to_pdf.php",{"url":surl,"dir_name":dir_name,"file_name":file_name,"user_id":user_id},
       function(res){
       console.log(res);
       var res = res.split('-')
       console.log(res[0]);
       console.log(res[1]);
        $('textarea#mt4').append(res[0]);
        $('#proposal_pdf_button').html(res[1]);
      });

    },
    error : function(response){
      console.log(response);
    }
  }); 
}
/*function generateorderPdf(user_id,users_car,dir_name) {


   var url ="result_client_orders.php?id="+user_id+"&car_model="+users_car;
    url+="&form=order";
   $.ajax({
        url : url,
        method: "POST",
       
        data: $("#signature_form_order").serialize(),
        success : function(response){
            

          var surl=response;
         //var dir_name="' . $first_name . '_' . $last_name . '_' . date('d-m-Y', strtotime($dob)) . '";
   var file_name="ordercontract.pdf";
   $.post("html_to_pdf.php",{"url":surl,"dir_name":dir_name,"file_name":file_name},
  function(res){
    $('#order_pdf_button').html(res);
  });

          

          
        },
        error : function(response){
          console.log(response);
        }

      });
}*/
</script>

<script>


$(document).ready(function() {
var count = 0; // To Count Blank Fields
/*------------ Validation Function-----------------*/
$(".submit_btn").click(function(event) {
//var radio_check = $('.rad'); // Fetching Radio Button By Class Name
var input_field = $('.text_field'); // Fetching All Inputs With Same Class Name text_field & An HTML Tag textarea
//var text_area = $('textarea');
// Validating Radio Button
/*if (radio_check[0].checked == false && radio_check[1].checked == false) {
var y = 0;
} else {
var y = 1;
}*/
// For Loop To Count Blank Inputs
for (var i = input_field.length; i > count; i--) {
if (input_field[i - 1].value == '' || text_area.value == '') {
count = count + 1;
} else {
count = 0;
}
}
// Notifying Validation
if (count != 0 || y == 0) {
alert("All Fields are mandatory*");
event.preventDefault();
} else {
return true;
}
});
/*---------------------------------------------------------*/

$(".next_btn").click(function() {
  var first_name = $('#uname').val();
  var last_name = $('#lname').val();
  var line_1 =$('#first_line').val();
  var line_2 =$('#second_line').val();
  var line_3 =$('#third_line').val();
  var post_town =$('#post_town').val();
  var postcode =$('#postcode').val();
  var address = '';
  address =line_1+','+ line_2+','+ line_3+','+ post_town+','+postcode;
  var mobile = $('#mobile').val();
  var email = $('#uemail').val();
  var national_insurance = $('#ninsurance').val();
  var taxilicencenumber = $('#dlicence').val();
  var pcolicencenumber = $('#taxi_no').val();

if (first_name == "" || last_name == "" || line_1 == "" || mobile == "" || email == "" || national_insurance == ""|| taxilicencenumber == "" || pcolicencenumber == "") {
  $('p.update.error').html("All Fields are mandatory*");
} 
 else{
 // Function Runs On NEXT Button Click
$(this).parent().next().fadeIn('slow');
$(this).parent().css({
'display': 'none'
});

// Adding Class Active To Show Steps Forward;
$('.active').next().addClass('active');

$('.active').prev().removeClass('active');
//$(".active").siblings().css({"color": "red", "border": "2px solid red"});
        var thanks = $('.flow_chart li:last-child').attr('class');
        if (thanks=='active'){
        //alert(thanks);
          $('#progressbar').css('display','none');
        }
  }
});

$(".next_btn_order").click(function() { // Function Runs On NEXT Button Click

//$('#order_pdf').trigger('click');
$(this).parent().next().fadeIn('slow');
$(this).parent().css({
'display': 'none'
});
// Adding Class Active To Show Steps Forward;
$('.active').next().addClass('active');

$('.active').prev().removeClass('active');
//$(".active").siblings().css({"color": "red", "border": "2px solid red"});
});

$(".pre_btn").click(function() { // Function Runs On PREVIOUS Button Click
$(this).parent().prev().fadeIn('slow');
$(this).parent().css({
'display': 'none'
});
// Removing Class Active To Show Steps Backward;
$('.active:last').removeClass('active');
});
// Validating All Input And Textarea Fields
$(".submit_btn").click(function(e) {
if ($('input').val() == "" || $('textarea').val() == "") {
alert("All Fields are mandatory*");
return false;
} else {
return true;
}
});
});
</script>
<script src="https://getaddress.io/js/jquery.getAddress-2.0.5.min.js"></script>
 <script>
var row='undefined';
$('#lookup_field').getAddress({
    api_key: 'g8bO1tjX4ESBDnhWHV8aPQ8101',  
   
    output_fields:{
            line_1: '#first_line',  
            line_2: '#second_line',         
            line_3: '#third_line',
            post_town: '#post_town',
            county: '#county',
            postcode: '#postcode'
    },
                                                                                                              
onLookupSuccess: function(data)
{},
onLookupError: function(){},onAddressSelected: function(elem,index){}
});
</script>
</body>
</html>