<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include("../../php/DataTables.php");

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;
// dropbox file upload code




// Build our Editor instance and process the data coming from _POST
if ($_POST) {

    if (isset($_POST['condd'])) {
        $db->sql('update users set `car`="car pending" where car="' . $_POST['condd'] . '"');
    }


    $id = 0;
    if (@$_POST['data'][0]) {


    } else if ($_POST['action'] == 'remove') {

        if (isset($_POST['data'])) {
            foreach (@$_POST['data'] as $k => $v) {
                $c = explode('_', $k);
                $row[$k] = @$c[1];
                $id = $k;
                $user_id = @$c[1];

            }



            $car_reg = $_POST['data'][$id]['cars']['reg'];
            if ($car_reg) {
                $result_1 = $db->sql('update cars set `availability`="0" , `customer_id`="0" , `condition`="used"  where `reg`= "' . $car_reg . '"');

            }
        }


    } else if ($_POST['action'] == 'edit') {

        //print_r($_POST['data']);exit;
        if (isset($_POST['data'])) {
            foreach (@$_POST['data'] as $k => $v) {
                $c = explode('_', $k);
                $row[$k] = @$c[1];
                $id = $k;
                $user_id = @$c[1];

            }


            if (@$_POST['data'][$id]['users']['shortterm_check'] == '1') {
                $car = @$_POST['data'][$id]['users']['shortterm_car'];
                $result_7 = $db->sql('update users set `shortterm_car`="' . $car . '" where  id="' . $user_id . '"');

            }

            if (@$_POST['data'][$id]['users']['site'] == 5) {

                if (@$_POST['data'][$id]['users']['car'] == '') {

                    $result_user = $db->sql('select * from cars where  customer_id="' . $user_id . '"');
                    $lastresult = $result_user->fetchAll();
                    if (count($lastresult) > 0) {
                        foreach ($lastresult as $k => $v) {
                            $result_1 = $db->sql('update cars set `availability`="0" , `condition`="used", `customer_id`=0 where  id="' . $v['id'] . '"');


                        }
                    }
                    $result_7 = $db->sql('update users set `car`="" where  id="' . $user_id . '"');


                }


            } else if (@$_POST['data'][$id]['users']['site'] == 4) {

                if (@$_POST['data'][$id]['users']['car'] != '') {

                    $result_user = $db->sql('select * from cars where  customer_id="' . $user_id . '"');
                    $lastresult = $result_user->fetchAll();
                    if (count($lastresult) > 0) {
                        foreach ($lastresult as $k => $v) {
                            $result_1 = $db->sql('update cars set `availability`="0" 
						 where  id="' . $v['id'] . '"');
                        }
                    }

                    $result = $db->sql('update cars set `availability`="-1" , 
						`condition`="accident" where  customer_id="' . $user_id . '"');


                }


            } else if (@$_POST['data'][$id]['users']['site'] == 10) {

                if (@$_POST['data'][$id]['users']['car'] != '') {

                    $result_user = $db->sql('select * from cars where  customer_id="' . $user_id . '"');
                    $lastresult = $result_user->fetchAll();
                    if (count($lastresult) > 0) {
                        foreach ($lastresult as $k => $v) {
                            $result_1 = $db->sql('update cars set `availability`="0" 
						 where  id="' . $v['id'] . '"');
                        }
                    }

//                    $result = $db->sql('update cars set `availability`="-1" ,
//						`condition`="accident" where  customer_id="' . $user_id . '"');
//

                }


            } else {


                if (@$_POST['data'][$id]['users']['car'] != '') {

                    $result_user = $db->sql('select * from cars where  customer_id= "' . $user_id . '"');
                    $lastresult = $result_user->fetchAll();
                    if (count($lastresult) > 0) {
                        foreach ($lastresult as $k => $v) {
                            $result_1 = $db->sql('update cars set availability="0" where  id= "' . $v['id'] . '"');
                        }
                    }
                    $result = $db->sql('update cars set availability=1,`customer_id`=' . $row[$id] . ' where  reg= "' . $_POST['data'][$id]['users']['car'] . '"');
                }else{


                     $result_user = $db->sql('select * from cars where  customer_id= "' . $user_id . '"');
                    $lastresult = $result_user->fetchAll();
                    if (count($lastresult) > 0) {
                        foreach ($lastresult as $k => $v) {
                            $result_1 = $db->sql('update cars set availability="0" where  id= "' . $v['id'] . '"');
                        }
                    }

                }

            }
        }

    }
}

if (isset($_GET['action'])) {
    $id = $_GET['id'];


    if ($id == 'all') {


        Editor::inst($db, 'users')
            ->fields(
                Field::inst('users.first_name'),
                Field::inst('users.last_name'),
                Field::inst('users.email'),
                Field::inst('users.address'),
                Field::inst('users.mobile'),
                Field::inst('users.phone'),
                Field::inst('users.national_insurance'),
                Field::inst('users.taxilicencenumber'),
                Field::inst('users.pcolicencenumber'),
                Field::inst('users.residentamount'),
                Field::inst('users.shortterm_check'),
                Field::inst('users.shortterm_car'),
                Field::inst('users.blacklist'),
                Field::inst('users.notes'),
                Field::inst('users.pickup_date'),
                Field::inst('users.site')
                    ->options('sites', 'id', 'name'),
                Field::inst('sites.name'),
                Field::inst('users.car')
                    //->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
                    ->options('cars', 'reg', 'reg'),
                Field::inst('users.cartype')
			->options( 'car_type', 'id','model'  ),
                Field::inst('cars.type'),
                Field::inst('cars.reg'),
                Field::inst('car_type.model'),
                Field::inst('car_type.id'),
                Field::inst('users.Rental_Price'),
                Field::inst('users.Rental_Number'),
                Field::inst('users.Frequency'),
                Field::inst('users.how_to_find')

            )
            ->join(
                Mjoin::inst('files')
                    ->link('users.id', 'users_files.user_id')
                    ->link('files.id', 'users_files.file_id')
                    ->fields(
                        Field::inst('id')
                            ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')

                                ->db('files', 'id', array(
                                    'filename' => Upload::DB_FILE_NAME,
                                    'filesize' => Upload::DB_FILE_SIZE,
                                    'web_path' => Upload::DB_WEB_PATH,
                                    'system_path' => Upload::DB_SYSTEM_PATH
                                ))
                                ->validator(function ($file) {
                                    return $file['size'] >= 5120000 ?
                                        "Files must be smaller than 5MB" :
                                        null;
                                })
                                ->allowedExtensions(['png','pdf','doc','docx','jpg'], "Please upload")
                            )
                    )
            )
            ->leftJoin('sites', 'sites.id', '=', 'users.site')
            ->leftJoin('cars', 'cars.reg', '=', 'users.car')
          //  ->leftJoin('car_type', 'car_type.id', '=', 'users.cartype')
            ->leftJoin('car_type', 'cars.type', '=', 'car_type.id')
            ->where('users.user_type', 'driver')
            ->process($_POST)
            ->json();

    } else {


        Editor::inst($db, 'users')
            ->fields(
                Field::inst('users.first_name'),
                Field::inst('users.last_name'),
                Field::inst('users.email'),
                Field::inst('users.address'),
                Field::inst('users.mobile'),
                Field::inst('users.phone'),
                Field::inst('users.national_insurance'),
                Field::inst('users.taxilicencenumber'),
                Field::inst('users.pcolicencenumber'),
                Field::inst('users.residentamount'),
                Field::inst('users.shortterm_check'),
                Field::inst('users.shortterm_car'),
                Field::inst('users.blacklist'),
                Field::inst('users.notes'),
                Field::inst('users.pickup_date'),
                Field::inst('users.site')
                    ->options('sites', 'id', 'name'),
                Field::inst('sites.name'),
                Field::inst('users.car')
                    ->options('cars', 'reg', 'reg', array('availability' => 0)),
                Field::inst('users.cartype')
			->options( 'car_type', 'id','model'  ),
                Field::inst('cars.type'),
                Field::inst('cars.reg'),
                Field::inst('car_type.model'),
                Field::inst('car_type.id'),
                //Field::inst('car_type.model')
                Field::inst('users.Rental_Price'),
                Field::inst('users.Rental_Number'),
                Field::inst('users.Frequency'),
                Field::inst('users.how_to_find')

            )
            ->join(
                Mjoin::inst('files')
                    ->link('users.id', 'users_files.user_id')
                    ->link('files.id', 'users_files.file_id')
                    ->fields(
                        Field::inst('id')
                            ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')
                                ->db('files', 'id', array(
                                    'filename' => Upload::DB_FILE_NAME,
                                    'filesize' => Upload::DB_FILE_SIZE,
                                    'web_path' => Upload::DB_WEB_PATH,
                                    'system_path' => Upload::DB_SYSTEM_PATH
                                ))
                                ->validator(function ($file) {
                                    return $file['size'] >= 5120000 ?
                                        "Files must be smaller than 5MB" :
                                        null;
                                })
                                ->allowedExtensions(['png','pdf','doc','docx','jpg'], "Please upload")
                            )
                    )
            )
            ->leftJoin('sites', 'sites.id', '=', 'users.site')
            ->leftJoin('cars', 'cars.reg', '=', 'users.car')
          // ->leftJoin('car_type', 'car_type.id', '=', 'users.cartype')
            ->leftJoin('car_type', 'cars.type', '=', 'car_type.id')
            ->where('users.site', $id)
            ->where('users.user_type', 'driver')
            ->process($_POST)
            ->json();
    }


} else {

    Editor::inst($db, 'users')
        ->fields(
            Field::inst('users.first_name'),
            Field::inst('users.last_name'),
            Field::inst('users.email'),
            Field::inst('users.address'),
            Field::inst('users.mobile'),
            Field::inst('users.phone'),
            Field::inst('users.national_insurance'),
            Field::inst('users.taxilicencenumber'),
            Field::inst('users.pcolicencenumber'),
            Field::inst('users.residentamount'),
            Field::inst('users.shortterm_check'),
            Field::inst('users.shortterm_car'),
            Field::inst('users.user_type'),
            Field::inst('users.notes'),
            Field::inst('users.pickup_date'),
            Field::inst('users.site')
                ->options('sites', 'id', 'name'),
            Field::inst('sites.name'),
            Field::inst('users.car')
                //->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
                ->options('cars', 'reg', 'reg'),
                Field::inst('users.cartype')
			->options( 'car_type', 'id','model'  ),
            Field::inst('cars.type'),
            Field::inst('cars.reg'),
            Field::inst('car_type.model')
                ->options('car_type', 'id', 'model'),
            Field::inst('car_type.id'),
            Field::inst('users.Rental_Price'),
            Field::inst('users.Rental_Number'),
            Field::inst('users.Frequency'),
            Field::inst('users.how_to_find')

                

        )
        ->join(
            Mjoin::inst('files')
                ->link('users.id', 'users_files.user_id')
                ->link('files.id', 'users_files.file_id')
                ->fields(
                    Field::inst('id')
                        ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/ottohub/upload/driver/__ID__.__EXTN__')
                            ->db('files', 'id', array(
                                'filename' => Upload::DB_FILE_NAME,
                                'filesize' => Upload::DB_FILE_SIZE,
                                'web_path' => Upload::DB_WEB_PATH,
                                'system_path' => Upload::DB_SYSTEM_PATH
                            ))
                            ->validator(function ($file) {
                                return $file['size'] >= 5120000 ?
                                    "Files must be smaller than 5MB" :
                                    null;
                            })
                            ->allowedExtensions(['png','pdf','doc','docx','jpg'], "Please upload")
                        )
                )
        )
        ->leftJoin('sites', 'sites.id', '=', 'users.site')
        ->leftJoin('cars', 'cars.reg', '=', 'users.car')
       // ->leftJoin('car_type', 'car_type.id', '=', 'users.cartype')
        ->leftJoin('car_type', 'cars.type', '=', 'car_type.id')
        ->where('users.user_type', 'driver')
        ->process($_POST)
        ->json();

}

