<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Mjoin,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;

// Build our Editor instance and process the data coming from _POST
if($_POST){

 $id=0;
		if(@$_POST['data'][0]){


		}else{ 
				if(isset($_POST['data'])){
					foreach(@$_POST['data'] as $k=>$v){
						$c=explode('_',$k);
						$row[$k]=@$c[1];
						$id=$k;
                                                $user_id=@$c[1];

					}

					

						if(@$_POST['data'][$id]['users']['car']!=''){

$result_user=$db->sql('select * from cars where  availability= "'.$user_id.'"');
				$lastresult=$result_user->fetchAll();
			if(count($lastresult) > 0){
				foreach($lastresult as $k=>$v){
			$result_1=$db->sql('update cars set availability="0" where  id= "'.$v['id'].'"');
				}
			}
				$result=$db->sql('update cars set availability='.$row[$id].' where  reg= "'.$_POST['data'][$id]['users']['car'].'"');
						}
			}

		}
}

if(isset($_GET['action'])){
	$id=$_GET['id'];

			Editor::inst( $db, 'users' )
				->fields(
					Field::inst( 'users.first_name' ),
					Field::inst( 'users.last_name' ),
			                Field::inst( 'users.email' ),
			                Field::inst( 'users.mobile' ),
					Field::inst( 'users.phone' ), 
			                Field::inst( 'users.national_insurance' ),
			                Field::inst( 'users.licence_number' ),
			                Field::inst( 'users.bank_card_number' ),
			                Field::inst( 'users.bank_expiry_date' ),
			                Field::inst( 'users.bank_cvv' ),
			                Field::inst( 'users.pickup_date' ),
							Field::inst( 'users.site' )
								->options( 'sites', 'id', 'name' ),
							Field::inst( 'sites.name' ),
							Field::inst( 'users.car' )
								->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
							Field::inst('cars.type'),
							Field::inst('cars.reg')
					
				)
				->join(
					Mjoin::inst( 'files' )
						->link( 'users.id', 'users_files.user_id' )
						->link( 'files.id', 'users_files.file_id' )
						->fields(
							Field::inst( 'id' )
								->upload( Upload::inst( $_SERVER['DOCUMENT_ROOT'].'/upload/__ID__.__EXTN__' )
									->db( 'files', 'id', array(
										'filename'    => Upload::DB_FILE_NAME,
										'filesize'    => Upload::DB_FILE_SIZE,
										'web_path'    => Upload::DB_WEB_PATH,
										'system_path' => Upload::DB_SYSTEM_PATH
									) )
									->validator( function ( $file ) {
										return$file['size'] >= 200000 ?
											"Files must be smaller than 200K" :
											null;
									} )
									->allowedExtensions( [ 'png', 'jpg' ], "Please upload an image" )
								)
						)
				)
				->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
				->leftJoin( 'cars', 'cars.reg', '=', 'users.car' )
				->where('users.site',$id)

				->process( $_POST )
				->json();


}else{  

			Editor::inst( $db, 'users' )
				->fields(
					Field::inst( 'users.first_name' ),
					Field::inst( 'users.last_name' ),
			                Field::inst( 'users.email' ),
			                Field::inst( 'users.mobile' ),
					Field::inst( 'users.phone' ), 
			                Field::inst( 'users.national_insurance' ),
			                Field::inst( 'users.licence_number' ),
			                Field::inst( 'users.bank_card_number' ),
			                Field::inst( 'users.bank_expiry_date' ),
			                Field::inst( 'users.bank_cvv' ),
			                Field::inst( 'users.pickup_date' ),
							Field::inst( 'users.site' )
								->options( 'sites', 'id', 'name' ),
							Field::inst( 'sites.name' ),
							Field::inst( 'users.car' )
								->options( 'cars', 'reg', 'reg', array('availability'=>0) ),
							Field::inst('cars.type'),
							Field::inst('cars.reg')
					
				)
				->join(
					Mjoin::inst( 'files' )
						->link( 'users.id', 'users_files.user_id' )
						->link( 'files.id', 'users_files.file_id' )
						->fields(
							Field::inst( 'id' )
								->upload( Upload::inst( $_SERVER['DOCUMENT_ROOT'].'/upload/__ID__.__EXTN__' )
									->db( 'files', 'id', array(
										'filename'    => Upload::DB_FILE_NAME,
										'filesize'    => Upload::DB_FILE_SIZE,
										'web_path'    => Upload::DB_WEB_PATH,
										'system_path' => Upload::DB_SYSTEM_PATH
									) )
									->validator( function ( $file ) {
										return$file['size'] >= 200000 ?
											"Files must be smaller than 200K" :
											null;
									} )
									->allowedExtensions( [ 'png', 'jpg' ], "Please upload an image" )
								)
						)
				)
				->leftJoin( 'sites', 'sites.id', '=', 'users.site' )
				->leftJoin( 'cars', 'cars.reg', '=', 'users.car' )
				
				->process( $_POST )
				->json();

}
