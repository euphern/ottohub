<?php
session_start();
$target_dir = "img/";

$temp = explode(".", $_FILES["fileToUpload"]["name"]);
$newfilename = round(microtime(true)) . '.' . end($temp);

$target_file = $target_dir.$newfilename;

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size

// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". $newfilename. " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

// enhance upload
/*require 'image_enhance/src/class.upload.php';

$t=time();

$handle = new upload($_FILES['fileToUpload']);
if ($handle->uploaded) {
  $handle->file_new_name_body   = $t;
  $handle->image_greyscale = true;
  //$handle->image_negative = true;
  //$handle->image_crop = array(120,500,200,380);
  $handle->image_contrast = 120;
  $handle->image_convert = 'png';
  $handle->process('img');
  if ($handle->processed) 
  {
    //exit;
    //echo 'image resized';
    $handle->clean(); 

  } 
  else 
  {
    echo 'error : ' . $handle->error;//exit;
  }  
}*/


        /*
        
           Sample project for OCRWebService.com (REST API).
           Extract text from scanned images and convert into editable formats.
           Please create new account with ocrwebservice.com via http://www.ocrwebservice.com/account/signup and get license code

        */

        // Provide your user name and license code
        $license_code = "6811BA99-CEF6-472D-917B-0E812B02BC54";
        $username =  "HIMADRI24";

        //$license_code = "4E5F9D1E-1B8C-491E-9343-32B7C25CAC2B";
        //$username =  "sz12";

         //$license_code = "C797C827-2C29-4800-B03A-D9242A9E7BC0";
        //$username =  "euphern";

        /*

           You should specify OCR settings. See full description http://www.ocrwebservice.com/service/restguide
         
           Input parameters:
         
	   [language]     - Specifies the recognition language. 
	   		    This parameter can contain several language names separated with commas. 
                            For example "language=english,german,spanish".
			    Optional parameter. By default:english
        
	   [pagerange]    - Enter page numbers and/or page ranges separated by commas. 
			    For example "pagerange=1,3,5-12" or "pagerange=allpages".
                            Optional parameter. By default:allpages
         
           [tobw]	  - Convert image to black and white (recommend for color image and photo). 
			    For example "tobw=false"
                            Optional parameter. By default:false
         
           [zone]         - Specifies the region on the image for zonal OCR. 
			    The coordinates in pixels relative to the left top corner in the following format: top:left:height:width. 
			    This parameter can contain several zones separated with commas. 
		            For example "zone=0:0:100:100,50:50:50:50"
                            Optional parameter.
          
           [outputformat] - Specifies the output file format.
                            Can be specified up to two output formats, separated with commas.
			    For example "outputformat=pdf,txt"
                            Optional parameter. By default:doc

           [gettext]	  - Specifies that extracted text will be returned.
			    For example "tobw=true"
                            Optional parameter. By default:false
        
           [description]  - Specifies your task description. Will be returned in response.
                            Optional parameter. 


	   !!!!  For getting result you must specify "gettext" or "outputformat" !!!!  

	*/


        // Build your OCR:

        // Extraction text with English language
        //$url = 'http://www.ocrwebservice.com/restservices/processDocument?gettext=true&tobw=true&outputformat=txt';
        $url = 'http://www.ocrwebservice.com/restservices/processDocument?gettext=true';

        // Extraction text with English and german language using zonal OCR
        // $url = 'http://www.ocrwebservice.com/restservices/processDocument?language=english,german&zone=0:0:600:400,500:1000:150:400';

        // Convert first 5 pages of multipage document into doc and txt
        // $url = 'http://www.ocrwebservice.com/restservices/processDocument?language=english&pagerange=1-5&outputformat=doc,txt';
      

        // Full path to uploaded document
       //$filePath = 'img/'.$t.'.png';
        //echo $filePath;exit;
        $filePath = 'img/'.$newfilename;
       //$filePath = 'img/driving44.jpeg';
        //$filePath = 'http://staging.euphern.com/ocr/driving33.jpeg';
        //$filePath = 'http://ottohub.euphern.com/demo/upload/enquiry/498.png';
  
        $fp = fopen($filePath, 'r');
        $session = curl_init();    

        curl_setopt($session, CURLOPT_URL, $url);
        curl_setopt($session, CURLOPT_USERPWD, "$username:$license_code");

        curl_setopt($session, CURLOPT_UPLOAD, true);
        curl_setopt($session, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($session, CURLOPT_TIMEOUT, 200);
        curl_setopt($session, CURLOPT_HEADER, false);


        // For SSL using
        //curl_setopt($session, CURLOPT_SSL_VERIFYPEER, true);

        // Specify Response format to JSON or XML (application/json or application/xml)
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
 
        curl_setopt($session, CURLOPT_INFILE, $fp);
        curl_setopt($session, CURLOPT_INFILESIZE, filesize($filePath));

        $result = curl_exec($session);

  	$httpCode = curl_getinfo($session, CURLINFO_HTTP_CODE);
        curl_close($session);
        fclose($fp);
	
        if($httpCode == 401) 
	{
           // Please provide valid username and license code
           die('Unauthorized request');
        }

        // Output response
	$data = json_decode($result);

        if($httpCode != 200) 
	{
	   // OCR error
           die($data->ErrorMessage);
        }

        // Task description
	//echo 'TaskDescription:'.$data->TaskDescription."\r\n";

        // Available pages 
	//echo 'AvailablePages:'.$data->AvailablePages."\r\n";

        // Extracted text
        //echo 'OCRText='.$data->OCRText[0][0]."\r\n";exit;


            $output=$data->OCRText[0][0];

//$output="â€¢ K DRIVING LICENCE 1. AGYEMFRA 2. JOHN GEORGE 3. 05-11-61 GHANA 4a. 28-11-10 4b. 10-09-17 4c. DVLA 5. AGYEM611051JG8CW 07 afiset-I e.% 8. FLAT 79. BANSTEAD COURT, 60 WESTWAY. LONDON, W12 00J B,B1,1,k,l,n,p ";

/*$output="
3. 31.05.1974 UNITED KINGDOM 
4a. 16.09.2016 
4c. DVLA 
4b. 13.11.2019 
5 ANJOR705314VA9PH 22 Ist_AAAA 
8. 95 THE CRESCENT. SLOUGH. SL1 2LJ â€¢ al DRIVING LICENCE 
1. ANJORIN 
2. DR VINCENT AKINTUNDE 0 
9. AM/A/131/B/t/k/p/q gs\1111___";*/


//$output='3. 31.05.1974 UNITED KINGDOM 4a. 16.09.2016 4c. DVLA 4b. 13.11.2019 5 ANJOR705314VA9PH 22 8. 95 THE CRESCENT, SLOUGH. SL1 2LJ DRIVING LICENCE 1. ANJORIN 2. DR VINCENT AKINTUNDE 0 9. AM/A/61/13/1/k/p/q';

//$output='***** *UK* **,** DRIVING LICENCE 1. FARAH 2. ABDIFATAH ABDI 3. 23.10.1972 SOMALIA 4a. 02.03.2017 4c. DVLA 4b. 12.11.2022 5 FARAH710232AA9DT 14 . , f-P404_ 8 17 HAYNES CLOSE, LONDON, N17 00X 9. AM/A/B1/B/D1/D/f/k/p/q';

//echo $output;exit;

$string_out=str_replace("1. ","*1=",$output);
$string_out=str_replace("2. ","*2=",$string_out);
$string_out=str_replace("3. ","*3=",$string_out);
$string_out=str_replace("4a. ","*4a=",$string_out);
$string_out=str_replace("4c. ","*4c=",$string_out);
$string_out=str_replace("4b. ","*4b=",$string_out);


//dot checking for 5
$checkfive=substr_count($string_out, ' 5 ');
if($checkfive==1)
{
$string_out=str_replace(" 5 ","*5=",$string_out);
}
else
{
$string_out=str_replace("5. ","*5=",$string_out);   
}


//dot checking for 8
$checkeight=substr_count($string_out, ' 8 ');
if($checkeight==1)
{
$string_out=str_replace(" 8 ","*8=",$string_out);
}
else
{
$string_out=str_replace("8. ","*8=",$string_out);  
}

//dot checking for 9
$checknine=substr_count($string_out, ' 9 ');
if($checknine==1)
{
$string_out=str_replace(" 9 ","*9=",$string_out);
}
else
{
$string_out=str_replace("9. ","*9=",$string_out);  
}


$string_out=$string_out."*";



//echo $string_out;exit;


function strafter($string, $substring) {
  $pos = strpos($string, $substring);
  if ($pos === false)
   return $string;
  else  
   return(substr($string, $pos+strlen($substring)));
}

function strbefore($string, $substring) {
  $pos = strpos($string, $substring);
  if ($pos === false)
   return $string;
  else  
   return(substr($string, 0, $pos));
}

		$lic_array = array();


        //last name
        $myvar_lastname = $string_out;
        $myvar_after_lastname = strafter($myvar_lastname,'*1=');
        $lastname= strbefore($myvar_after_lastname,' *');


        //first name
        $myvar_firstname = $string_out;
        $myvar_after_firstnamee = strafter($myvar_firstname,'*2=');
        $firstname= strbefore($myvar_after_firstnamee,'*');

        //dob
        $myvar_dob = $string_out;
        $myvar_after_dob = strafter($myvar_dob,'*3=');
        $dob_process= strbefore($myvar_after_dob,'*');
        $dob_exploded= explode(" ",$dob_process);

        $dob=$dob_exploded[0];
        $birth_place=$dob_exploded[1];


        //licence start
        $myvar_licstart = $string_out;
        $myvar_after_licstart = strafter($myvar_licstart,'*4a=');
        $lic_startdate= strbefore($myvar_after_licstart,'*');

        //licence expiry
        $myvar_licexp = $string_out;
        $myvar_after_licexp = strafter($myvar_licexp,'*4b=');
        $lic_expirydate= strbefore($myvar_after_licexp,'*');

        //licence type
        $myvar_lictype = $string_out;
        $myvar_after_lictype = strafter($myvar_lictype,'*4c=');
        $lictype= strbefore($myvar_after_lictype,'*');


        //licence id.
        $myvar_licenseid = $string_out;
        $myvar_after_licenseid = strafter($myvar_licenseid,'*5=');
        $licid_process= strbefore($myvar_after_licenseid,'*');
        $licid_expolded= explode(" ",$licid_process);
        //array_pop($licid_expolded);
        $licenseid=$licid_expolded[0]." ".$licid_expolded[1];
        //echo $licenseid;exit;
       

        //address.
        $myvar_address = $string_out;
        $myvar_after_address = strafter($myvar_address,'*8=');   
        $address= strbefore($myvar_after_address,'*');   


        $lic_array[] = array('firstname' => "$firstname" ,'lastname' => "$lastname" ,'dob' => "$dob"  ,'birth_place' => "$birth_place" ,'lic_startdate' => "$lic_startdate" ,'lic_expirydate' => "$lic_expirydate" ,'lictype' => "$lictype" ,'licenseid' => "$licenseid" ,'address' => "$address");


        $_SESSION['lic_array'] = $lic_array;

        //header('Location: http://192.168.200.13/ottohub/examples/php/client_new.php');
        header('Location: http://ottohub.euphern.com/demo/examples/php/client_new.php');


       //$json = json_encode($lic_array);

       //print_r($json);

        // For zonal OCR: OCRText[z][p]    z - zone, p - pages

        // Get First zone from each page 
        //echo 'OCRText[0][0]='.$data->OCRText[0][0]."\r\n";
        //echo 'OCRText[0][1]='.$data->OCRText[0][1]."\r\n";


        // Get second zone from each page
        //echo 'OCRText[1][0]='.$data->OCRText[1][0]."\r\n";
        //echo 'OCRText[1][1]='.$data->OCRText[1][1]."\r\n";


        // Download output file (if outputformat was specified)

        //$url = $data->OutputFileUrl;   
        //$content = file_get_contents($url);
        //file_put_contents('converted_document.doc', $content);

        // End recognition

?>