<?php
/**
 * Created by PhpStorm.
 * User: deb
 * Date: 22/11/16
 * Time: 11:08 AM
 */

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;


Editor::inst($db, 'users')
    ->fields(
        Field::inst('users.first_name'),
        Field::inst('users.last_name'),
        Field::inst('users.phone'),
        Field::inst('users.licencetype'),
        Field::inst('users.address'),
         Field::inst('users.user_type'),
        Field::inst('users.site')
            ->options('sites', 'id', 'name'),
        Field::inst('sites.name'),
        Field::inst('users.car')
            ->options('car_type', 'id', 'model'),
        Field::inst('car_type.model'),
        Field::inst('car_type.id')

    )
    ->leftJoin('sites', 'sites.id', '=', 'users.site')
    ->leftJoin('car_type', 'car_type.id', '=', 'users.car')
    ->where('users.user_type', 'blacklisted')
    //->where('cars.car','1')
    ->process($_POST)
    ->json();


