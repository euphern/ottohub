<?php
	
	//Dropbox Access Token
	$access_token = 'LZGo3bViDbAAAAAAAAAAFy4TSVcBpnehWszlJYi6WYeLoNbV63X3Dx5JmZqQ1zmS';
	
	//Folder path for which list of sub folder will be fetched
	$dropbox_folder_path = '/Ottohub/Drivers';

	//Set parameters for "Dropbox-API-Arg" header
	$dropbox_api_arg = array(
		'path' => $dropbox_folder_path,
		'recursive' => false,
		'include_media_info' => false,
		'include_deleted' => false,
		'include_has_explicit_shared_members' => true
	);

	/*echo "<pre>";
	echo json_encode($dropbox_api_arg);
	echo "</pre>";*/

	// Set headers
  	$headers = array(
  		'User-Agent: api-explorer-client',
  		'Authorization: Bearer '.$access_token, // This is the access token for a particular user
  		'Content-Type: application/json',
  		//'Dropbox-API-Arg: '.stripslashes(json_encode($dropbox_api_arg)) // This is the array set at the very begining
  	);

  	// Get cURL resource
	$curl = curl_init();

	// Set some options - we are passing in a useragent too here
	curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_URL => 'https://api.dropboxapi.com/2/files/list_folder',
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POST => TRUE,
			//CURLOPT_BINARYTRANSFER => TRUE,
			CURLOPT_POSTFIELDS => stripslashes(json_encode($dropbox_api_arg))
		)
	);

	// Send the request & save response to $resp
	$res = curl_exec($curl);
	// echo $res;

	// Convert the json response into array
	$result = json_decode($res, 1);

	/*echo "<pre>";
	print_r($result);
	echo "</pre>";*/

	// Close request to clear up some resources
	curl_close($curl);

	if( !empty($result['entries']) && count($result['entries']) > 0 && $result['has_more'] ) {
		// Get cURL resource
		$curl2 = curl_init();

		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl2, array(
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_URL => 'https://api.dropboxapi.com/2/files/list_folder/continue',
				CURLOPT_HTTPHEADER => $headers,
				CURLOPT_POST => TRUE,
				//CURLOPT_BINARYTRANSFER => TRUE,
				CURLOPT_POSTFIELDS => stripslashes(json_encode($dropbox_api_arg))
			)
		);

		// Send the request & save response to $resp
		$res2 = curl_exec($curl2);
		// echo $res;

		// Convert the json response into array
		$result2 = json_decode($res2, 1);

		/*echo "<pre>";
		print_r($result2);
		echo "</pre>";*/

		// Close request to clear up some resources
		curl_close($curl2);

		if( !empty($result2['entries']) && count($result2['entries']) > 0 ) {
			foreach ($result2['entries'] as $key => $value) {
				array_push($result['entries'], $value);
			}
		}
	}
	
	$result['entries'] = array_reverse($result['entries']);
?>
<!DOCTYPE html>
<html>
<head id="head_part">
    <meta charset="utf-8">
    <link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <title>OttoHub beta</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.0/css/select.dataTables.min.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.2.0/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/editor.dataTables.css">

    <link rel="stylesheet" type="text/css" href="../../css/otto.css">
    <!--  <link rel="stylesheet" type="text/css" href="../../css/red.css">
     <link rel="stylesheet" type="text/css" href="../../css/blue.css">  -->
    <link rel="stylesheet" type="text/css" href="../resources/syntax/shCore.css">
    <link rel="stylesheet" type="text/css" href="../resources/demo.css">
    <script src="http://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>


    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/select/1.2.0/js/dataTables.select.min.js">
    </script>
    <script type="text/javascript" language="javascript"
            src="https://cdn.datatables.net/buttons/1.2.0/js/dataTables.buttons.min.js">
    </script>
    <script type="text/javascript" src="http://momentjs.com/downloads/moment.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/dataTables.editor.js">
    </script>
    <script type="text/javascript" language="javascript" src="../resources/syntax/shCore.js">
    </script>
    <script type="text/javascript" language="javascript" src="../resources/demo.js">
    </script>

    <script type="text/javascript" language="javascript" src="../../js/custom.js">
    </script>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <script type="text/javascript" language="javascript" class="init">
    </script>
</head>
<body>
<section
        style="display:block;width:100%;float:left;background-color:#333e48;padding-top:12px;padding-bottom:12px;margin-bottom:30px">
    <header style=" margin: 0 auto;max-width: 1100px;">
        <h1 style="float:left;color:white;margin:0;font-size:2.9em;">Ottohub <span>beta</span></h1>

        <img style="float:right;" src="../../images/otto_logo_gray.JPG" width="90px" height="40px"/>
        <div style="float:right;margin-right:40px;">
            <!--<a href="https://www.dropbox.com/home/Ottohub/Drivers"
               style="color:white;font-size:1.4em;padding-right:20px;text-decoration:underline;">Dropbox</a>-->
            <a href="../hub/upload-many-master.html"
               style="color:white;font-size:1.4em;text-decoration:underline;">Database</a>
        </div>
    </header>
</section>
<div class="container">
    <section class="file_pannel">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="bread_crumb">
                        <li>
                            <a href="https://www.dropbox.com/home" target="_blank" title="Dropbox">Dropbox</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="https://www.dropbox.com/home/Ottohub" target="_blank" title="Ottohub">Ottohub</a>
                        </li>
                        <li>
                            <i class="fa fa-angle-right fa-lg" aria-hidden="true"></i>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="active" title="Drivers">Drivers</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6">
                    <!-- <ul class="icon_right">
                        <li><a href="#" title=""><img src="images/icon_1.png" alt="icon_1"></a></li>
                        <li><a href="#" title=""><img src="images/icon_2.png" alt="icon_2"></a></li>
                        <li><a href="#" title=""><img src="images/icon_3.png" alt="icon_3"></a></li>
                        <li><a href="#" title=""><img src="images/icon_4.png" alt="icon_4"></a></li>
                        <li>
                            <form>
                            <div class="input-group">
                              <input type="text" class="form-control" placeholder="Search">
                              <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                              </div>
                            </div>
                          </form>
                        </li>
                    </ul> -->
                    &nbsp;
                </div>
            </div>

            <div class="folder_pannel">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th style="width: 80%;">Name</th>
                                        <th>&nbsp;</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
					if( !empty($result['entries']) && count($result['entries']) > 0 ) {
						foreach ($result['entries'] as $key => $value) {
				?>
                                    <tr>
                                        <td>
                                            <span class="icon_space">
                                                <img src="../../images/folder-icon.png" alt="folder-icon">
                                            </span>
                                            &nbsp;
                                            <span class="folderName"><?php echo $value['name']; ?></span>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <span>
                                                <a href="<?php echo 'https://www.dropbox.com/home'.$value['path_display']; ?>" title="View" class="view_btn" target="_blank">
                                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                                    View
                                                </a>
                                            </span>
                                            &nbsp;
                                            <span>
                                                <a href="javascript:void(0);" title="Share" class="view_btn shareFolder">
                                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                                    Share
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
				<?php
						}
					}
				?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div id="shareModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    
      <!-- Modal Header-->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
            <span class="icon_space">
                <img src="../../images/folder-icon.png" alt="folder-icon">
            </span>
            &nbsp;
            <span id="shareModalTitle"></span>
        </h4>
      </div>
      
      <!-- Modal Body-->
      <div class="modal-body">
          <div class="modal_input">
	     <div class="col-sm-9">
	        <div class="col-sm-1"><p>To: </p></div>
	        <div class="col-sm-11"><input type="text" placeholder="Email(s) separated with semicolon(;)" class="m_input_de" id="emails"></div>
	     </div>
	     <div class="col-sm-3">
	     	
	     	 <div class="form-group">
		  <select class="form-control" id="operation">
		    <option value="editor">Can edit</option>
		    <option value="viewer">Can view</option>
		  </select>
		</div>
		  
	     </div>
	     
	     <div class="col-md-12">
	     	<div class="midd_part">
	     		<textarea rows="8" class="model_textarea" placeholder="Add a message (optional)" id="custom_message"></textarea>
	     	</div>
	     </div>
	     
	 </div>
        <!--end-->
        <div class="clearfix"></div>
      </div>
      
      <!-- Modal Footer-->
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-primary" id="shareBtn">Share</button>-->
        <button type="button" class="btn btn-primary" id="shareBtn" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Sharing">Share</button>
      </div>
    </div>

  </div>
</div>

<script>
	$(document).ready(function(){
		$(".shareFolder").bind('click', shareFolderClickHandler);
		$("#shareBtn").bind('click', shareBtnClickHandler);
	});
	
	function shareFolderClickHandler(e) {
		var $parentTr = $(e.currentTarget).parents('tr');
		var folderName = $parentTr.find('.folderName').text();
		$('#shareModalTitle').text(folderName);
		
		$('#shareModal').modal('show');
	}
	
	function shareBtnClickHandler(e) {
		var $this = $(this);
  		$this.button('loading');
		
		var folderName = $('#shareModalTitle').text();
		var emails = $('#emails').val();
		var operation = $('#operation').val();
		var custom_message = $('#custom_message').val();
		
		$('#emails').attr('disabled','disabled');
		$('#operation').attr('disabled','disabled');
		$('#custom_message').attr('disabled','disabled');
		
		var data = {
			"share_folder": folderName,
			"emails": emails,
			"operation": operation,
			"custom_message": custom_message
		};
		$.ajax({
			'url': 'dropbox_folder_share.php',
			'data': data,
			'type': 'post',
			'dataType':'json',
			success: function(response){
				console.log(response);
				
				if(response.status == 0) {
					alert(response.message);
				} else {
					//Success section
					$('#emails').removeAttr('disabled');
					$('#operation').removeAttr('disabled');
					$('#custom_message').removeAttr('disabled');
					$this.button('reset');
					$('#shareModal').modal('hide');
					$('#emails').val('');
					$('#custom_message').val('');
					alert("Folder '"+folderName+"' shared successfully.");
				}
			},
			error: function(response){
				console.log(response);
				alert("Oops!!! An error occured.");
			}
		});
	}
</script>

</body>
</html>