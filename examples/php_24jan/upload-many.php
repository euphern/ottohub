<?php

/*
 * Example PHP implementation used for the index.html example
 */

// DataTables PHP library
include("../../php/DataTables.php");

// Alias Editor classes so they are easy to use
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate;


$action = @$_REQUEST['action'];
if ($action == 'carlist') {

    $car_type_list = array();
    $optlist1 = array();
    $optlist[0]['id'] = '';
    $optlist[0]['name'] = 'No car available';

    $result_car_type = $db->sql('select *  from car_type where 1');
    $lastresult_car_type = $result_car_type->fetchAll();
    if (count($lastresult_car_type) > 0) {
        foreach ($lastresult_car_type as $k => $v) {
            $car_type_list[$v['id']] = $v['model'];
        }
    }

    if (isset($_GET['userid'])) {
        if ($_GET['userid'] != 'undefined') {
            $usersid_data = @explode('_', $_GET['userid']);
            $usersid = $usersid_data[1];


            $result = $db->sql('select *  from cars where availability=1 AND customer_id=' . $usersid);
            $lastresult = $result->fetchAll();
            if (count($lastresult) > 0) {

                foreach ($lastresult as $k => $v) {
                    $j = $k++;
                    $optlist1[$j]['id'] = $v['reg'];
                    $optlist1[$j]['name'] = $car_type_list[$v['type']];

                }
            }
        }
    }


    $result = $db->sql('select *  from cars where availability=0 AND car=0');
    $lastresult = $result->fetchAll();

    if (count($lastresult) > 0) {

        foreach ($lastresult as $k => $v) {
            $j = $k++;
            $optlist[$j]['id'] = $v['reg'];
            $optlist[$j]['name'] = $car_type_list[$v['type']];
        }
    }


    if (count($optlist1) > 0) {
        $optlist2 = array_merge($optlist1, $optlist);
    } else {
        $optlist2 = $optlist;
    }


    echo json_encode($optlist2);
    exit;


} else if ($action == 'carlist-candidate') {


    $optlist1 = array();
    $optlist[0]['id'] = '';
    $optlist[0]['name'] = 'No car available';


    $result = $db->sql('select *  from car_type where 1 order by id ASC ');
    $lastresult = $result->fetchAll();

    if (count($lastresult) > 0) {

        foreach ($lastresult as $k => $v) {
            $j = $k++;
            $optlist[$j]['id'] = $v['id'];
            $optlist[$j]['name'] = $v['model'];
        }
    }


    if (count($optlist1) > 0) {
        $optlist2 = array_merge($optlist1, $optlist);
    } else {
        $optlist2 = $optlist;
    }


    echo json_encode($optlist2);
    exit;


} else if ($action == 'carlistwith-registration_rev') {

    $id = $_GET['id'];
    $optlist1 = array();
    $optlist[0]['id'] = '';
    $optlist[0]['name'] = 'No car available';

    if ($id) {
        if ($_GET['uid'] != 'undefined') {
            $usersid_data = @explode('_', $_GET['uid']);
            $usersid = $usersid_data[1];
            $id = trim($id);
            $result = $db->sql('select *  from cars where  `reg`="' . $id . '" AND `availability`=0  order by id ASC ');
        } else {

            $result = $db->sql('select *  from cars where  `reg`="' . $id . '" AND `availability`=0  order by id ASC ');
        }


        $lastresult = $result->fetchAll();

        if (count($lastresult) > 0) {

            foreach ($lastresult as $k => $v) {
                $j = $k++;
                $optlist[$j]['id'] = $v['type'];
                //$optlist[$j]['name']=$data_model[$v['type']];
            }
        }

        echo @$lastresult[0]['type'] ? @$lastresult[0]['type'] : 'nodata';
        exit;

    } else {

        echo 'nodata';
        exit;
    }


} else if ($action == 'carlistwith-registration') {

    $id = $_GET['id'];
    $optlist1 = array();
    $optlist[0]['id'] = '';
    $optlist[0]['name'] = 'No car available';

    if ($id) {
        if ($_GET['uid'] != 'undefined') {
            $usersid_data = @explode('_', $_GET['uid']);
            $usersid = $usersid_data[1];

            $result = $db->sql('select *  from cars where  (cars.type=' . $id . ' OR cars.customer_id=' . $usersid . ') AND cars.availability=0  order by id ASC ');
        } else {

            $result = $db->sql('select *  from cars where  cars.type=' . $id . ' AND cars.availability=0  order by id ASC ');
        }
    } else {

        $result = $db->sql('select *  from cars where   cars.availability=0  order by id ASC ');
    }
    

    $lastresult = $result->fetchAll();

    if (count($lastresult) > 0) {

        foreach ($lastresult as $k => $v) {
            $j = $k++;
            $optlist[$j]['id'] = $v['reg'];
            $optlist[$j]['name'] = $v['reg'];
        }
    }

    echo json_encode($optlist);
    exit;
} else if ($action == 'carlist-shotterm') {

    $id = 1;
    $optlist1 = array();
    $optlist[0]['id'] = '';
    $optlist[0]['name'] = 'No car available';


    $user = array();
    $result_user = $db->sql('select *  from users where  shortterm_check=1');
    $lastresult_user = $result_user->fetchAll();

   
    if (count($lastresult_user) > 0) {

        foreach ($lastresult_user as $k => $v) {
            $user[$v['id']] = $v['shortterm_car'];
        }
    }
    if ($_GET['userid'] != 'undefined') {
        $usersid_data = @explode('_', $_GET['userid']);
        $usersid = $usersid_data[1];
    }

    /*if ($_GET['userid'] != 'undefined') {
        $usersid_data = @explode('_', $_GET['userid']);
        $usersid = $usersid_data[1];

        $result = $db->sql('select *  from cars where   cars.availability=0   AND 
			cars.customer_id=' . $usersid . ' AND cars.plan="PCO Hire" order by id ASC ');
    } else {*/
        $result = $db->sql('select *  from cars where  cars.availability=0  AND cars.plan="PCO Hire" order by id ASC ');

    //}


    $lastresult = $result->fetchAll();
    if (count($lastresult) > 0) {

        foreach ($lastresult as $k => $v) {
            $j = $k++;
            if (@$user[$usersid] == $v['reg']) {
                $optlist[$j]['selected'] = 1;
            } else {
                $optlist[$j]['selected'] = 0;
            }
            $optlist[$j]['id'] = $v['reg'];
            $optlist[$j]['name'] = $v['reg'];
        }
    }

    echo json_encode($optlist);
    exit;

} else {


// Build our Editor instance and process the data coming from _POST
    if ($_POST) {

        $id = 0;
        if (isset($_POST['data'][0])) {

        } else if (isset($_POST['data'])) {
            foreach ($_POST['data'] as $k => $v) {
                $c = explode('_', $k);
                $row[$k] = $c[1];
                $id = $k;
            }


            if (!empty($_POST['data'][$k]['users']['car'])) {
                $result = $db->sql('update cars set availability=' . $row[$id] . ' where  reg= "' . $_POST['data'][$k]['users']['car'] . '"');
            }

        }
    }

    Editor::inst($db, 'users')
        ->fields(
            Field::inst('users.first_name'),
            Field::inst('users.last_name'),
            Field::inst('users.phone'),
            Field::inst('users.site')
                ->options('sites', 'id', 'name'),
            Field::inst('sites.name'),
            Field::inst('users.car')
                ->options('cars', 'reg', 'reg', array('availability' => 0)),
            Field::inst('cars.type'),
            Field::inst('cars.reg')

        )
        ->join(
            Mjoin::inst('files')
                ->link('users.id', 'users_files.user_id')
                ->link('files.id', 'users_files.file_id')
                ->fields(
                    Field::inst('id')
                        ->upload(Upload::inst($_SERVER['DOCUMENT_ROOT'] . '/upload/__ID__.__EXTN__')
                            ->db('files', 'id', array(
                                'filename' => Upload::DB_FILE_NAME,
                                'filesize' => Upload::DB_FILE_SIZE,
                                'web_path' => Upload::DB_WEB_PATH,
                                'system_path' => Upload::DB_SYSTEM_PATH
                            ))
                            ->validator(function ($file) {
                                return $file['size'] >= 50000 ?
                                    "Files must be smaller than 50K" :
                                    null;
                            })
                            ->allowedExtensions(['png', 'jpg'], "Please upload an image")
                        )
                )
        )
        ->leftJoin('sites', 'sites.id', '=', 'users.site')
        ->leftJoin('cars', 'cars.reg', '=', 'users.car')
        ->on('postEdit', function ($editor, $id, $values, $row) {
            //logChange( $editor->db(), 'edit', $id, $values );
        })
        ->process($_POST)
        ->json();


    function logChange($db, $action, $id, $values)
    {
        $db->insert('staff-log', array(
            'user' => $_SESSION['username'],
            'action' => $action,
            'values' => json_encode($values),
            'row' => $id,
            'when' => date('c')
        ));
    }
}